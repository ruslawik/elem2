<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Numeration extends Model
{
    protected $fillable = ['num', 'institute_id', 'system_id'];
    public function system(){
        return $this->belongsTo('App\Models\LawSystem', 'system_id');
    }
}
