<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use \DateTimeInterface;

class Rule extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'rules';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const TYPE_SELECT = [
        'deist' => 'Действующая',
        'init'  => 'Предлагаемая',
    ];

    protected $fillable = [
        'npa_id',
        'number',
        'name',
        'red_1',
        'red_2',
        'type',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 120, 120);
    }

    public function npa()
    {
        return $this->belongsTo(Npa::class, 'npa_id');
    }

    public function count_word($text){
        return count(explode(' ', $text));
    }

    public function count_chars($text){
        $trim = $text;
        preg_match_all("/\s/", $trim, $out);
        $spaces = count($out);
        $trim=str_replace([" ","\n","\t","&ndash;","&rsquo;","&#39;","&quot;","&nbsp;"], '', $trim);
        $num = strlen(utf8_decode($trim));
        $num += $spaces;
        return $num;
    }

    public function char_count($type){
        switch ($type) {
            case 'red_1':
                return $this->count_chars($this->red_1);
                break;
            case 'red_2':
                return $this->count_chars($this->red_2);
                break;
            default:
                # code...
                break;
        }
    }

    public function word_count($type){
        switch ($type) {
            case 'red_1':
                return $this->count_word(strip_tags(nl2br($this->red_1)));
                break;
            case 'red_2':
                return $this->count_word(strip_tags(nl2br($this->red_2)));
                break;
            default:
                # code...
                break;
        }
    }

    public function pro_positions_count($rule_id, $type){
        return Position::where('rule_id', $rule_id)
                            ->where('pro_contra', 'pro')
                            ->where('type', $type)
                            ->count();
    }

    public function contra_positions_count($rule_id, $type){
        return Position::where('rule_id', $rule_id)
                            ->where('pro_contra', 'contra')
                            ->where('type', $type)
                            ->count();
    }
}
