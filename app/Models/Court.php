<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use \DateTimeInterface;

class Court extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'courts';

    protected $appends = [
        'file',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'doctrine_id',
        'law_system_id',
        'type',
        'citata',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const TYPE_SELECT = [
        'osnova' => 'Основополагающая судебная практика',
        'posled' => 'Последующая судебная практика',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 120, 120);
    }

    public function doctrine()
    {
        return $this->belongsTo(Doctrine::class, 'doctrine_id');
    }

    public function law_system()
    {
        return $this->belongsTo(LawSystem::class, 'law_system_id');
    }

    public function getFileAttribute()
    {
        return $this->getMedia('file');
    }
}
