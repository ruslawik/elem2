<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use \DateTimeInterface;

class Discuss extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'discusses';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'npa_id',
        'institute_id',
        'jurisdiction_id',
        'post_type',
        'rule',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const POST_TYPE_SELECT = [
        'academ'   => 'Акамедическое представление',
        'normativ' => 'Нормативное представление',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 120, 120);
    }

    public function npa()
    {
        return $this->belongsTo(Npa::class, 'npa_id');
    }

    public function institute()
    {
        return $this->belongsTo(Institute::class, 'institute_id');
    }

    public function jurisdiction()
    {
        return $this->belongsTo(LawSystem::class, 'jurisdiction_id');
    }
    public function positions(){
        return $this->hasMany(Position::class, 'rule_id');
    }
    public function pro_positions_count(){
        return Position::where('rule_id', $this->id)
                            ->where('pro_contra', 'pro')
                            ->where('type', 'discusses')
                            ->count();
    }

    public function contra_positions_count($discuss_id){
        return Position::where('rule_id', $discuss_id)
                            ->where('pro_contra', 'contra')
                            ->where('type', 'discusses')
                            ->count();
    }
}
