<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use \DateTimeInterface;

class Position extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'positions';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const APPROVED_SELECT = [
        'moder'     => 'На модерации',
        'published' => 'Опубликовано',
    ];

    const TYPE_SELECT = [
        'discusses'     => 'Институциональное представление',
        'normats' => 'Постатейное представление',
        'fikirs' => 'Предложения к изменению в постатейном',
    ];

    const PRO_CONTRA_SELECT = [
        'pro' => 'PRO',
        'contra' => 'CONTRA'
    ];

    protected $fillable = [
        'approved',
        'author_id',
        'title',
        'position',
        'doctrine_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'rule_id',
        'type',
        'pro_contra',
        'doctrine_text',
        'vs_position_id',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 120, 120);
    }

    public function positionArguments()
    {
        return $this->hasMany(Argument::class, 'position_id', 'id');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function doctrine()
    {
        return $this->belongsTo(Doctrine::class, 'doctrine_id');
    }

    public function rule_id()
    {
        return $this->belongsTo(Discuss::class, 'rule_id');
    }

    public function contra_positions_count($position_id){

        return Position::where("vs_position_id", $position_id)->count();
    }
}
