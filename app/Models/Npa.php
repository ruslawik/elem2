<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;
use App\Models\Institute;
use App\Models\Rule;
use Spatie\Translatable\HasTranslations as BaseHasTranslations;

trait HasTranslations
{
    use BaseHasTranslations;
    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        $attributes = parent::toArray();
        foreach ($this->getTranslatableAttributes() as $field) {
            $attributes[$field] = $this->getTranslation($field, \App::getLocale());
        }
        return $attributes;
    }
}

class Npa extends Model
{
    use SoftDeletes;
    use HasTranslations;

    public $table = 'npas';

    public $translatable = ['name', 'pod_name'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const TYPE_SELECT = [
        'institute' => 'Институциональный',
        'article'   => 'Постатейный',
    ];

    protected $fillable = [
        'name',
        'is_inhouse',
        'pod_name',
        'category_id',
        'type',
        'organization_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function first_institute() {
        return $this->hasOne('App\Models\Institute')->oldest();
    }
    public function first_article() {
        return $this->hasOne('App\Models\Rule')->oldest();
    }
    public function first_element ($doc_id){
        if($this->type == "institute"){
            if(isset(Institute::where("npa_id", $doc_id)->first()->id)){
                return Institute::where("npa_id", $doc_id)->first()->id;
            }else{
                return 0;
            }
        }
        if($this->type == "article"){
            if(isset(Rule::where("npa_id", $doc_id)->first()->id)){
                return Rule::where("npa_id", $doc_id)->first()->id;
            }else{
                return 0;
            }
        }
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function npaInstitutes()
    {
        return $this->hasMany(Institute::class, 'npa_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id');
    }
}
