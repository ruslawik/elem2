<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use \DateTimeInterface;

class Suggestion extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'suggestions';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'npa_id',
        'rule_id',
        'suggestion',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 120, 120);
    }

    public function npa()
    {
        return $this->belongsTo(Npa::class, 'npa_id');
    }

    public function rule()
    {
        return $this->belongsTo(Rule::class, 'rule_id');
    }

    public function count_word($text){
        return count(explode(' ', $text));
    }

    public function count_chars($text){
        $trim = $text;
        preg_match_all("/\s/", $trim, $out);
        $spaces = count($out);
        $trim=str_replace([" ","\n","\t","&ndash;","&rsquo;","&#39;","&quot;","&nbsp;"], '', $trim);
        $num = strlen(utf8_decode($trim));
        $num += $spaces;
        return $num;
    }

    public function char_count(){
        return $this->count_chars($this->suggestion);
    }

    public function word_count(){
        return $this->count_word($this->suggestion);
    }

    public function pro_positions_count($suggestion_id){
        return Position::where('rule_id', $suggestion_id)
                            ->where('pro_contra', 'pro')
                            ->where('type', 'suggestion')
                            ->count();
    }

    public function contra_positions_count($suggestion_id){
        return Position::where('rule_id', $suggestion_id)
                            ->where('pro_contra', 'contra')
                            ->where('type', 'suggestion')
                            ->count();
    }
}
