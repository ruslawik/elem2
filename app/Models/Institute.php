<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Institute extends Model
{
    use SoftDeletes;

    public $table = 'institutes';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const TYPE_SELECT = [
        'baza'     => 'Базовый',
        'initiate' => 'Инициативный',
    ];

    protected $fillable = [
        'name',
        'npa_id',
        'parent_institute_id',
        'type',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function parent_institute()
    {
        return $this->belongsTo(Institute::class, 'parent_institute_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function npa()
    {
        return $this->belongsTo(Npa::class, 'npa_id');
    }

    
}
