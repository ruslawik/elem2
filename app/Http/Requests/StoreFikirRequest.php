<?php

namespace App\Http\Requests;

use App\Models\Fikir;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreFikirRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('fikir_create');
    }

    public function rules()
    {
        return [
            'doctrine_id' => [
                'required',
                'integer',
            ],
            'fio'         => [
                'string',
                'required',
            ],
            'text'        => [
                'required',
            ],
        ];
    }
}
