<?php

namespace App\Http\Requests;

use App\Models\User;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('user_edit');
    }

    public function rules()
    {
        return [
            'roles.*'         => [
                'integer',
            ],
            'roles'           => [
                'required',
                'array',
            ],
            'name'            => [
                'string',
                'required',
            ],
            'surname'         => [
                'string',
                'required',
            ],
            'last_name'       => [
                'string',
                'nullable',
            ],
            'organization_id' => [
                'required',
                'integer',
            ],
            'email'           => [
                'required',
                'unique:users,email,' . request()->route('user')->id,
            ],
            'phone'           => [
                'string',
                'nullable',
            ],
            'job_place'       => [
                'string',
                'nullable',
            ],
        ];
    }
}
