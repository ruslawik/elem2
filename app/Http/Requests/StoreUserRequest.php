<?php

namespace App\Http\Requests;

use App\Models\User;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreUserRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('user_create');
    }

    public function rules()
    {
        return [
            'roles.*'         => [
                'integer',
            ],
            'roles'           => [
                'required',
                'array',
            ],
            'name'            => [
                'string',
                'required',
            ],
            'surname'         => [
                'string',
                'required',
            ],
            'last_name'       => [
                'string',
                'nullable',
            ],
            'organization_id' => [
                'required',
                'integer',
            ],
            'email'           => [
                'required',
                'unique:users',
            ],
            'phone'           => [
                'string',
                'nullable',
            ],
            'job_place'       => [
                'string',
                'nullable',
            ],
            'password'        => [
                'required',
            ],
        ];
    }
}
