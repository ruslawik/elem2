<?php

namespace App\Http\Requests;

use App\Models\Suggestion;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroySuggestionRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('suggestion_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:suggestions,id',
        ];
    }
}
