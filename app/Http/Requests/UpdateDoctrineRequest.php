<?php

namespace App\Http\Requests;

use App\Models\Doctrine;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateDoctrineRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('doctrine_edit');
    }

    public function rules()
    {
        return [
            'razdel_id'  => [
                'required',
                'integer',
            ],
            'name'       => [
                'string',
                'required',
            ],
            'alter_name' => [
                'string',
                'nullable',
            ],
            'goal'       => [
                'required',
            ],
            'base'       => [
                'required',
            ],
        ];
    }
}
