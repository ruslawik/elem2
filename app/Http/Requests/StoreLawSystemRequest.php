<?php

namespace App\Http\Requests;

use App\Models\LawSystem;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreLawSystemRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('law_system_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'   => [
                'string',
                'required',
            ],
            'avatar' => [
                'required',
            ],
        ];
    }
}
