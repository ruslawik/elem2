<?php

namespace App\Http\Requests;

use App\Models\Normat;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyNormatRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('normat_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:normats,id',
        ];
    }
}
