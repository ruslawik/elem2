<?php

namespace App\Http\Requests;

use App\Models\Fikir;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateFikirRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('fikir_edit');
    }

    public function rules()
    {
        return [
            'doctrine_id' => [
                'required',
                'integer',
            ],
            'fio'         => [
                'string',
                'required',
            ],
            'text'        => [
                'required',
            ],
        ];
    }
}
