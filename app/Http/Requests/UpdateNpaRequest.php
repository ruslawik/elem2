<?php

namespace App\Http\Requests;

use App\Models\Npa;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateNpaRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('npa_edit');
    }

    public function rules()
    {
        return [
            'name'            => [
                'string',
                'required',
            ],
            'name_kz'            => [
                'string',
            ],
            'category_id'     => [
                'required',
                'integer',
            ],
            'type'            => [
                'required',
            ],
            'organization_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
