<?php

namespace App\Http\Requests;

use App\Models\Razdel;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreRazdelRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('razdel_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
        ];
    }
}
