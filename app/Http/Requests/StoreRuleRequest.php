<?php

namespace App\Http\Requests;

use App\Models\Rule;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreRuleRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('rule_create');
    }

    public function rules()
    {
        return [
            'npa_id' => [
                'required',
                'integer',
            ],
            'number' => [
                'required',
                'numeric',
                'min:-2147483648',
                'max:2147483647',
            ],
            'name'   => [
                'string',
                'required',
            ],
            'red_1'  => [
                'required',
            ],
            'red_2'  => [
                'required',
            ],
        ];
    }
}
