<?php

namespace App\Http\Requests;

use App\Models\Discuss;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateDiscussRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('discuss_edit');
    }

    public function rules()
    {
        return [
            'npa_id'          => [
                'required',
                'integer',
            ],
            'institute_id'    => [
                'required',
                'integer',
            ],
            'jurisdiction_id' => [
                'required',
                'integer',
            ],
            'post_type'       => [
                'required',
            ],
            'rule'            => [
                'required',
            ],
        ];
    }
}
