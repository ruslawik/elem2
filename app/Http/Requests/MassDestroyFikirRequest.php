<?php

namespace App\Http\Requests;

use App\Models\Fikir;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyFikirRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('fikir_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:fikirs,id',
        ];
    }
}
