<?php

namespace App\Http\Requests;

use App\Models\Razdel;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateRazdelRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('razdel_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
        ];
    }
}
