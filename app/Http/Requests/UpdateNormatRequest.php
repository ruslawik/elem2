<?php

namespace App\Http\Requests;

use App\Models\Normat;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateNormatRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('normat_edit');
    }

    public function rules()
    {
        return [
            'doctrine_id' => [
                'required',
                'integer',
            ],
            'name'        => [
                'string',
                'required',
            ],
            'text'        => [
                'required',
            ],
        ];
    }
}
