<?php

namespace App\Http\Requests;

use App\Models\Institute;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreInstituteRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('institute_create');
    }

    public function rules()
    {
        return [
            'name'   => [
                'string',
                'required',
            ],
            'npa_id' => [
                'required',
                'integer',
            ],
            'type'   => [
                'required',
            ],
        ];
    }
}
