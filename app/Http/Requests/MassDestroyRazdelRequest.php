<?php

namespace App\Http\Requests;

use App\Models\Razdel;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRazdelRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('razdel_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:razdels,id',
        ];
    }
}
