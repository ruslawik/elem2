<?php

namespace App\Http\Requests;

use App\Models\Suggestion;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreSuggestionRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('suggestion_create');
    }

    public function rules()
    {
        return [
            'npa_id'     => [
                'required',
                'integer',
            ],
            'rule_id'    => [
                'required',
                'integer',
            ],
            'suggestion' => [
                'required',
            ],
        ];
    }
}
