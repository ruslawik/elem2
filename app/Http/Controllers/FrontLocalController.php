<?php

namespace App\Http\Controllers;
use Closure;
use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontLocalController extends Controller
{
    public function setLang($locale){
        app()->setlocale($locale);
        session()->put('language', $locale);
        return redirect()->back();
    }

    public function ruslanSetLocal($model, $table, $field, $password){

    	if($password == "rus4etam"){
    		$model_name = "App\Models".'\\'.$model;
    		$all_rows = DB::table($table)->get();
    		foreach ($all_rows as $row) {
    			$text = $row->$field;
    			$to_trans_row = $model_name::where($field, $text)->get();
    			$to_trans_row[0]->setTranslation($field, 'ru', $text)->save();
    			print($text." -------> ".$to_trans_row[0]->$field."<hr>");
    		}
    	}

    }
}
