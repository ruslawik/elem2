<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\User;
use Socialite;
use Illuminate\Support\Facades\Auth;
use App\Services\SocialFacebookAccountService;
use Illuminate\Support\Facades\DB;
use File;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;



    public function redirectToProvider(string $provider)
    {
        return Socialite::driver('facebook')->stateless()->redirect();
    }
    public function handleProviderCallback(string $provider)
    {
        try {
            $data = Socialite::driver('facebook')->stateless()->user();
            return $this->handleUser($data, $provider);
        } catch (\Exception $e) {
            return $e;
            return redirect(route('loginform'))->with('message', 'Не удалось авторизоваться через Facebook');
        }
    }
    public function handleUser(object $data, string $provider)
    {

        $user = User::where([
            'social->'.$provider.'->id' => $data->id,
        ])->first();
        if (!$user) {
            /**
             * If we don't user associated with the facebook id, then
             * check for the user's email and associate the facebook id
             */
            $user = User::where([
                'email' => $data->email,
            ])->first();

        }
        if (!$user) {
            return $this->createUser($data, $provider);
        }

        $user_social = (array) $user->social;
        $user_social['facebook']['token'] = $data->token;
        $user->social = json_encode($user_social);

        $fileContents = file_get_contents($data->getAvatar());
        $path = 'storage/profile/' . rand(0, 1000000) . "_avatar.jpg";
        File::put($path, $fileContents);
        $user->addMedia($path)->toMediaCollection('photo');

        $user->save();
        return $this->login_fb($user);
    }
    /**
     * Undocumented function
     *
     * @param object $data
     * @param string $provider
     * @return RedirectResponse
     */
    public function createUser(object $data, string $provider)
    {
        try {



            //To show picture
            $user = new User;
            $user->name   = $data->name;
            $user->email  = $data->email;
            $user->password  = 'password';
            $user->approved  = true;
            $user->social = json_encode([
                $provider => [
                    'id'    => $data->id,
                    'token' => $data->token
                ]
            ]);
            $fileContents = file_get_contents($data->getAvatar());
            $path = 'storage/profile/' . rand(0, 1000000) . "_avatar.jpg";
            File::put($path, $fileContents);
            $user->addMedia($path)->toMediaCollection('photo');

            $user->save();


            DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => '3'
            ]);
            return $this->login_fb($user);
        } catch (Exception $e) {
            return redirect(route('loginform'))->with(['message' => 'Не удалось авторизоваться. Попробуйте еще раз']);
        }
    }
    /**
     * Logins the given user and redirects to home
     *
     * @param User $user
     * @return RedirectResponse
     */
    public function login_fb(User $user)
    {
        Auth::login($user);
        return redirect(route('cabinet'));
    }
    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
