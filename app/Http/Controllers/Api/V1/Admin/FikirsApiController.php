<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreFikirRequest;
use App\Http\Requests\UpdateFikirRequest;
use App\Http\Resources\Admin\FikirResource;
use App\Models\Fikir;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FikirsApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('fikir_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new FikirResource(Fikir::with(['doctrine'])->get());
    }

    public function store(StoreFikirRequest $request)
    {
        $fikir = Fikir::create($request->all());

        return (new FikirResource($fikir))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Fikir $fikir)
    {
        abort_if(Gate::denies('fikir_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new FikirResource($fikir->load(['doctrine']));
    }

    public function update(UpdateFikirRequest $request, Fikir $fikir)
    {
        $fikir->update($request->all());

        return (new FikirResource($fikir))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Fikir $fikir)
    {
        abort_if(Gate::denies('fikir_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $fikir->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
