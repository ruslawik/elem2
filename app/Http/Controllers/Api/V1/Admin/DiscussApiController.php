<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreDiscussRequest;
use App\Http\Requests\UpdateDiscussRequest;
use App\Http\Resources\Admin\DiscussResource;
use App\Models\Discuss;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DiscussApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('discuss_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DiscussResource(Discuss::with(['npa', 'institute', 'jurisdiction'])->get());
    }

    public function store(StoreDiscussRequest $request)
    {
        $discuss = Discuss::create($request->all());

        return (new DiscussResource($discuss))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Discuss $discuss)
    {
        abort_if(Gate::denies('discuss_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DiscussResource($discuss->load(['npa', 'institute', 'jurisdiction']));
    }

    public function update(UpdateDiscussRequest $request, Discuss $discuss)
    {
        $discuss->update($request->all());

        return (new DiscussResource($discuss))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Discuss $discuss)
    {
        abort_if(Gate::denies('discuss_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $discuss->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
