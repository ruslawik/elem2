<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreInstituteRequest;
use App\Http\Requests\UpdateInstituteRequest;
use App\Http\Resources\Admin\InstituteResource;
use App\Models\Institute;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class InstitutesApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('institute_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new InstituteResource(Institute::with(['npa', 'parent_institute'])->get());
    }

    public function store(StoreInstituteRequest $request)
    {
        $institute = Institute::create($request->all());

        return (new InstituteResource($institute))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Institute $institute)
    {
        abort_if(Gate::denies('institute_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new InstituteResource($institute->load(['npa', 'parent_institute']));
    }

    public function update(UpdateInstituteRequest $request, Institute $institute)
    {
        $institute->update($request->all());

        return (new InstituteResource($institute))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Institute $institute)
    {
        abort_if(Gate::denies('institute_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $institute->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
