<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreCourtRequest;
use App\Http\Requests\UpdateCourtRequest;
use App\Http\Resources\Admin\CourtResource;
use App\Models\Court;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CourtApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('court_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CourtResource(Court::with(['doctrine', 'law_system'])->get());
    }

    public function store(StoreCourtRequest $request)
    {
        $court = Court::create($request->all());

        if ($request->input('file', false)) {
            $court->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
        }

        return (new CourtResource($court))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Court $court)
    {
        abort_if(Gate::denies('court_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CourtResource($court->load(['doctrine', 'law_system']));
    }

    public function update(UpdateCourtRequest $request, Court $court)
    {
        $court->update($request->all());

        if ($request->input('file', false)) {
            if (!$court->file || $request->input('file') !== $court->file->file_name) {
                if ($court->file) {
                    $court->file->delete();
                }

                $court->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
            }
        } elseif ($court->file) {
            $court->file->delete();
        }

        return (new CourtResource($court))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Court $court)
    {
        abort_if(Gate::denies('court_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $court->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
