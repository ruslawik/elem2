<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreArgumentRequest;
use App\Http\Requests\UpdateArgumentRequest;
use App\Http\Resources\Admin\ArgumentResource;
use App\Models\Argument;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ArgumentsApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('argument_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ArgumentResource(Argument::with(['position'])->get());
    }

    public function store(StoreArgumentRequest $request)
    {
        $argument = Argument::create($request->all());

        return (new ArgumentResource($argument))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Argument $argument)
    {
        abort_if(Gate::denies('argument_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ArgumentResource($argument->load(['position']));
    }

    public function update(UpdateArgumentRequest $request, Argument $argument)
    {
        $argument->update($request->all());

        return (new ArgumentResource($argument))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Argument $argument)
    {
        abort_if(Gate::denies('argument_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $argument->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
