<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreSuggestionRequest;
use App\Http\Requests\UpdateSuggestionRequest;
use App\Http\Resources\Admin\SuggestionResource;
use App\Models\Suggestion;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SuggestionsApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('suggestion_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SuggestionResource(Suggestion::with(['npa', 'rule'])->get());
    }

    public function store(StoreSuggestionRequest $request)
    {
        $suggestion = Suggestion::create($request->all());

        return (new SuggestionResource($suggestion))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Suggestion $suggestion)
    {
        abort_if(Gate::denies('suggestion_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SuggestionResource($suggestion->load(['npa', 'rule']));
    }

    public function update(UpdateSuggestionRequest $request, Suggestion $suggestion)
    {
        $suggestion->update($request->all());

        return (new SuggestionResource($suggestion))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Suggestion $suggestion)
    {
        abort_if(Gate::denies('suggestion_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $suggestion->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
