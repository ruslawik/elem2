<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreRuleRequest;
use App\Http\Requests\UpdateRuleRequest;
use App\Http\Resources\Admin\RuleResource;
use App\Models\Rule;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RulesApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('rule_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RuleResource(Rule::with(['npa'])->get());
    }

    public function store(StoreRuleRequest $request)
    {
        $rule = Rule::create($request->all());

        return (new RuleResource($rule))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Rule $rule)
    {
        abort_if(Gate::denies('rule_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RuleResource($rule->load(['npa']));
    }

    public function update(UpdateRuleRequest $request, Rule $rule)
    {
        $rule->update($request->all());

        return (new RuleResource($rule))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Rule $rule)
    {
        abort_if(Gate::denies('rule_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rule->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
