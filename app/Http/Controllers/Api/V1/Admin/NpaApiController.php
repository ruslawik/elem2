<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNpaRequest;
use App\Http\Requests\UpdateNpaRequest;
use App\Http\Resources\Admin\NpaResource;
use App\Models\Npa;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class NpaApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('npa_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new NpaResource(Npa::with(['category', 'organization'])->get());
    }

    public function store(StoreNpaRequest $request)
    {
        $npa = Npa::create($request->all());

        return (new NpaResource($npa))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Npa $npa)
    {
        abort_if(Gate::denies('npa_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new NpaResource($npa->load(['category', 'organization']));
    }

    public function update(UpdateNpaRequest $request, Npa $npa)
    {
        $npa->update($request->all());

        return (new NpaResource($npa))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Npa $npa)
    {
        abort_if(Gate::denies('npa_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $npa->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
