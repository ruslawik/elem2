<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRazdelRequest;
use App\Http\Requests\UpdateRazdelRequest;
use App\Http\Resources\Admin\RazdelResource;
use App\Models\Razdel;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RazdelsApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('razdel_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RazdelResource(Razdel::with(['parent'])->get());
    }

    public function store(StoreRazdelRequest $request)
    {
        $razdel = Razdel::create($request->all());

        return (new RazdelResource($razdel))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Razdel $razdel)
    {
        abort_if(Gate::denies('razdel_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RazdelResource($razdel->load(['parent']));
    }

    public function update(UpdateRazdelRequest $request, Razdel $razdel)
    {
        $razdel->update($request->all());

        return (new RazdelResource($razdel))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Razdel $razdel)
    {
        abort_if(Gate::denies('razdel_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $razdel->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
