<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreNormatRequest;
use App\Http\Requests\UpdateNormatRequest;
use App\Http\Resources\Admin\NormatResource;
use App\Models\Normat;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class NormatApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('normat_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new NormatResource(Normat::with(['doctrine'])->get());
    }

    public function store(StoreNormatRequest $request)
    {
        $normat = Normat::create($request->all());

        return (new NormatResource($normat))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Normat $normat)
    {
        abort_if(Gate::denies('normat_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new NormatResource($normat->load(['doctrine']));
    }

    public function update(UpdateNormatRequest $request, Normat $normat)
    {
        $normat->update($request->all());

        return (new NormatResource($normat))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Normat $normat)
    {
        abort_if(Gate::denies('normat_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $normat->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
