<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreDoctrineRequest;
use App\Http\Requests\UpdateDoctrineRequest;
use App\Http\Resources\Admin\DoctrineResource;
use App\Models\Doctrine;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DoctrineApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('doctrine_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DoctrineResource(Doctrine::with(['razdel'])->get());
    }

    public function store(StoreDoctrineRequest $request)
    {
        $doctrine = Doctrine::create($request->all());

        return (new DoctrineResource($doctrine))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Doctrine $doctrine)
    {
        abort_if(Gate::denies('doctrine_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DoctrineResource($doctrine->load(['razdel']));
    }

    public function update(UpdateDoctrineRequest $request, Doctrine $doctrine)
    {
        $doctrine->update($request->all());

        return (new DoctrineResource($doctrine))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Doctrine $doctrine)
    {
        abort_if(Gate::denies('doctrine_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $doctrine->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
