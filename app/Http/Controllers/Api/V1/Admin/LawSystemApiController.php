<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreLawSystemRequest;
use App\Http\Requests\UpdateLawSystemRequest;
use App\Http\Resources\Admin\LawSystemResource;
use App\Models\LawSystem;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LawSystemApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('law_system_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new LawSystemResource(LawSystem::all());
    }

    public function store(StoreLawSystemRequest $request)
    {
        $lawSystem = LawSystem::create($request->all());

        if ($request->input('avatar', false)) {
            $lawSystem->addMedia(storage_path('tmp/uploads/' . $request->input('avatar')))->toMediaCollection('avatar');
        }

        return (new LawSystemResource($lawSystem))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(LawSystem $lawSystem)
    {
        abort_if(Gate::denies('law_system_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new LawSystemResource($lawSystem);
    }

    public function update(UpdateLawSystemRequest $request, LawSystem $lawSystem)
    {
        $lawSystem->update($request->all());

        if ($request->input('avatar', false)) {
            if (!$lawSystem->avatar || $request->input('avatar') !== $lawSystem->avatar->file_name) {
                if ($lawSystem->avatar) {
                    $lawSystem->avatar->delete();
                }

                $lawSystem->addMedia(storage_path('tmp/uploads/' . $request->input('avatar')))->toMediaCollection('avatar');
            }
        } elseif ($lawSystem->avatar) {
            $lawSystem->avatar->delete();
        }

        return (new LawSystemResource($lawSystem))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(LawSystem $lawSystem)
    {
        abort_if(Gate::denies('law_system_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $lawSystem->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
