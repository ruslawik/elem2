<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyDiscussRequest;
use App\Http\Requests\StoreDiscussRequest;
use App\Http\Requests\UpdateDiscussRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\StoreUserFrontRequest;
use App\Http\Requests\UpdateUserSelfRequest;
use App\Models\About;
use App\Models\Numeration;
use SN\DaisyDiff\DaisyDiff;
use App\Models\Discuss;
use App\Models\Institute;
use App\Models\Npa;
use App\Models\Position;
use App\Models\Category;
use App\Models\LawSystem;
use App\Models\Comment;
use App\Models\Rule;
use App\Models\Suggestion;
use App\Models\Razdel;
use App\Models\Doctrine;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use App\Models\User;
use Auth;
use App\Models\Argument;

class FrontController extends Controller
{
	use MediaUploadingTrait;

	public function navbar (){
		$navbar['first_category'] = Category::orderBy('place_num', 'ASC')->first();
        $navbar['first_library_razdel'] = Razdel::orderBy('id', 'ASC')->first();
		return $navbar;
	}

    public function index (){
        $ar['navbar'] = $this->navbar();
        return view("front.index", $ar);
    }
    public function about(){
        $ar = [];
        $ar['navbar'] = $this->navbar();
        $about = About::first();
        $ar['about'] = $about;

        return view('front.about', $ar);
    }

    public function allDocs ($cat_id){

        $ar['navbar'] = $this->navbar();
        $ar['all_cats'] = Category::orderBy('place_num', 'ASC')->get();

        $ar['docs'] = Npa::where('category_id', $cat_id)->get();

        $ar['cat_id'] = $cat_id;

        return view("front.all_docs", $ar);
    }
    public function fetchDocs ($cat_id){

        $ar['docs'] = Npa::where('category_id', $cat_id)->with('first_institute', 'first_article')->get();

        $ar['cat_id'] = $cat_id;

        return $ar;
    }


    public function docByArticle ($doc_id, $article_id){

        $ar['navbar'] = $this->navbar();
        $ar['now_article'] = Rule::where('id', $article_id)->get();
        $rule = Rule::where('id', $article_id)->get();

        if($ar['now_article']->count() < 1){
            return redirect('/error/1');
        }

        $ar['now_npa'] = Npa::where('id', $doc_id)->get();

        if($ar['now_npa'][0]->is_inhouse == 1){
            if(!Auth::check() || $ar['now_npa'][0]->organization_id != Auth::user()->organization_id){
                if(Auth::check() && (Auth::user()->roles->contains(2) || Auth::user()->roles->contains(1))){

                }else{
                    return redirect('/error/5');
                }
            }
        }

        $ar['all_articles'] = Rule::where('npa_id', $doc_id)->orderBy('number', 'ASC')->get();
        $suggestions = Suggestion::where('rule_id', $article_id)->get();

        //START Сравниваем редакцию проекта и редакцию зак. акта
        $daisyDiff = new DaisyDiff();
        $rule[0]->red_1 = preg_replace("/\<br\>/", "|br|", $rule[0]->red_1);
        $rule[0]->red_2 = preg_replace("/\<br\>/", "|br|", $rule[0]->red_2);
        $ar['diff_red_2'] = $daisyDiff->diff($rule[0]->red_1, $rule[0]->red_2);
        $ar['diff_red_2'] = preg_replace("/\|br\|/", "<br>", $ar['diff_red_2']);
        //END Сравниваем редакцию проекта и редакцию зак. акта

        //START Считаем процент изменений
        $all_del_tags = preg_match_all('/\<del class=\"diff-html-removed\"\>(.*?)<\/del\>/', $ar['diff_red_2'], $matches);

        $deleted_word_count = 0;

        foreach ($matches[1] as $deleted_text) {
            $words_here = count(explode(' ', $deleted_text));
            $deleted_word_count += $words_here;
        }

        if($deleted_word_count > $rule[0]->word_count('red_1')){
            $ar['percentage'] = 100;
        }else{
            $ar['percentage'] = round((($deleted_word_count/$rule[0]->word_count('red_1'))*100), 1);
        }

        //END Считаем процент изменений

        $suggestions_to_print = array();

        foreach ($suggestions as $key => $suggestion){
            $suggestion_html_text = preg_replace("/\<br\>/", "|br|", $suggestion->suggestion);
            $suggestion_diff = $daisyDiff->diff($rule[0]->red_1, $suggestion_html_text);
            $suggestion_diff = preg_replace("/\|br\|/", "<br>", $suggestion_diff);
            $suggestions_to_print[$key]['suggestion'] = $suggestion_diff;
            $suggestions_to_print[$key]['id'] = $suggestion->id;
            $suggestions_to_print[$key]['pro_positions_count'] = $suggestion->pro_positions_count($suggestion->id);
            $suggestions_to_print[$key]['contra_positions_count'] = $suggestion->contra_positions_count($suggestion->id);
            $suggestions_to_print[$key]['char_count'] = $suggestion->char_count();
            $suggestions_to_print[$key]['word_count'] = $suggestion->word_count();

            //START Считаем процент изменений
            $all_del_tags = preg_match_all('/\<del class=\"diff-html-removed\"\>(.*?)<\/del\>/', $suggestion_diff, $matches);
            $deleted_word_count = 0;

            foreach ($matches[1] as $deleted_text) {
                $words_here = count(explode(' ', $deleted_text));
                $deleted_word_count += $words_here;
            }

            if($deleted_word_count > $rule[0]->word_count('red_1')){
                $suggestions_to_print[$key]['percentage'] = 100;
            }else{
                $suggestions_to_print[$key]['percentage'] = round((($deleted_word_count/$rule[0]->word_count('red_1'))*100), 1);
            }
            //END Считаем процент изменений
        }

        $ar['suggestions_to_print'] = $suggestions_to_print;

        return view('front.doc_by_article', $ar);
    }
    public function fetchArticle ($doc_id, $article_id){

        $ar['now_article'] = Rule::where('id', $article_id)->first();
        $rule = Rule::where('id', $article_id)->get();


        $ar['all_articles'] = Rule::where('npa_id', $doc_id)->orderBy('number', 'ASC')->get();
        $suggestions = Suggestion::where('rule_id', $article_id)->get();

        //START Сравниваем редакцию проекта и редакцию зак. акта
        $daisyDiff = new DaisyDiff();
        $rule[0]->red_1 = preg_replace("/\<br\>/", "|br|", $rule[0]->red_1);
        $rule[0]->red_2 = preg_replace("/\<br\>/", "|br|", $rule[0]->red_2);
        $ar['diff_red_2'] = $daisyDiff->diff($rule[0]->red_1, $rule[0]->red_2);
        $ar['diff_red_2'] = preg_replace("/\|br\|/", "<br>", $ar['diff_red_2']);
        //END Сравниваем редакцию проекта и редакцию зак. акта

            //START Считаем процент изменений
        $all_del_tags = preg_match_all('/\<del class=\"diff-html-removed\"\>(.*?)<\/del\>/', $ar['diff_red_2'], $matches);

        $deleted_word_count = 0;

        foreach ($matches[1] as $deleted_text) {
            $words_here = count(explode(' ', $deleted_text));
            $deleted_word_count += $words_here;
        }

        if($deleted_word_count > $rule[0]->word_count('red_1')){
            $ar['percentage'] = 100;
        }else{
            $ar['percentage'] = round((($deleted_word_count/$rule[0]->word_count('red_1'))*100), 1);
        }

        //END Считаем процент изменений

        $suggestions_to_print = array();

        foreach ($suggestions as $key => $suggestion){
            $suggestion_html_text = preg_replace("/\<br\>/", "|br|", $suggestion->suggestion);
            $suggestion_diff = $daisyDiff->diff($rule[0]->red_1, $suggestion_html_text);
            $suggestion_diff = preg_replace("/\|br\|/", "<br>", $suggestion_diff);
            $suggestions_to_print[$key]['suggestion'] = $suggestion_diff;
            $suggestions_to_print[$key]['id'] = $suggestion->id;
            $suggestions_to_print[$key]['pro_positions_count'] = $suggestion->pro_positions_count($suggestion->id);
            $suggestions_to_print[$key]['contra_positions_count'] = $suggestion->contra_positions_count($suggestion->id);
            $suggestions_to_print[$key]['char_count'] = $suggestion->char_count();
            $suggestions_to_print[$key]['word_count'] = $suggestion->word_count();

            //START Считаем процент изменений
            $all_del_tags = preg_match_all('/\<del class=\"diff-html-removed\"\>(.*?)<\/del\>/', $suggestion_diff, $matches);
            $deleted_word_count = 0;

            foreach ($matches[1] as $deleted_text) {
                $words_here = count(explode(' ', $deleted_text));
                $deleted_word_count += $words_here;
            }

            if($deleted_word_count > $rule[0]->word_count('red_1')){
                $suggestions_to_print[$key]['percentage'] = 100;
            }else{
                $suggestions_to_print[$key]['percentage'] = round((($deleted_word_count/$rule[0]->word_count('red_1'))*100), 1);
            }
            //END Считаем процент изменений
        }

        $ar['suggestions_to_print'] = $suggestions_to_print;

        $ar['red_1_pro'] = Position::where('rule_id', $article_id)
                            ->where('pro_contra', 'pro')
                            ->where('type', 'red1')
                            ->count();
        $ar['red_1_contra'] = Position::where('rule_id', $article_id)
                            ->where('pro_contra', 'contra')
                            ->where('type', 'red1')
                            ->count();
        $ar['red_2_pro'] = Position::where('rule_id', $article_id)
                            ->where('pro_contra', 'pro')
                            ->where('type', 'red2')
                            ->count();
        $ar['red_2_contra'] = Position::where('rule_id', $article_id)
                            ->where('pro_contra', 'contra')
                            ->where('type', 'red2')
                            ->count();
        return $ar;
    }

    public function error($err_id){
        $ar['navbar'] = $this->navbar();
        if($err_id == 1){
            $ar['error'] = "Этот документ еще пуст.";
            return view("front.error", $ar);
        }else if($err_id == 5){
            $ar['error'] = "Вы не зарегистрированы или Ваш аккаунт не привязан к организации, в которой ведется обсуждение данного документа.";
            return view("front.error", $ar);
        }else{
            $ar['error'] = "Такой ошибки не существует.";
            return view("front.error", $ar);
        }
    }

    public function docInstitutional ($doc_id, $inst_id){

        $ar['navbar'] = $this->navbar();
        $ar['now_institute'] = Institute::where('id', $inst_id)->get();
        if($ar['now_institute']->count() < 1){
            return redirect('/error/1');
        }
        $ar['now_npa'] = Npa::where('id', $doc_id)->get();
        if($ar['now_npa'][0]->is_inhouse == 1){
            if(!Auth::check() || $ar['now_npa'][0]->organization_id != Auth::user()->organization_id){
                if(Auth::check() && (Auth::user()->roles->contains(2) || Auth::user()->roles->contains(1))){

                }else{
                    return redirect('/error/5');
                }
            }
        }
        $ar['all_institutes'] = Institute::where("npa_id", $doc_id)->where('parent_institute_id', NULL)->get();
        if($ar['now_institute'][0]->parent_institute_id == NULL){
            $children = Institute::where("parent_institute_id", $inst_id)->get();
            if($children->count() > 0){
                $ar['has_children'] = 1;
                $ar['children_institutes'] = $children;
            }else{
                $ar['has_children'] = 0;
            }
        }else{
            $children = Institute::where("parent_institute_id", $ar['now_institute'][0]->parent_institute_id)->get();
            if($children->count() > 0){
                $ar['has_children'] = 1;
                $ar['children_institutes'] = $children;
            }else{
                $ar['has_children'] = 0;
            }
        }

        $ar['academic_discuss_rules'] = Discuss::where("npa_id", $doc_id)
                                            ->where("institute_id", $inst_id)
                                            ->where("post_type", "academ")
            ->with('positions')
                                            ->get();

        $ar['normativ_discuss_rules'] = Discuss::where("npa_id", $doc_id)
                                            ->where("institute_id", $inst_id)
                                            ->where("post_type", "normativ")
                                            ->with('positions')
                                            ->get();

        $law_system_ids = [];
        foreach($ar['academic_discuss_rules'] as $item){
            if(!in_array($item->id, $law_system_ids)){
                $law_system_ids[] = $item->jurisdiction_id;
            }
        }
        foreach($ar['normativ_discuss_rules'] as $item){
            if(!in_array($item->id, $law_system_ids)){
                $law_system_ids[] = $item->jurisdiction_id;
            }
        }

        $ar['jurisdictions'] = LawSystem::whereIn('id', $law_system_ids)->with(['numeration' => function ($query) {
            $query->orderBy('num', 'desc');
        }])->get();
        /*$ar['jurisdictions'] = LawSystem::
        join('numerations', 'systems.id', '=', 'numerations.system_id')
            ->whereIn('systems.id', $law_system_ids)
            ->orderBy('numerations.num')
            ->get();*/

        $ar['jurisdictions'] = LawSystem::whereIn('id', $law_system_ids)->get();

        $numerations = Numeration::
        where('institute_id', $ar['now_institute'][0]->id)
        ->whereIn('system_id', $law_system_ids)
        ->orderByRaw('ISNULL(num), num ASC')
        ->get();

        $with_nums = array();

        if(count($numerations) > 0){
            foreach($numerations as $numeration){
                foreach($ar['jurisdictions'] as $jur){
                    if($numeration->system_id == $jur->id){
                        $with_nums[] = $jur;
                    }
                }
            }
            $ar['jurisdictions'] = $with_nums;
        }

        return view("front.doc_institutional", $ar);
    }
    public function fetch_institutional ($doc_id, $inst_id){
        $ar['now_institute'] = Institute::where('id', $inst_id)->get();
        $ar['now_npa'] = Npa::where('id', $doc_id)->get();
        if($ar['now_institute'][0]->parent_institute_id == NULL){
            $children = Institute::where("parent_institute_id", $inst_id)->get();
            if($children->count() > 0){
                $ar['has_children'] = 1;
                $ar['children_institutes'] = $children;
            }else{
                $ar['has_children'] = 0;
            }
        }else{
            $children = Institute::where("parent_institute_id", $ar['now_institute'][0]->parent_institute_id)->get();
            if($children->count() > 0){
                $ar['has_children'] = 1;
                $ar['children_institutes'] = $children;
            }else{
                $ar['has_children'] = 0;
            }
        }

        $ar['academic_discuss_rules'] = Discuss::where("npa_id", $doc_id)
            ->where("institute_id", $inst_id)
            ->where("post_type", "academ")
            ->with('positions')
            ->get();

        $ar['normativ_discuss_rules'] = Discuss::where("npa_id", $doc_id)
            ->where("institute_id", $inst_id)
            ->where("post_type", "normativ")
            ->with('positions')
            ->get();

        $law_system_ids = [];
        foreach($ar['academic_discuss_rules'] as $item){
            if(!in_array($item->id, $law_system_ids)){
                $law_system_ids[] = $item->jurisdiction_id;
            }
        }
        foreach($ar['normativ_discuss_rules'] as $item){
            if(!in_array($item->id, $law_system_ids)){
                $law_system_ids[] = $item->jurisdiction_id;
            }
        }

        $ar['jurisdictions'] = LawSystem::whereIn('id', $law_system_ids)->orderByRaw('ISNULL(discuss_num), discuss_num ASC')->get();

        $numerations = Numeration::where('institute_id', $ar['now_institute'][0]->id)
            ->whereIn('system_id', $law_system_ids)
            ->orderByRaw('ISNULL(num), num ASC')
            ->get();

        $with_nums = array();

        if(count($numerations) > 0){
            foreach($numerations as $numeration){
                foreach($ar['jurisdictions'] as $jur){
                    if($numeration->system_id == $jur->id){
                        $with_nums[] = $jur;
                    }
                }
            }
            $ar['jurisdictions'] = $with_nums;

        }

        return $ar;
    }

    public function getDiscussion (Request $r){
        $is_child = 0;
        $parent_institute_data = 0;
        $rule_id = $r->input("rule_id");
        $type = $r->input("type");
        $rule_data = Discuss::where('id', $rule_id)->get();

        $npa_data = Npa::where('id', $rule_data[0]->npa_id)->get();
        $institute_data = Institute::where('id', $rule_data[0]->institute_id)->get();
        $jurisdiction_data = LawSystem::where('id', $rule_data[0]->jurisdiction_id)->get();

        if($institute_data[0]->parent_institute_id != NULL){
            $is_child = 1;
            $parent_institute_data = Institute::where('id', $institute_data[0]->parent_institute_id)->get();
        }

        $positions_pro = Position::where("rule_id", $rule_data[0]->id)
                                    ->where('type', $type)
                                    ->where('pro_contra', 'pro')
                                    ->get();

        $positions_contra = Position::where("rule_id", $rule_data[0]->id)
                                    ->where('type', $type)
                                    ->where('pro_contra', 'contra')
                                    ->get();

        $comments = Comment::where('post_id', $rule_data[0]->id)
                             ->where('type', $type)
                             ->where('status', 'published')
                             ->get();

        $ar['rule_id'] = $rule_id;
        $ar['rule_data'] = $rule_data;
        $ar['type'] = $type;
        $ar['npa_data'] = $npa_data;
        $ar['institute_data'] = $institute_data;
        $ar['jurisdiction_data'] = $jurisdiction_data;
        $ar['is_child'] = $is_child;
        $ar['parent_institute_data'] = $parent_institute_data;
        $ar['positions_pro'] = $positions_pro;
        $ar['positions_contra'] = $positions_contra;
        $ar['comments'] = $comments;

        return view("front._partials.discussion", $ar);
    }

    public function getContraPositions(Request $r){

        $position_id = $r->input("position_id");
        $pro_contra = $r->input("pro_contra");
        $type = $r->input('type');

        $current_position_data = Position::where('id', $position_id)->get();
        $ar['positions_contra'] = Position::where('vs_position_id', $position_id)->get();
        $ar['current_position'] = $current_position_data[0];
        $ar['type'] = $type;

        if($pro_contra == "pro"){
            return view("front._partials.contra_positions", $ar);
        }
        if($pro_contra == "contra"){
            return view("front._partials.pro_positions", $ar);
        }
    }

    public function getCompareBoxJurisdictions (Request $r){

        $ar['jurisdictions'] = LawSystem::all();
        $ar['now_institute_id'] = $r->input('now_institute');

        return view("front._partials.compare_box_jurisdictions", $ar);
    }

    public function getCompareBoxInstitute (Request $r){

        $institute_id = $r->input('institute_id');
        $jurisdiction_id = $r->input('jurisdiction_id');
        $type = $r->input('type');

        $ar['type'] = $type;
        $ar['comp_box_id'] = $r->input('comp_box_id');
        $ar['jurisdiction'] = LawSystem::where('id', $jurisdiction_id)->get();
        $ar['rules'] = Discuss::where('post_type', $type)
                                ->where('institute_id', $institute_id)
                                ->where('jurisdiction_id', $jurisdiction_id)
                                ->get();

        return view("front._partials.compare_box_institute", $ar);

    }

    public function getCompareBoxNew (Request $r){

        $ar['institute_id'] = $r->input('institute_id');
        $ar['box_id'] = $r->input('box_id');

        return view("front._partials.compare_box_new", $ar);

    }

    public function getDoctrine (Request $r){

        $pos_id = $r->input("position_id");

        $doctrine = Position::where('id', $pos_id)->get();

        return $doctrine[0]->doctrine_text;
    }

    public function loadDoctrine ($pos_id){

        $doctrine = Position::where('id', $pos_id)->get();

        return $doctrine[0]->doctrine_text;
    }
    public function editPosition(){
	    $arguments = request('arguments');
	    Position::where('id', request('id'))->update([
            'title' => \Illuminate\Support\Str::limit(strip_tags(request('doctrine_text'))),
            'pro_contra' => request('pro_contra'),
            'position' => request('position_text'),
            'doctrine_text' => request('doctrine_text'),
            'approved' => 'moder',
        ]);
	    Argument::where('position_id', request('id'))->delete();
	    $array = array();
	    for($i = 0; $i < count($arguments); $i++){
	        $array[] = [
	            'argument' => $arguments[$i],
                'position_id' => request('id')
            ];
        }
	    Argument::insert($array);
        return redirect()->back()->with('success', 'Ваша позиция успешно отправлена на проверку модератором!');
    }
    public function addPosition (Request $request){
        $pro_contra = $request->input("pro_contra");
        if($request->has('text_pro_contra') && $request->input('text_pro_contra') != "-1"){
            $pro_contra = $request->input('text_pro_contra');
        }
        $position_text = $request->input("position_text");
        $doctrine_text = $request->input("doctrine_text");
        $title = $this->str_limit(strip_tags($position_text));
        $discuss_rule_id = $request->input("discuss_rule_id");
        $author_id = Auth::user()->id;

        $position = new Position();
        $position->position = $position_text;
        $position->doctrine_text = $doctrine_text;
        $position->title = $title;
        $position->approved = "moder";
        $position->pro_contra = $pro_contra;
        $position->rule_id = $discuss_rule_id;
        $position->author_id = $author_id;
        $position->organization_id = $request->input('organization_id');

        if($request->has('vs_position_id') && $request->input('vs_position_id') != "-1")
            $position->vs_position_id = $request->input("vs_position_id");

        $position->type = $request->input('type');
        $position->save();

        $position_id = $position->id;

        $arg_num = $request->input("total_arguments");
        for($i=1; $i<=$arg_num; $i++){
            if($request->has('argument_'.$i)){
                $argument = new Argument();
                $argument->argument =  $request->input("argument_".$i);
                $argument->position_id = $position_id;
                $argument->save();
            }
        }

        return redirect()->back()->with('success', 'Ваша позиция успешно отправлена на проверку модератором! <br> Вы можете просмотреть статус написанных Вами позиций <a href="/cabinet/positions">в личном кабинете.</a>');

    }


    public function str_limit($value, $limit = 40, $end = '...')
    {
        $limit = $limit - mb_strlen($end); // Take into account $end string into the limit
        $valuelen = mb_strlen($value);
        return $limit < $valuelen ? mb_substr($value, 0, mb_strrpos($value, ' ', $limit - $valuelen)) . $end : $value;
    }

    public function addComment(Request $r){

        $post_id = $r->input('discuss_rule_id');
        $comment_text = $r->input('comment_text');
        $type = $r->input('type');
        $author_id = Auth::user()->id;
        $status = "published";

        $comment = new Comment();
        $comment->post_id = $post_id;
        $comment->comment = $comment_text;
        $comment->type = $type;
        $comment->author_id = $author_id;
        $comment->status = $status;
        $comment->organization_id = $r->input('organization_id');
        $comment->save();

        return redirect()->back()->with('success', 'Ваш комментарий успешно опубликован!');

    }


    public function loginForm (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['action'] = action("Front\FrontController@frontLogin");

    	return view("front.loginform", $ar);
    }

    public function registrationForm (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['action'] = action("Front\FrontController@frontReg");

    	return view("front.registration_form", $ar);
    }

    public function cabinet (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['user_data'] = Auth::user();
    	//$ar['action'] = action("Front\FrontController@updateUserData");

    	return view('front.cabinet', $ar);
    }

    public function frontReg (StoreUserFrontRequest $request){

    	$user = User::create($request->all());

        $user->roles()->sync($request->input('roles', []));

        /*
        if ($request->input('photo', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }*/

        if ($request->input('education_diploma', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('education_diploma')))->toMediaCollection('education_diploma');
        }

        if ($request->input('degree_diploma', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('degree_diploma')))->toMediaCollection('degree_diploma');
        }

        return redirect()->back()->with('success', 'Ваша заявка успешно отправлена на рассмотрение модератором!');
    }

    public function update (UpdateUserSelfRequest $request, User $user)
    {
    	foreach ($request->input('roles') as $role) {
    		if($role == 4 && $user->roles->contains(3)){
    			$user->update(["approved" => "0"]);
    		}
    	}
        $user->update($request->all());
        $user->roles()->sync($request->input('roles', []));


        if ($request->input('photo', false)) {
            if (!$user->photo || $request->input('photo') !== $user->photo->file_name) {
                if ($user->photo) {
                    $user->photo->delete();
                }

                $user->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
            }
        } elseif ($user->photo) {
            $user->photo->delete();
        }

        if ($request->input('education_diploma', false)) {
            if (!$user->education_diploma || $request->input('education_diploma') !== $user->education_diploma->file_name) {
                if ($user->education_diploma) {
                    $user->education_diploma->delete();
                }

                $user->addMedia(storage_path('tmp/uploads/' . $request->input('education_diploma')))->toMediaCollection('education_diploma');
            }
        } elseif ($user->education_diploma) {
            $user->education_diploma->delete();
        }

        if ($request->input('degree_diploma', false)) {
            if (!$user->degree_diploma || $request->input('degree_diploma') !== $user->degree_diploma->file_name) {
                if ($user->degree_diploma) {
                    $user->degree_diploma->delete();
                }

                $user->addMedia(storage_path('tmp/uploads/' . $request->input('degree_diploma')))->toMediaCollection('degree_diploma');
            }
        } elseif ($user->degree_diploma) {
            $user->degree_diploma->delete();
        }

        return redirect()->back()->with('success', 'Данные успешно обновлены!');;
    }

    public function cabinetPositions (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['user_data'] = Auth::user();
    	$ar['positions'] = Position::where("author_id", Auth::user()->id)->orderBy("id", "DESC")->get();

    	return view("front.cabinet_positions", $ar);
    }

    public function publicProfile ($user_id){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	$ar['user_data'] = User::where('id', $user_id)->get();
    	$ar['positions'] = Position::where("author_id", $user_id)->orderBy("id", "DESC")->get();

    	return view("front.public_profile", $ar);
    }

    public function posText ($position_id){

    	$text = Position::where('id', $position_id)->get();
    	print($text['0']->position);
    }

    public function select2Institutes ($npa_id){

    	$data = Institute::where('npa_id', $npa_id)->get();
    	foreach ($data as $institute) {
    		$select2[] = array("id"=>$institute->id, "text"=> $institute->name);
    	}
    	echo json_encode($select2);
    }

    public function select2Npas (){

    	$data = Npa::all();
    	foreach ($data as $npa) {
    		$select2[] = array("id"=>$npa->id, "text"=> $npa->name);
    	}
    	echo json_encode($select2);
    }

    public function Team (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	return view("front.team", $ar);
    }

    public function Faqs (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	return view("front.faqs", $ar);
    }

    public function baseDocs (){

    	$ar = [];
    	$ar['navbar'] = $this->navbar();
    	return view("front.base_docs", $ar);
    }

    public function insesToSearch ($npa_id){

    	$data = Institute::where('npa_id', $npa_id)->get();
    	print('<option value>Все</option>');
    	foreach ($data as $institute) {
    		print('<option value="'.$institute->name.'">'.$institute->name.'</option>');
    	}
    }

}









