<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Discuss;
use App\Models\Institute;
use App\Models\Npa;
use App\Models\Position;
use App\Models\Category;
use App\Models\LawSystem;
use App\Models\Comment;
use App\Models\Rule;
use App\Models\Suggestion;

class DiscussionController extends Controller
{
    public function getDiscussionArticle (Request $r){

    	$rule_id = $r->input("rule_id");
        $type = $r->input("type");
        $rule_data = Rule::where('id', $rule_id)->get();

        $npa_data = Npa::where('id', $rule_data[0]->npa_id)->get();

        $positions_pro = Position::where("rule_id", $rule_data[0]->id)
                                    ->where('type', $type)
                                    ->where('pro_contra', 'pro')
                                    ->get();

        $positions_contra = Position::where("rule_id", $rule_data[0]->id)
                                    ->where('type', $type)
                                    ->where('pro_contra', 'contra')
                                    ->get();

        $comments = Comment::where('post_id', $rule_data[0]->id)
                             ->where('type', $type)
                             ->where('status', 'published')
                             ->get();

        $ar['rule_id'] = $rule_id;
        $ar['rule_data'] = $rule_data;
        $ar['type'] = $type;
        $ar['npa_data'] = $npa_data;
        $ar['positions_pro'] = $positions_pro;
        $ar['positions_contra'] = $positions_contra;
        $ar['comments'] = $comments;

        return view("front._partials.discussion_by_article", $ar);
        
    }

    public function getDiscussionSuggestions (Request $r){

    	$sug_id = $r->input('suggestion_id');
    	$suggestion = Suggestion::where('id', $sug_id)->get();

    	$positions_pro = Position::where("rule_id", $sug_id)
                                    ->where('type', 'suggestion')
                                    ->where('pro_contra', 'pro')
                                    ->get();

        $positions_contra = Position::where("rule_id", $sug_id)
                                    ->where('type', 'suggestion')
                                    ->where('pro_contra', 'contra')
                                    ->get();

        $comments = Comment::where('post_id', $sug_id)
                             ->where('type', 'suggestion')
                             ->where('status', 'published')
                             ->get();

        $ar['positions_pro'] = $positions_pro;
        $ar['positions_contra'] = $positions_contra;
        $ar['comments'] = $comments;
        $ar['suggestion'] = $suggestion;

    	return view("front._partials.discussion_by_article_suggestion", $ar);
    }

    public function getContraPositionsSuggestions (Request $r){

    	$position_id = $r->input("position_id");
        $pro_contra = $r->input("pro_contra");
        $type = $r->input('type');

        $current_position_data = Position::where('id', $position_id)->get();
        $ar['positions_contra'] = Position::where('vs_position_id', $position_id)->get();
        $ar['current_position'] = $current_position_data[0];
        $ar['type'] = $type;

        if($pro_contra == "pro"){
            return view("front._partials.contra_positions_suggestions", $ar);
        }
        if($pro_contra == "contra"){
            return view("front._partials.pro_positions_suggestions", $ar);
        }

    }
}
