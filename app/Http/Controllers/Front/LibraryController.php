<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Front\FrontController;
use App\Models\Razdel;
use App\Models\Doctrine;
use App\Models\Normat;
use App\Models\Court;
use App\Models\Fikir;
use App\Models\LawSystem;


class LibraryController extends Controller
{
    public function index (){

    	$front = new FrontController();
    	$ar['navbar'] = $front->navbar();
    	$ar['all_razdels'] = Razdel::where('parent_id', NULL)->get();

    	return view("front.library", $ar);
    }

    public function fetch_doctrines ($razdel_id){
        return Doctrine::where('razdel_id', $razdel_id)->get();
    }
    public function doctrinePage ($razdel_id, $doctrine_id){


    	$ar['now_doctrine'] = Doctrine::where('id', $doctrine_id)->first();
    	$ar['all_normats'] = Normat::where('doctrine_id', $doctrine_id)->get();
    	$ar['all_fikirs'] = Fikir::where('doctrine_id', $doctrine_id)->get();
    	$ar['all_courts_osnova'] = Court::where('doctrine_id', $doctrine_id)
    									->where('type', 'osnova')
    									->get();
    	$ar['all_courts_posled'] = Court::where('doctrine_id', $doctrine_id)
    									->where('type', 'posled')
    									->get();

    	$law_system_ids = [];
    	foreach($ar['all_courts_osnova'] as $item){
    	    if(!in_array($item->law_system_id, $law_system_ids)){
                $law_system_ids[] = $item->law_system_id;
            }
        }
        foreach($ar['all_courts_posled'] as $item){
            if(!in_array($item->law_system_id, $law_system_ids)){
                $law_system_ids[] = $item->law_system_id;
            }
        }
    	$ar['jurisdictions'] = LawSystem::whereIn('id', $law_system_ids)->orderBy('court_num', 'asc')->get();

    	$ar['now_razdel_id'] = $razdel_id;

    	return $ar;
    }
    public function doctrinePage2 ($razdel_id, $doctrine_id){

        $front = new FrontController();
        $ar['navbar'] = $front->navbar();

        $ar['all_razdels'] = Razdel::where('parent_id', NULL)->get();
        $ar['now_razdel'] = Razdel::where('id', $razdel_id)->get();
        $ar['all_doctrine_in_razdel'] = Doctrine::where('razdel_id', $razdel_id)->get();

        $ar['now_doctrine'] = Doctrine::where('id', $doctrine_id)->get();
        $ar['all_normats'] = Normat::where('doctrine_id', $doctrine_id)->get();
        $ar['all_fikirs'] = Fikir::where('doctrine_id', $doctrine_id)->get();
        $ar['all_courts_osnova'] = Court::where('doctrine_id', $doctrine_id)
            ->where('type', 'osnova')
            ->get();
        $ar['all_courts_posled'] = Court::where('doctrine_id', $doctrine_id)
            ->where('type', 'posled')
            ->get();
        $ar['jurisdictions'] = LawSystem::all();

        $ar['now_razdel_id'] = $razdel_id;

        return view("front.library_doctrine_open", $ar);
    }

}
