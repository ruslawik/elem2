<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\About;

class AboutController extends Controller
{

    public function edit(){
        $about = About::first();
        return view('admin.about.edit', compact('about'));
    }
    public function update(){
        About::first()->update([
            'text' => request('text')
        ]);
        return redirect()->back();

    }
}
