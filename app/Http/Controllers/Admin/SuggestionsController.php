<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroySuggestionRequest;
use App\Http\Requests\StoreSuggestionRequest;
use App\Http\Requests\UpdateSuggestionRequest;
use App\Models\Npa;
use App\Models\Rule;
use App\Models\Suggestion;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Auth;

class SuggestionsController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('suggestion_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
            $suggestions = Suggestion::whereHas('npa', function($q){$q->where('organization_id', Auth::user()->organization_id);})->get();
        }else{
            $suggestions = Suggestion::all();
        }

        if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
            $npas       = Npa::where('organization_id', auth()->user()->organization_id)->get();
            $rules = Rule::whereHas('npa', function($q){$q->where('organization_id', Auth::user()->organization_id);})->get();
        }else{
            $npas = Npa::get();
            $rules = Rule::get();
        }

        return view('admin.suggestions.index', compact('suggestions', 'npas', 'rules'));
    }

    public function create()
    {
        abort_if(Gate::denies('suggestion_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if(Auth::user()->roles->contains(2)){
            $npas = Npa::where('type', 'article')->where('organization_id', Auth::user()->organization_id)->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        }else{
            $npas = Npa::where('type', 'article')->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        }

        $rules = Rule::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.suggestions.create', compact('npas', 'rules'));
    }

    public function store(StoreSuggestionRequest $request)
    {
        $suggestion = Suggestion::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $suggestion->id]);
        }

        return redirect()->route('admin.suggestions.index');
    }

    public function edit(Suggestion $suggestion)
    {
        abort_if(Gate::denies('suggestion_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $npas = Npa::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $rules = Rule::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $suggestion->load('npa', 'rule');

        return view('admin.suggestions.edit', compact('npas', 'rules', 'suggestion'));
    }

    public function update(UpdateSuggestionRequest $request, Suggestion $suggestion)
    {
        $suggestion->update($request->all());

        return redirect()->route('admin.suggestions.index');
    }

    public function show(Suggestion $suggestion)
    {
        abort_if(Gate::denies('suggestion_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $suggestion->load('npa', 'rule');

        return view('admin.suggestions.show', compact('suggestion'));
    }

    public function destroy(Suggestion $suggestion)
    {
        abort_if(Gate::denies('suggestion_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $suggestion->delete();

        return back();
    }

    public function massDestroy(MassDestroySuggestionRequest $request)
    {
        Suggestion::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('suggestion_create') && Gate::denies('suggestion_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Suggestion();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
