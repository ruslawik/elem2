<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Organization;
use App\Models\Role;
use App\Models\User;
use Auth;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('user_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            if(Auth::user()->roles->contains(2)){
                $query = User::where('organization_id', Auth::user()->organization_id)->with(['roles', 'organization'])->select(sprintf('%s.*', (new User)->table));
            }else{
                $query = User::with(['roles', 'organization'])->select(sprintf('%s.*', (new User)->table));
            }
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'user_show';
                $editGate      = 'user_edit';
                $deleteGate    = 'user_delete';
                $crudRoutePart = 'users';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('photo', function ($row) {
                if ($photo = $row->photo) {
                    return sprintf(
                        '<a href="%s" target="_blank"><img src="%s" width="50px" height="50px"></a>',
                        $photo->url,
                        $photo->thumbnail
                    );
                }

                return '';
            });
            $table->editColumn('roles', function ($row) {
                $labels = [];

                foreach ($row->roles as $role) {
                    $labels[] = sprintf('<span class="label label-info label-many">%s</span>', $role->title);
                }

                return implode(' ', $labels);
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('surname', function ($row) {
                return $row->surname ? $row->surname : "";
            });
            $table->editColumn('approved', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->approved ? 'checked' : null) . '>';
            });
            $table->addColumn('organization_name', function ($row) {
                return $row->organization ? $row->organization->name : '';
            });

            $table->editColumn('email', function ($row) {
                return $row->email ? $row->email : "";
            });
            $table->editColumn('phone', function ($row) {
                return $row->phone ? $row->phone : "";
            });
            $table->editColumn('job_place', function ($row) {
                return $row->job_place ? $row->job_place : "";
            });
            $table->editColumn('education_diploma', function ($row) {
                return $row->education_diploma ? '<a href="' . $row->education_diploma->getUrl() . '" target="_blank">' . trans('global.downloadFile') . '</a>' : '';
            });
            $table->editColumn('degree_diploma', function ($row) {
                return $row->degree_diploma ? '<a href="' . $row->degree_diploma->getUrl() . '" target="_blank">' . trans('global.downloadFile') . '</a>' : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'photo', 'roles', 'approved', 'organization', 'education_diploma', 'degree_diploma']);

            return $table->make(true);
        }

        $roles         = Role::get();
        $organizations = Organization::get();

        return view('admin.users.index', compact('roles', 'organizations'));
    }

    public function create()
    {
        abort_if(Gate::denies('user_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $roles = Role::all()->pluck('title', 'id');

        $organizations = Organization::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.users.create', compact('roles', 'organizations'));
    }

    public function store(StoreUserRequest $request)
    {
        $user = User::create($request->all());
        $user->roles()->sync($request->input('roles', []));

        if ($request->input('photo', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }

        if ($request->input('education_diploma', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('education_diploma')))->toMediaCollection('education_diploma');
        }

        if ($request->input('degree_diploma', false)) {
            $user->addMedia(storage_path('tmp/uploads/' . $request->input('degree_diploma')))->toMediaCollection('degree_diploma');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $user->id]);
        }

        return redirect()->route('admin.users.index');
    }

    public function edit(User $user)
    {
        abort_if(Gate::denies('user_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if(auth()->user()->roles->contains(2) && auth()->user()->organization_id != $user->organization_id){
            return "Вы не можете редактировать профайлы пользователей, не принадлежащих Вашей организации";
        }

        $roles = Role::all()->pluck('title', 'id');

        $organizations = Organization::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $user->load('roles', 'organization');

        return view('admin.users.edit', compact('roles', 'organizations', 'user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->all());
        $user->roles()->sync($request->input('roles', []));

        if ($request->input('photo', false)) {
            if (!$user->photo || $request->input('photo') !== $user->photo->file_name) {
                if ($user->photo) {
                    $user->photo->delete();
                }

                $user->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
            }
        } elseif ($user->photo) {
            $user->photo->delete();
        }

        if ($request->input('education_diploma', false)) {
            if (!$user->education_diploma || $request->input('education_diploma') !== $user->education_diploma->file_name) {
                if ($user->education_diploma) {
                    $user->education_diploma->delete();
                }

                $user->addMedia(storage_path('tmp/uploads/' . $request->input('education_diploma')))->toMediaCollection('education_diploma');
            }
        } elseif ($user->education_diploma) {
            $user->education_diploma->delete();
        }

        if ($request->input('degree_diploma', false)) {
            if (!$user->degree_diploma || $request->input('degree_diploma') !== $user->degree_diploma->file_name) {
                if ($user->degree_diploma) {
                    $user->degree_diploma->delete();
                }

                $user->addMedia(storage_path('tmp/uploads/' . $request->input('degree_diploma')))->toMediaCollection('degree_diploma');
            }
        } elseif ($user->degree_diploma) {
            $user->degree_diploma->delete();
        }

        return redirect()->route('admin.users.index');
    }

    public function show(User $user)
    {
        abort_if(Gate::denies('user_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if(auth()->user()->roles->contains(2) && auth()->user()->organization_id != $user->organization_id){
            return "Вы не можете просматривать профайлы пользователей, не принадлежащих Вашей организации";
        }

        $user->load('roles', 'organization', 'authorPositions');

        return view('admin.users.show', compact('user'));
    }

    public function destroy(User $user)
    {
        abort_if(Gate::denies('user_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if(auth()->user()->roles->contains(2) && auth()->user()->organization_id != $user->organization_id){
            return "Вы не можете удалять профайлы пользователей, не принадлежащих Вашей организации";
        }

        $user->delete();

        return back();
    }

    public function massDestroy(MassDestroyUserRequest $request)
    {
        User::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('user_create') && Gate::denies('user_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new User();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
