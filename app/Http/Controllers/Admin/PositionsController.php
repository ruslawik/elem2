<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyPositionRequest;
use App\Http\Requests\StorePositionRequest;
use App\Http\Requests\UpdatePositionRequest;
use App\Models\Doctrine;
use App\Models\Position;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use Auth;

class PositionsController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('position_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
                $query = Position::where('organization_id', Auth::user()->organization_id)->with(['author', 'doctrine'])->select(sprintf('%s.*', (new Position)->table));
            }else{
                $query = Position::with(['author', 'doctrine'])->select(sprintf('%s.*', (new Position)->table));
            }
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'position_show';
                $editGate      = 'position_edit';
                $deleteGate    = 'position_delete';
                $crudRoutePart = 'positions';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('approved', function ($row) {
                return $row->approved ? Position::APPROVED_SELECT[$row->approved] : '';
            });
            $table->addColumn('author_name', function ($row) {
                return $row->author ? $row->author->name : '';
            });

            $table->editColumn('title', function ($row) {
                return $row->title ? $row->title : "";
            });
            $table->addColumn('doctrine_name', function ($row) {
                return $row->doctrine ? $row->doctrine->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'author', 'doctrine']);

            return $table->make(true);
        }

        $users     = User::get();
        $doctrines = Doctrine::get();

        return view('admin.positions.index', compact('users', 'doctrines'));
    }

    public function create()
    {
        abort_if(Gate::denies('position_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $authors = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $doctrines = Doctrine::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.positions.create', compact('authors', 'doctrines'));
    }

    public function store(StorePositionRequest $request)
    {
        $position = Position::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $position->id]);
        }

        return redirect()->route('admin.positions.index');
    }

    public function edit(Position $position)
    {
        abort_if(Gate::denies('position_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $authors = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $doctrines = Doctrine::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $position->load('author', 'doctrine');

        return view('admin.positions.edit', compact('authors', 'doctrines', 'position'));
    }

    public function update(UpdatePositionRequest $request, Position $position)
    {
        $position->update($request->all());

        return redirect()->route('admin.positions.index');
    }

    public function show(Position $position)
    {
        abort_if(Gate::denies('position_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $position->load('author', 'doctrine', 'positionArguments');

        return view('admin.positions.show', compact('position'));
    }

    public function destroy(Position $position)
    {
        abort_if(Gate::denies('position_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $position->delete();

        return back();
    }

    public function massDestroy(MassDestroyPositionRequest $request)
    {
        Position::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('position_create') && Gate::denies('position_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Position();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
