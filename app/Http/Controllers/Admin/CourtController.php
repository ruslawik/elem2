<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyCourtRequest;
use App\Http\Requests\StoreCourtRequest;
use App\Http\Requests\UpdateCourtRequest;
use App\Models\Court;
use App\Models\Doctrine;
use App\Models\LawSystem;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class CourtController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('court_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courts = Court::all();

        return view('admin.courts.index', compact('courts'));
    }

    public function create()
    {
        abort_if(Gate::denies('court_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $doctrines = Doctrine::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $law_systems = LawSystem::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.courts.create', compact('doctrines', 'law_systems'));
    }

    public function store(StoreCourtRequest $request)
    {
        $court = Court::create($request->all());

        foreach ($request->input('file', []) as $file) {
            $court->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('file');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $court->id]);
        }

        return redirect()->route('admin.courts.index');
    }

    public function edit(Court $court)
    {
        abort_if(Gate::denies('court_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $doctrines = Doctrine::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $law_systems = LawSystem::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $court->load('doctrine', 'law_system');

        return view('admin.courts.edit', compact('doctrines', 'law_systems', 'court'));
    }

    public function update(UpdateCourtRequest $request, Court $court)
    {
        $court->update($request->all());

        if (count($court->file) > 0) {
            foreach ($court->file as $media) {
                if (!in_array($media->file_name, $request->input('file', []))) {
                    $media->delete();
                }
            }
        }

        $media = $court->file->pluck('file_name')->toArray();

        foreach ($request->input('file', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $court->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('file');
            }
        }

        return redirect()->route('admin.courts.index');
    }

    public function show(Court $court)
    {
        abort_if(Gate::denies('court_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $court->load('doctrine', 'law_system');

        return view('admin.courts.show', compact('court'));
    }

    public function destroy(Court $court)
    {
        abort_if(Gate::denies('court_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $court->delete();

        return back();
    }

    public function massDestroy(MassDestroyCourtRequest $request)
    {
        Court::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('court_create') && Gate::denies('court_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Court();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
