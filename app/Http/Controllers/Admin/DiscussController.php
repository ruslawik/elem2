<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyDiscussRequest;
use App\Http\Requests\StoreDiscussRequest;
use App\Http\Requests\UpdateDiscussRequest;
use App\Models\Discuss;
use App\Models\Institute;
use App\Models\LawSystem;
use App\Models\Npa;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use Auth;

class DiscussController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('discuss_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
                $query = Discuss::whereHas('npa', function($q){$q->where('organization_id', Auth::user()->organization_id);})->with(['npa', 'institute', 'jurisdiction'])->select(sprintf('%s.*', (new Discuss)->table));
            }else{
                $query = Discuss::with(['npa', 'institute', 'jurisdiction'])->select(sprintf('%s.*', (new Discuss)->table));
            }
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'discuss_show';
                $editGate      = 'discuss_edit';
                $deleteGate    = 'discuss_delete';
                $crudRoutePart = 'discusses';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('npa_name', function ($row) {
                return $row->npa ? $row->npa->name : '';
            });

            $table->addColumn('institute_name', function ($row) {
                return $row->institute ? $row->institute->name : '';
            });

            $table->addColumn('jurisdiction_name', function ($row) {
                return $row->jurisdiction ? $row->jurisdiction->name : '';
            });

            $table->editColumn('post_type', function ($row) {
                return $row->post_type ? Discuss::POST_TYPE_SELECT[$row->post_type] : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'npa', 'institute', 'jurisdiction']);

            return $table->make(true);
        }

        if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
            $npas       = Npa::where('organization_id', auth()->user()->organization_id)->get();
            $institutes = Institute::whereHas('npa', function($q){$q->where('organization_id', Auth::user()->organization_id);})->get();
        }else{
            $npas = Npa::get();
            $institutes = Institute::get();
        }
        
        $law_systems = LawSystem::get();

        return view('admin.discusses.index', compact('npas', 'institutes', 'law_systems'));
    }

    public function create()
    {
        abort_if(Gate::denies('discuss_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
            $npas = Npa::where('type', 'institute')->where('organization_id', Auth::user()->organization_id)->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        }else{
            $npas = Npa::where('type', 'institute')->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        }

        $institutes = Institute::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $jurisdictions = LawSystem::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.discusses.create', compact('npas', 'institutes', 'jurisdictions'));
    }

    public function store(StoreDiscussRequest $request)
    {
        $discuss = Discuss::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $discuss->id]);
        }

        return redirect()->route('admin.discusses.index');
    }

    public function edit(Discuss $discuss)
    {
/*        abort_if(Gate::denies('discuss_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
*/
        if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
            $npas = Npa::where('type', 'institute')->where('organization_id', Auth::user()->organization_id)->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        }else{
            $npas = Npa::where('type', 'institute')->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        }

        $institutes = Institute::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $jurisdictions = LawSystem::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $discuss->load('npa', 'institute', 'jurisdiction');

        return view('admin.discusses.edit', compact('npas', 'institutes', 'jurisdictions', 'discuss'));
    }

    public function update(UpdateDiscussRequest $request, Discuss $discuss)
    {
        $discuss->update($request->all());

        return redirect()->route('admin.discusses.index');
    }

    public function show(Discuss $discuss)
    {
        abort_if(Gate::denies('discuss_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $discuss->load('npa', 'institute', 'jurisdiction');

        return view('admin.discusses.show', compact('discuss'));
    }

    public function destroy(Discuss $discuss)
    {
        abort_if(Gate::denies('discuss_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $discuss->delete();

        return back();
    }

    public function massDestroy(MassDestroyDiscussRequest $request)
    {
        Discuss::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('discuss_create') && Gate::denies('discuss_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Discuss();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
