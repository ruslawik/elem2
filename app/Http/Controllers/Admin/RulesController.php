<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyRuleRequest;
use App\Http\Requests\StoreRuleRequest;
use App\Http\Requests\UpdateRuleRequest;
use App\Models\Npa;
use App\Models\Rule;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Auth;

class RulesController extends Controller
{
    use MediaUploadingTrait;

     public function select2Rules ($npa_id){

        $data = Rule::where('npa_id', $npa_id)->get();
        foreach ($data as $rule) {
            $select2[] = array("id"=>$rule->id, "text"=> $rule->name);
        }
        echo json_encode($select2);
    }

    public function index()
    {
        abort_if(Gate::denies('rule_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
            $rules = Rule::whereHas('npa', function($q){$q->where('organization_id', Auth::user()->organization_id);})->get();
        }else{
            $rules = Rule::all();
        }

        if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
            $npas       = Npa::where('organization_id', auth()->user()->organization_id)->get();
        }else{
            $npas = Npa::get();
        }

        return view('admin.rules.index', compact('rules', 'npas'));
    }

    public function create()
    {
        abort_if(Gate::denies('rule_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if(Auth::user()->roles->contains(2)){
            $npas = Npa::where('type', 'article')->where('organization_id', Auth::user()->organization_id)->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        }else{
            $npas = Npa::where('type', 'article')->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        }

        return view('admin.rules.create', compact('npas'));
    }

    public function store(StoreRuleRequest $request)
    {
        $rule = Rule::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $rule->id]);
        }

        return redirect()->route('admin.rules.index');
    }

    public function edit(Rule $rule)
    {
        abort_if(Gate::denies('rule_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $npas = Npa::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $rule->load('npa');

        return view('admin.rules.edit', compact('npas', 'rule'));
    }

    public function update(UpdateRuleRequest $request, Rule $rule)
    {
        $rule->update($request->all());

        return redirect()->route('admin.rules.index');
    }

    public function show(Rule $rule)
    {
        abort_if(Gate::denies('rule_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rule->load('npa');

        return view('admin.rules.show', compact('rule'));
    }

    public function destroy(Rule $rule)
    {
        abort_if(Gate::denies('rule_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rule->delete();

        return back();
    }

    public function massDestroy(MassDestroyRuleRequest $request)
    {
        Rule::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('rule_create') && Gate::denies('rule_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Rule();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
