<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyArgumentRequest;
use App\Http\Requests\StoreArgumentRequest;
use App\Http\Requests\UpdateArgumentRequest;
use App\Models\Argument;
use App\Models\Position;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class ArgumentsController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('argument_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Argument::with(['position'])->select(sprintf('%s.*', (new Argument)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'argument_show';
                $editGate      = 'argument_edit';
                $deleteGate    = 'argument_delete';
                $crudRoutePart = 'arguments';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('position_title', function ($row) {
                return $row->position ? $row->position->title : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'position']);

            return $table->make(true);
        }

        return view('admin.arguments.index');
    }

    public function create()
    {
        abort_if(Gate::denies('argument_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $positions = Position::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.arguments.create', compact('positions'));
    }

    public function store(StoreArgumentRequest $request)
    {
        $argument = Argument::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $argument->id]);
        }

        return redirect()->route('admin.arguments.index');
    }

    public function edit(Argument $argument)
    {
        abort_if(Gate::denies('argument_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $positions = Position::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        $argument->load('position');

        return view('admin.arguments.edit', compact('positions', 'argument'));
    }

    public function update(UpdateArgumentRequest $request, Argument $argument)
    {
        $argument->update($request->all());

        return redirect()->route('admin.arguments.index');
    }

    public function show(Argument $argument)
    {
        abort_if(Gate::denies('argument_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $argument->load('position');

        return view('admin.arguments.show', compact('argument'));
    }

    public function destroy(Argument $argument)
    {
        abort_if(Gate::denies('argument_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $argument->delete();

        return back();
    }

    public function massDestroy(MassDestroyArgumentRequest $request)
    {
        Argument::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('argument_create') && Gate::denies('argument_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Argument();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
