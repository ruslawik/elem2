<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyDoctrineRequest;
use App\Http\Requests\StoreDoctrineRequest;
use App\Http\Requests\UpdateDoctrineRequest;
use App\Models\Doctrine;
use App\Models\Razdel;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class DoctrineController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('doctrine_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $doctrines = Doctrine::all();

        return view('admin.doctrines.index', compact('doctrines'));
    }

    public function create()
    {
        abort_if(Gate::denies('doctrine_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $razdels = Razdel::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.doctrines.create', compact('razdels'));
    }

    public function store(StoreDoctrineRequest $request)
    {
        $doctrine = Doctrine::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $doctrine->id]);
        }

        return redirect()->route('admin.doctrines.index');
    }

    public function edit(Doctrine $doctrine)
    {
        abort_if(Gate::denies('doctrine_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $razdels = Razdel::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $doctrine->load('razdel');

        return view('admin.doctrines.edit', compact('razdels', 'doctrine'));
    }

    public function update(UpdateDoctrineRequest $request, Doctrine $doctrine)
    {
        $doctrine->update($request->all());

        return redirect()->route('admin.doctrines.index');
    }

    public function show(Doctrine $doctrine)
    {
        abort_if(Gate::denies('doctrine_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $doctrine->load('razdel');

        return view('admin.doctrines.show', compact('doctrine'));
    }

    public function destroy(Doctrine $doctrine)
    {
        abort_if(Gate::denies('doctrine_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $doctrine->delete();

        return back();
    }

    public function massDestroy(MassDestroyDoctrineRequest $request)
    {
        Doctrine::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('doctrine_create') && Gate::denies('doctrine_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Doctrine();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
