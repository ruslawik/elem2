<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyNpaRequest;
use App\Http\Requests\StoreNpaRequest;
use App\Http\Requests\UpdateNpaRequest;
use App\Models\Category;
use App\Models\Npa;
use App\Models\Organization;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use Auth;

class NpaController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('npa_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()){
            if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
                $query = Npa::where('organization_id', Auth::user()->organization_id)->with(['category', 'organization'])->select(sprintf('%s.*', (new Npa)->table));
            }
            else{
                $query = Npa::with(['category', 'organization'])->select(sprintf('%s.*', (new Npa)->table));
            }
            
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'npa_show';
                $editGate      = 'npa_edit';
                $deleteGate    = 'npa_delete';
                $crudRoutePart = 'npas';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('pod_name', function ($row) {
                return $row->pod_name ? $row->pod_name : "";
            });
            $table->addColumn('category_name', function ($row) {
                return $row->category ? $row->category->name : '';
            });

            $table->editColumn('type', function ($row) {
                return $row->type ? Npa::TYPE_SELECT[$row->type] : '';
            });
            $table->addColumn('organization_name', function ($row) {
                return $row->organization ? $row->organization->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'category', 'organization']);

            return $table->make(true);
        }

        return view('admin.npas.index');
    }

    public function create()
    {
        abort_if(Gate::denies('npa_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $organizations = Organization::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.npas.create', compact('categories', 'organizations'));
    }

    public function store(StoreNpaRequest $request)
    {
        $npa = Npa::create($request->all());
        $npa->setTranslation('name', 'ru', $request->input('name'))
                 ->setTranslation('name', 'kz', $request->input('name_kz'))
                 ->setTranslation('pod_name', 'ru', $request->input('pod_name'))
                 ->setTranslation('pod_name', 'kz', $request->input('pod_name_kz'))
                 ->save();

        return redirect()->route('admin.npas.index');
    }

    public function edit(Npa $npa)
    {
        abort_if(Gate::denies('npa_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if(auth()->user()->roles->contains(2) && auth()->user()->organization_id != $npa->organization_id){
            return "Вы не можете редактировать НПА, не принадлежащие Вашей организации";
        }

        $categories = Category::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $organizations = Organization::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $npa->load('category', 'organization');

        return view('admin.npas.edit', compact('categories', 'organizations', 'npa'));
    }

    public function update(UpdateNpaRequest $request, Npa $npa)
    {
        $npa->update($request->all());
        $npa->setTranslation('name', 'ru', $request->input('name'))
                 ->setTranslation('name', 'kz', $request->input('name_kz'))
                 ->setTranslation('pod_name', 'ru', $request->input('pod_name'))
                 ->setTranslation('pod_name', 'kz', $request->input('pod_name_kz'))
                 ->save();

        return redirect()->route('admin.npas.index');
    }

    public function show(Npa $npa)
    {
        abort_if(Gate::denies('npa_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if(auth()->user()->roles->contains(2) && auth()->user()->organization_id != $npa->organization_id){
            return "Вы не можете просматривать НПА, не принадлежащие Вашей организации";
        }

        if(auth()->user()->roles->contains(5) && auth()->user()->organization_id != $npa->organization_id){
            return "Вы не можете просматривать НПА, не принадлежащие Вашей организации";
        }

        $npa->load('category', 'organization', 'npaInstitutes');

        return view('admin.npas.show', compact('npa'));
    }

    public function destroy(Npa $npa)
    {
        abort_if(Gate::denies('npa_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if(auth()->user()->roles->contains(2) && auth()->user()->organization_id != $npa->organization_id){
            return "Вы не можете удалять НПА, не принадлежащие Вашей организации";
        }

        $npa->delete();

        return back();
    }

    public function massDestroy(MassDestroyNpaRequest $request)
    {
        Npa::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
