<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyLawSystemRequest;
use App\Http\Requests\StoreLawSystemRequest;
use App\Http\Requests\UpdateLawSystemRequest;
use App\Models\Institute;
use App\Models\LawSystem;
use App\Models\Numeration;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class LawSystemController extends Controller
{
    use MediaUploadingTrait;
    public function index(Request $request)
    {
        abort_if(Gate::denies('law_system_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = LawSystem::query()->select(sprintf('%s.*', (new LawSystem)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'law_system_show';
                $editGate      = 'law_system_edit';
                $deleteGate    = 'law_system_delete';
                $crudRoutePart = 'law-systems';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('avatar', function ($row) {
                if ($photo = $row->avatar) {
                    return sprintf(
                        '<a href="%s" target="_blank"><img src="%s" width="50px" height="50px"></a>',
                        $photo->url,
                        $photo->thumbnail
                    );
                }

                return '';
            });

            $table->rawColumns(['actions', 'placeholder', 'avatar']);

            return $table->make(true);
        }

        return view('admin.lawSystems.index');
    }

    public function create()
    {
        abort_if(Gate::denies('law_system_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.lawSystems.create');
    }

    public function store(StoreLawSystemRequest $request)
    {
        $lawSystem = LawSystem::create($request->all());

        if ($request->input('avatar', false)) {
            $lawSystem->addMedia(storage_path('tmp/uploads/' . $request->input('avatar')))->toMediaCollection('avatar');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $lawSystem->id]);
        }

        $institutes = Institute::all();
        foreach($institutes as $institute){
            Numeration::create([
                'institute_id' => $institute->id,
                'system_id' => $lawSystem->id
            ]);
        }
        return redirect()->route('admin.law-systems.index');
    }

    public function edit(LawSystem $lawSystem)
    {
        abort_if(Gate::denies('law_system_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.lawSystems.edit', compact('lawSystem'));
    }

    public function update(UpdateLawSystemRequest $request, LawSystem $lawSystem)
    {
        $lawSystem->update($request->all());

        if ($request->input('avatar', false)) {
            if (!$lawSystem->avatar || $request->input('avatar') !== $lawSystem->avatar->file_name) {
                if ($lawSystem->avatar) {
                    $lawSystem->avatar->delete();
                }

                $lawSystem->addMedia(storage_path('tmp/uploads/' . $request->input('avatar')))->toMediaCollection('avatar');
            }
        } elseif ($lawSystem->avatar) {
            $lawSystem->avatar->delete();
        }

        return redirect()->route('admin.law-systems.index');
    }

    public function show(LawSystem $lawSystem)
    {
        abort_if(Gate::denies('law_system_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.lawSystems.show', compact('lawSystem'));
    }

    public function destroy(LawSystem $lawSystem)
    {
        abort_if(Gate::denies('law_system_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $lawSystem->delete();

        Numeration::where('system_id', $lawSystem->id)->delete();

        return back();
    }

    public function massDestroy(MassDestroyLawSystemRequest $request)
    {
        LawSystem::whereIn('id', request('ids'))->delete();
        Numeration::whereIn('system_id', request('ids'))->delete();


        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('law_system_create') && Gate::denies('law_system_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new LawSystem();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
