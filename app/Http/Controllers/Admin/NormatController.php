<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyNormatRequest;
use App\Http\Requests\StoreNormatRequest;
use App\Http\Requests\UpdateNormatRequest;
use App\Models\Doctrine;
use App\Models\Normat;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class NormatController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('normat_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $normats = Normat::all();

        return view('admin.normats.index', compact('normats'));
    }

    public function create()
    {
        abort_if(Gate::denies('normat_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $doctrines = Doctrine::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.normats.create', compact('doctrines'));
    }

    public function store(StoreNormatRequest $request)
    {
        $normat = Normat::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $normat->id]);
        }

        return redirect()->route('admin.normats.index');
    }

    public function edit(Normat $normat)
    {
        abort_if(Gate::denies('normat_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $doctrines = Doctrine::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $normat->load('doctrine');

        return view('admin.normats.edit', compact('doctrines', 'normat'));
    }

    public function update(UpdateNormatRequest $request, Normat $normat)
    {
        $normat->update($request->all());

        return redirect()->route('admin.normats.index');
    }

    public function show(Normat $normat)
    {
        abort_if(Gate::denies('normat_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $normat->load('doctrine');

        return view('admin.normats.show', compact('normat'));
    }

    public function destroy(Normat $normat)
    {
        abort_if(Gate::denies('normat_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $normat->delete();

        return back();
    }

    public function massDestroy(MassDestroyNormatRequest $request)
    {
        Normat::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('normat_create') && Gate::denies('normat_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Normat();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
