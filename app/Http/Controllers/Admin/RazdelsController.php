<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRazdelRequest;
use App\Http\Requests\StoreRazdelRequest;
use App\Http\Requests\UpdateRazdelRequest;
use App\Models\Razdel;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RazdelsController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('razdel_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $razdels = Razdel::all();

        return view('admin.razdels.index', compact('razdels'));
    }

    public function create()
    {
        abort_if(Gate::denies('razdel_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parents = Razdel::where('parent_id', NULL)->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.razdels.create', compact('parents'));
    }

    public function store(StoreRazdelRequest $request)
    {
        $razdel = Razdel::create($request->all());

        return redirect()->route('admin.razdels.index');
    }

    public function edit(Razdel $razdel)
    {
        abort_if(Gate::denies('razdel_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parents = Razdel::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $razdel->load('parent');

        return view('admin.razdels.edit', compact('parents', 'razdel'));
    }

    public function update(UpdateRazdelRequest $request, Razdel $razdel)
    {
        $razdel->update($request->all());

        return redirect()->route('admin.razdels.index');
    }

    public function show(Razdel $razdel)
    {
        abort_if(Gate::denies('razdel_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $razdel->load('parent');

        return view('admin.razdels.show', compact('razdel'));
    }

    public function destroy(Razdel $razdel)
    {
        abort_if(Gate::denies('razdel_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $razdel->delete();

        return back();
    }

    public function massDestroy(MassDestroyRazdelRequest $request)
    {
        Razdel::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
