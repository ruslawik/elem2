<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyInstituteRequest;
use App\Http\Requests\StoreInstituteRequest;
use App\Http\Requests\UpdateInstituteRequest;
use App\Models\Institute;
use App\Models\LawSystem;
use App\Models\Npa;
use App\Models\Numeration;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use Auth;

class InstitutesController extends Controller
{


    public function updateNumeration(){

        $numerations = request('numerations');
        $system_id = request('system_id');
        $institute_id = request('institute_id');

        for($i = 0; $i < count($system_id); $i++){
            $exists = Numeration::where([
                ['system_id', $system_id[$i]],
                ['institute_id', $institute_id]
            ])->first();
            if($exists){
                $exists->update([
                    'num' => $numerations[$i],
                ]);
            }else{
                Numeration::create([
                    'system_id' => $system_id[$i],
                    'institute_id' => $institute_id,
                    'num' => $numerations[$i],
                ]);
            }
        }
        return redirect()->back();
    }
    public function select2Institutes ($npa_id){

        $data = Institute::where('npa_id', $npa_id)->get();
        foreach ($data as $institute) {
            $select2[] = array("id"=>$institute->id, "text"=> $institute->name);
        }
        echo json_encode($select2);
    }

    public function select2InstitutesParent ($npa_id){

        $data = Institute::where('npa_id', $npa_id)->where('parent_institute_id', NULL)->get();
        foreach ($data as $institute) {
            $select2[] = array("id"=>$institute->id, "text"=> $institute->name);
        }
        echo json_encode($select2);
    }

    public function index(Request $request)
    {
        abort_if(Gate::denies('institute_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
                $query = Institute::whereHas('npa', function($q){$q->where('organization_id', Auth::user()->organization_id);})->with(['npa', 'parent_institute'])->select(sprintf('%s.*', (new Institute)->table));
            }else{
                $query = Institute::with(['npa', 'parent_institute'])->select(sprintf('%s.*', (new Institute)->table));
            }
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'institute_show';
                $editGate      = 'institute_edit';
                $deleteGate    = 'institute_delete';
                $crudRoutePart = 'institutes';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->addColumn('npa_name', function ($row) {
                return $row->npa ? $row->npa->name : '';
            });

            $table->addColumn('parent_institute_name', function ($row) {
                return $row->parent_institute ? $row->parent_institute->name : '';
            });

            $table->editColumn('type', function ($row) {
                return $row->type ? Institute::TYPE_SELECT[$row->type] : '';
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
            
        }

        if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
            $npas       = Npa::where('organization_id', auth()->user()->organization_id)->get();
            $institutes = Institute::whereHas('npa', function($q){$q->where('organization_id', Auth::user()->organization_id);})->get();
        }else{
            $npas = Npa::get();
            $institutes = Institute::get();
        }

        return view('admin.institutes.index', compact('npas', 'institutes'));
    }

    public function create()
    {
        abort_if(Gate::denies('institute_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
            $npas = Npa::where('type', 'institute')->where('organization_id', Auth::user()->organization_id)->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        }else{
            $npas = Npa::where('type', 'institute')->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        }

        $parent_institutes = Institute::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.institutes.create', compact('npas', 'parent_institutes'));
    }

    public function store(StoreInstituteRequest $request)
    {

        
/*        THIS CODE WILL CHECK FOR EMPTY NUMERATIONS IN INSTITUTES, DO NOT DELETE
*/        /*$institutes = Institute::all();
        $systems = LawSystem::all();

        foreach($institutes as $institute){
            foreach($systems as $system){
                $numeration_exists = Numeration::where([
                    ['institute_id', $institute->id],
                    ['system_id', $system->id],
                ])->first();

                if(!$numeration_exists){
                    Numeration::create([
                        'institute_id' => $institute->id,
                        'system_id' => $system->id
                    ]);
                }
            }
        }*/
        $institute = Institute::create($request->all());
        $systems = LawSystem::all();

        foreach($systems as $system){
            Numeration::create([
                'institute_id' => $institute->id,
                'system_id' => $system->id
            ]);
        }

        return redirect()->route('admin.institutes.index');
    }

    public function edit(Institute $institute)
    {
        abort_if(Gate::denies('institute_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if(Auth::user()->roles->contains(2) || Auth::user()->roles->contains(5)){
            $npas = Npa::where('organization_id', auth()->user()->organization_id)
                    ->where('type', 'institute')
                    ->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');;
        }else{
            $npas = Npa::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        }

        $parent_institutes = Institute::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $institute->load('npa', 'parent_institute');

        $law_systems = LawSystem::whereHas('numeration', function($q) use ($institute){
            $q->where('institute_id', $institute->id);
        })->with('numeration')->first();

        if($law_systems){
            $law_systems = Numeration::where('institute_id', $institute->id)
                ->with('system')->get();

            $created = true;
        }else{
            $created = false;
            $law_systems = LawSystem::all();
        }


        return view('admin.institutes.edit', compact('npas', 'created', 'law_systems', 'parent_institutes', 'institute'));
    }

    public function update(UpdateInstituteRequest $request, Institute $institute)
    {
        $institute->update($request->all());

        return redirect()->route('admin.institutes.index');
    }

    public function show(Institute $institute)
    {
        abort_if(Gate::denies('institute_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $institute->load('npa', 'parent_institute');

        return view('admin.institutes.show', compact('institute'));
    }

    public function destroy(Institute $institute)
    {
        abort_if(Gate::denies('institute_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $institute->delete();

        return back();
    }

    public function massDestroy(MassDestroyInstituteRequest $request)
    {
        Institute::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
