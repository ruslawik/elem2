<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyFikirRequest;
use App\Http\Requests\StoreFikirRequest;
use App\Http\Requests\UpdateFikirRequest;
use App\Models\Doctrine;
use App\Models\Fikir;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class FikirsController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('fikir_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $fikirs = Fikir::all();

        return view('admin.fikirs.index', compact('fikirs'));
    }

    public function create()
    {
        abort_if(Gate::denies('fikir_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $doctrines = Doctrine::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.fikirs.create', compact('doctrines'));
    }

    public function store(StoreFikirRequest $request)
    {
        $fikir = Fikir::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $fikir->id]);
        }

        return redirect()->route('admin.fikirs.index');
    }

    public function edit(Fikir $fikir)
    {
        abort_if(Gate::denies('fikir_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $doctrines = Doctrine::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $fikir->load('doctrine');

        return view('admin.fikirs.edit', compact('doctrines', 'fikir'));
    }

    public function update(UpdateFikirRequest $request, Fikir $fikir)
    {
        $fikir->update($request->all());

        return redirect()->route('admin.fikirs.index');
    }

    public function show(Fikir $fikir)
    {
        abort_if(Gate::denies('fikir_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $fikir->load('doctrine');

        return view('admin.fikirs.show', compact('fikir'));
    }

    public function destroy(Fikir $fikir)
    {
        abort_if(Gate::denies('fikir_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $fikir->delete();

        return back();
    }

    public function massDestroy(MassDestroyFikirRequest $request)
    {
        Fikir::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('fikir_create') && Gate::denies('fikir_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Fikir();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
