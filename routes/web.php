<?php

Route::get('/settrans/{model}/{table}/{field}/{password}', 'FrontLocalController@ruslanSetLocal');

Route::get('/local/{locale}', 'FrontLocalController@setLang');

Route::get('fb-login/{provider}', 'Auth\LoginController@redirectToProvider')->name('social.login');
Route::get('fb-login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::any('/', 'Front\FrontController@index');
Route::get('/about', 'Front\FrontController@about');

Route::any('/all-docs/{cat_id}', 'Front\FrontController@allDocs');
Route::get('/fetch-docs/{cat_id}', 'Front\FrontController@fetchDocs');
Route::any('/doc/institutional/{doc_id}/institute/{inst_id}', 'Front\FrontController@docInstitutional');

Route::get('/doc/fetch-institutional/{doc_id}/institute/{inst_id}', 'Front\FrontController@fetch_institutional');

Route::any('/doc/by_article/{doc_id}/article/{article_id}', 'Front\FrontController@docByArticle');
Route::get('/fetch-article/{doc_id}/{article_id}', 'Front\FrontController@fetchArticle');

Route::any('/doc/by_article/{doc_id}/article/{article_id}/hide', 'Front\FrontController@docByArticle2');

Route::any('/library', 'Front\LibraryController@index');
Route::get('/library/fetch-doctrines/{razdel_id}', 'Front\LibraryController@fetch_doctrines');
Route::get('/library/{razdel_id}/doctrine/{doctrine_id}', 'Front\LibraryController@doctrinePage');
Route::get('/library/razdel/{razdel_id}/doctrine/{doctrine_id}', 'Front\LibraryController@doctrinePage2');

//All AJAX ROUTES
Route::post('/get_discussion', 'Front\FrontController@getDiscussion');
Route::post('/get_discussion_by_article', 'Front\DiscussionController@getDiscussionArticle');
Route::post('/get_contra_positions', 'Front\FrontController@getContraPositions');
Route::post('/get_compare_box_jurisdictions', 'Front\FrontController@getCompareBoxJurisdictions');
Route::post('/get_compare_box_institute', 'Front\FrontController@getCompareBoxInstitute');
Route::post('/get_compare_box_new', 'Front\FrontController@getCompareBoxNew');
Route::post('/get_doctrine', 'Front\FrontController@getDoctrine');
Route::post('/get_discussion_suggestion', 'Front\DiscussionController@getDiscussionSuggestions');
Route::post('/get_contra_positions_suggestions', 'Front\DiscussionController@getContraPositionsSuggestions');

Route::any('/error/{err_id}', 'Front\FrontController@error');

Route::any('/load_doctrine/{pos_id}', 'Front\FrontController@loadDoctrine');

//END ALL AJAX ROUTES

Route::group(['middleware' => ['authruslan']],
    function(){
    Route::any('/cabinet', 'Front\FrontController@cabinet')->name('cabinet');
    Route::post('/logout', 'Front\LoginController@logout')->name('logout');
    Route::any('/cabinet/positions', 'Front\FrontController@cabinetPositions');
    Route::resource('user', 'Front\FrontController');
    Route::post('/add_position', 'Front\FrontController@addPosition');
    Route::post('/edit_position', 'Front\FrontController@editPosition');
    Route::post('/add_comment', 'Front\FrontController@addComment');
});

Route::any('/team', 'Front\FrontController@Team');
Route::any('/base_docs', 'Front\FrontController@baseDocs');
Route::any('/faqs', 'Front\FrontController@Faqs');

Route::any('/loginform', 'Front\FrontController@loginForm')->name('loginform');
Route::any('/registration_form', 'Front\FrontController@registrationForm');
Route::any('/wall/{u_id}', 'Front\FrontController@publicProfile');

Route::any('/select2/institutes/{npa_id}', 'Front\FrontController@select2Institutes');
Route::any('/select2/npas', 'Front\FrontController@select2Npas');
Route::any('/inses_to_search/{npa_id}', 'Front\FrontController@insesToSearch');

Route::post('/front_register_post', 'Front\FrontController@frontReg');
Route::post('/reg/media', 'Front\FrontController@storeMedia')->name('front.reg.storeMedia');
Route::post('/front_login', 'Front\FrontController@frontLogin');



Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});


Route::any('/select2/institutes_parent/{npa_id}', 'Admin\InstitutesController@select2InstitutesParent');
Route::any('/select2/institutes/{npa_id}', 'Admin\InstitutesController@select2Institutes');
Route::any('/select2/rules/{npa_id}', 'Admin\RulesController@select2Rules');

Auth::routes();
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::post('users/media', 'UsersController@storeMedia')->name('users.storeMedia');
    Route::post('users/ckmedia', 'UsersController@storeCKEditorImages')->name('users.storeCKEditorImages');
    Route::resource('users', 'UsersController');

    // Organizations
    Route::delete('organizations/destroy', 'OrganizationsController@massDestroy')->name('organizations.massDestroy');
    Route::resource('organizations', 'OrganizationsController');

    // Npas
    Route::delete('npas/destroy', 'NpaController@massDestroy')->name('npas.massDestroy');
    Route::resource('npas', 'NpaController');

    // Institutes
    Route::delete('institutes/destroy', 'InstitutesController@massDestroy')->name('institutes.massDestroy');
    Route::resource('institutes', 'InstitutesController');

    Route::post('update-numeration', 'InstitutesController@updateNumeration')->name('update-numeration');


    Route::get('about', 'AboutController@edit')->name('about.edit');
    Route::post('about/update', 'AboutController@update')->name('about.update');

    // Discusses
    Route::delete('discusses/destroy', 'DiscussController@massDestroy')->name('discusses.massDestroy');
    Route::post('discusses/media', 'DiscussController@storeMedia')->name('discusses.storeMedia');
    Route::post('discusses/ckmedia', 'DiscussController@storeCKEditorImages')->name('discusses.storeCKEditorImages');
    Route::resource('discusses', 'DiscussController');

    // Law Systems
    Route::delete('law-systems/destroy', 'LawSystemController@massDestroy')->name('law-systems.massDestroy');
    Route::post('law-systems/media', 'LawSystemController@storeMedia')->name('law-systems.storeMedia');
    Route::post('law-systems/ckmedia', 'LawSystemController@storeCKEditorImages')->name('law-systems.storeCKEditorImages');
    Route::resource('law-systems', 'LawSystemController');


    // Positions
    Route::delete('positions/destroy', 'PositionsController@massDestroy')->name('positions.massDestroy');
    Route::post('positions/media', 'PositionsController@storeMedia')->name('positions.storeMedia');
    Route::post('positions/ckmedia', 'PositionsController@storeCKEditorImages')->name('positions.storeCKEditorImages');
    Route::resource('positions', 'PositionsController');

    // Arguments
    Route::delete('arguments/destroy', 'ArgumentsController@massDestroy')->name('arguments.massDestroy');
    Route::post('arguments/media', 'ArgumentsController@storeMedia')->name('arguments.storeMedia');
    Route::post('arguments/ckmedia', 'ArgumentsController@storeCKEditorImages')->name('arguments.storeCKEditorImages');
    Route::resource('arguments', 'ArgumentsController');

    // Categories
    Route::delete('categories/destroy', 'CategoryController@massDestroy')->name('categories.massDestroy');
    Route::resource('categories', 'CategoryController');

    // Articles
    Route::delete('articles/destroy', 'ArticleController@massDestroy')->name('articles.massDestroy');
    Route::post('articles/media', 'ArticleController@storeMedia')->name('articles.storeMedia');
    Route::post('articles/ckmedia', 'ArticleController@storeCKEditorImages')->name('articles.storeCKEditorImages');
    Route::resource('articles', 'ArticleController');

    // Rules
    Route::delete('rules/destroy', 'RulesController@massDestroy')->name('rules.massDestroy');
    Route::post('rules/media', 'RulesController@storeMedia')->name('rules.storeMedia');
    Route::post('rules/ckmedia', 'RulesController@storeCKEditorImages')->name('rules.storeCKEditorImages');
    Route::resource('rules', 'RulesController');

    // Suggestions
    Route::delete('suggestions/destroy', 'SuggestionsController@massDestroy')->name('suggestions.massDestroy');
    Route::post('suggestions/media', 'SuggestionsController@storeMedia')->name('suggestions.storeMedia');
    Route::post('suggestions/ckmedia', 'SuggestionsController@storeCKEditorImages')->name('suggestions.storeCKEditorImages');
    Route::resource('suggestions', 'SuggestionsController');

    // Razdels
    Route::delete('razdels/destroy', 'RazdelsController@massDestroy')->name('razdels.massDestroy');
    Route::resource('razdels', 'RazdelsController');

    // Doctrines
    Route::delete('doctrines/destroy', 'DoctrineController@massDestroy')->name('doctrines.massDestroy');
    Route::post('doctrines/media', 'DoctrineController@storeMedia')->name('doctrines.storeMedia');
    Route::post('doctrines/ckmedia', 'DoctrineController@storeCKEditorImages')->name('doctrines.storeCKEditorImages');
    Route::resource('doctrines', 'DoctrineController');

    // Normats
    Route::delete('normats/destroy', 'NormatController@massDestroy')->name('normats.massDestroy');
    Route::post('normats/media', 'NormatController@storeMedia')->name('normats.storeMedia');
    Route::post('normats/ckmedia', 'NormatController@storeCKEditorImages')->name('normats.storeCKEditorImages');
    Route::resource('normats', 'NormatController');

    // Courts
    Route::delete('courts/destroy', 'CourtController@massDestroy')->name('courts.massDestroy');
    Route::post('courts/media', 'CourtController@storeMedia')->name('courts.storeMedia');
    Route::post('courts/ckmedia', 'CourtController@storeCKEditorImages')->name('courts.storeCKEditorImages');
    Route::resource('courts', 'CourtController');

    // Fikirs
    Route::delete('fikirs/destroy', 'FikirsController@massDestroy')->name('fikirs.massDestroy');
    Route::post('fikirs/media', 'FikirsController@storeMedia')->name('fikirs.storeMedia');
    Route::post('fikirs/ckmedia', 'FikirsController@storeCKEditorImages')->name('fikirs.storeCKEditorImages');
    Route::resource('fikirs', 'FikirsController');

     // Comments
    Route::delete('comments/destroy', 'CommentController@massDestroy')->name('comments.massDestroy');
    Route::post('comments/media', 'CommentController@storeMedia')->name('comments.storeMedia');
    Route::post('comments/ckmedia', 'CommentController@storeCKEditorImages')->name('comments.storeCKEditorImages');
    Route::resource('comments', 'CommentController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});


