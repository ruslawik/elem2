<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::post('users/media', 'UsersApiController@storeMedia')->name('users.storeMedia');
    Route::apiResource('users', 'UsersApiController');

    // Organizations
    Route::apiResource('organizations', 'OrganizationsApiController');

    // Npas
    Route::apiResource('npas', 'NpaApiController');

    // Institutes
    Route::apiResource('institutes', 'InstitutesApiController');

    // Discusses
    Route::post('discusses/media', 'DiscussApiController@storeMedia')->name('discusses.storeMedia');
    Route::apiResource('discusses', 'DiscussApiController');

    // Law Systems
    Route::post('law-systems/media', 'LawSystemApiController@storeMedia')->name('law-systems.storeMedia');
    Route::apiResource('law-systems', 'LawSystemApiController');

    // Positions
    Route::post('positions/media', 'PositionsApiController@storeMedia')->name('positions.storeMedia');
    Route::apiResource('positions', 'PositionsApiController');

    // Arguments
    Route::post('arguments/media', 'ArgumentsApiController@storeMedia')->name('arguments.storeMedia');
    Route::apiResource('arguments', 'ArgumentsApiController');

    // Categories
    Route::apiResource('categories', 'CategoryApiController');

    // Articles
    Route::post('articles/media', 'ArticleApiController@storeMedia')->name('articles.storeMedia');
    Route::apiResource('articles', 'ArticleApiController');

    // Rules
    Route::post('rules/media', 'RulesApiController@storeMedia')->name('rules.storeMedia');
    Route::apiResource('rules', 'RulesApiController');

    // Suggestions
    Route::post('suggestions/media', 'SuggestionsApiController@storeMedia')->name('suggestions.storeMedia');
    Route::apiResource('suggestions', 'SuggestionsApiController');

    // Razdels
    Route::apiResource('razdels', 'RazdelsApiController');

    // Doctrines
    Route::post('doctrines/media', 'DoctrineApiController@storeMedia')->name('doctrines.storeMedia');
    Route::apiResource('doctrines', 'DoctrineApiController');

    // Normats
    Route::post('normats/media', 'NormatApiController@storeMedia')->name('normats.storeMedia');
    Route::apiResource('normats', 'NormatApiController');

    // Courts
    Route::post('courts/media', 'CourtApiController@storeMedia')->name('courts.storeMedia');
    Route::apiResource('courts', 'CourtApiController');

    // Fikirs
    Route::post('fikirs/media', 'FikirsApiController@storeMedia')->name('fikirs.storeMedia');
    Route::apiResource('fikirs', 'FikirsApiController');
});
