<div id="sidebar" class="c-sidebar c-sidebar-fixed c-sidebar-lg-show">

    <div class="c-sidebar-brand d-md-down-none">
        <a class="c-sidebar-brand-full h4" href="#">
            <img src="/front/assets/images/logokazguu.png" width=60>
        </a>
    </div>

    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.home") }}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-tachometer-alt">

                </i>
                {{ trans('global.dashboard') }}
            </a>
        </li>
        @can('user_management_access')
            <li class="c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-users c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.userManagement.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('permission_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.permissions.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/permissions") || request()->is("admin/permissions/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-unlock-alt c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.permission.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('role_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.roles.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/roles") || request()->is("admin/roles/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-briefcase c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.role.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('user_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.users.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/users") || request()->is("admin/users/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-user c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.user.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('organization_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.organizations.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/organizations") || request()->is("admin/organizations/*") ? "active" : "" }}">
                    <i class="fa-fw fas fa-sitemap c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.organization.title') }}
                </a>
            </li>
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.about.edit") }}" class="c-sidebar-nav-link {{ request()->is("admin/about") || request()->is("admin/about/*") ? "active" : "" }}">
                    <i class="fa-fw fas fa-sitemap c-sidebar-nav-icon">

                    </i>
                    О нас
                </a>
            </li>
        @endcan
        @can('npa_manage_access')
            <li class="c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.npaManage.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('category_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.categories.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/categories") || request()->is("admin/categories/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-list-ol c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.category.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('npa_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.npas.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/npas") || request()->is("admin/npas/*") ? "active" : "" }}">
                                <i class="fa-fw far fa-file-alt c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.npa.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('institutional_npa_access')
            <li class="c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-university c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.institutionalNpa.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('law_system_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.law-systems.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/law-systems") || request()->is("admin/law-systems/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-gavel c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.lawSystem.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('institute_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.institutes.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/institutes") || request()->is("admin/institutes/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-bezier-curve c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.institute.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('discuss_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.discusses.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/discusses") || request()->is("admin/discusses/*") ? "active" : "" }}">
                                <i class="fa-fw fab fa-discord c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.discuss.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('postateika_access')
            <li class="c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-file-alt c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.postateika.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('rule_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.rules.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/rules") || request()->is("admin/rules/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-list-ol c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.rule.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('suggestion_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.suggestions.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/suggestions") || request()->is("admin/suggestions/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-users c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.suggestion.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('library_access')
            <li class="c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-book-open c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.library.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('razdel_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.razdels.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/razdels") || request()->is("admin/razdels/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-align-left c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.razdel.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('doctrine_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.doctrines.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/doctrines") || request()->is("admin/doctrines/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-book c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.doctrine.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('normat_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.normats.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/normats") || request()->is("admin/normats/*") ? "active" : "" }}">
                                <i class="fa-fw far fa-check-circle c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.normat.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('court_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.courts.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/courts") || request()->is("admin/courts/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-gavel c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.court.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('fikir_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.fikirs.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/fikirs") || request()->is("admin/fikirs/*") ? "active" : "" }}">
                                <i class="fa-fw fas fa-user-alt c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.fikir.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('position_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.positions.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/positions") || request()->is("admin/positions/*") ? "active" : "" }}">
                    <i class="fa-fw fas fa-crosshairs c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.position.title') }}
                </a>
            </li>
        @endcan
        @can('argument_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.arguments.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/arguments") || request()->is("admin/arguments/*") ? "active" : "" }}">
                    <i class="fa-fw fas fa-balance-scale c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.argument.title') }}
                </a>
            </li>
        @endcan
        @can('comment_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.comments.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/comments") || request()->is("admin/comments/*") ? "active" : "" }}">
                    <i class="fa-fw fas fa-comment c-sidebar-nav-icon">

                    </i>
                    Комментарии
                </a>
            </li>
        @endcan
        @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
            @can('profile_password_edit')
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'active' : '' }}" href="{{ route('profile.password.edit') }}">
                        <i class="fa-fw fas fa-key c-sidebar-nav-icon">
                        </i>
                        {{ trans('global.change_password') }}
                    </a>
                </li>
            @endcan
        @endif
        <li class="c-sidebar-nav-item">
            <a href="#" class="c-sidebar-nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                <i class="c-sidebar-nav-icon fas fa-fw fa-sign-out-alt">

                </i>
                {{ trans('global.logout') }}
            </a>
        </li>
    </ul>

</div>
