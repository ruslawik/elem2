<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="irstheme">

    <title>Университет КАЗГЮУ - Обсуждение НПА</title>

    <link href="/front/assets/css/themify-icons.css" rel="stylesheet">
    <link href="/front/assets/css/flaticon.css" rel="stylesheet">
    <link href="/front/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/front/assets/css/animate.css" rel="stylesheet">
    <link href="/front/assets/css/owl.carousel.css" rel="stylesheet">
    <link href="/front/assets/css/owl.theme.css" rel="stylesheet">
    <link href="/front/assets/css/slick.css" rel="stylesheet">
    <link href="/front/assets/css/slick-theme.css" rel="stylesheet">
    <link href="/front/assets/css/swiper.min.css" rel="stylesheet">
    <link href="/front/assets/css/owl.transitions.css" rel="stylesheet">
    <link href="/front/assets/css/jquery.fancybox.css" rel="stylesheet">
    <link href="/front/assets/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- start page-wrapper -->
    <div class="page-wrapper">

    <!-- start preloader -->
    <div class="preloader">
        <div class="sk-chase">
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
        </div>
    </div>
    <!-- end preloader -->



        <!-- start of hero -->
        <section class="hero-slider hero-style-1">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="slide-inner slide-bg-image" data-background="/front/assets/images/slider/slide-1.jpg" data-text="<table style='margin-left:-30%;margin-top:-3px;padding:0;'><tr><td><img src='/front/assets/images/icons/contract.png' width=60></td><td width='10'></td><td>Цивилистика<h4>Гражданский кодекс РК</h4></td></tr></table>">
                            <div class="container">
                                <div data-swiper-parallax="300" class="slide-title">
                                    <h2>Гражданский кодекс РК</h2>
                                </div>
                                <div data-swiper-parallax="400" class="slide-text">
                                    <p>Платформа для обсуждения онлайн</p>
                                </div>
                                <div class="clearfix"></div>
                                <div data-swiper-parallax="500" class="slide-btns">
                                    <a href="NPApage.html" class="theme-btn">Перейти</a>
                                </div>
                                <div class="video-btns">
                                    <a href="https://www.youtube.com/embed/7e90gBu4pas?autoplay=1" class="video-btn" data-type="iframe" tabindex="0"></a>
                                </div>
                            </div>
                        </div> <!-- end slide-inner -->
                    </div> <!-- end swiper-slide -->

                    <div class="swiper-slide">
                        <div class="slide-inner slide-bg-image" data-background="/front/assets/images/slider/slide-2.jpg" data-text="<table style='margin-left:-30%;margin-top:-3px;padding:0;'><tr><td><img src='/front/assets/images/icons/ground.png' width=60></td><td width='10'></td><td>Земельное право<h4>Земельный кодекс РК</h4></td></tr></table>">
                            <div class="container">
                                <div data-swiper-parallax="300" class="slide-title">
                                    <h2>Земельный кодекс</h2>
                                </div>
                                <div data-swiper-parallax="400" class="slide-text">
                                    <p>Онлайн обсуждение</p>
                                </div>
                                <div class="clearfix"></div>
                                <div data-swiper-parallax="500" class="slide-btns">
                                    <a href="NPApage.html" class="theme-btn">Перейти</a>
                                </div>
                                <div class="video-btns">
                                    <a href="https://www.youtube.com/embed/7e90gBu4pas?autoplay=1" class="video-btn" data-type="iframe" tabindex="0"></a>
                                </div>
                            </div>
                        </div> <!-- end slide-inner -->
                    </div> <!-- end swiper-slide -->

                    <div class="swiper-slide">
                        <div class="slide-inner slide-bg-image" data-background="/front/assets/images/slider/slide-3.jpg" data-text="<table style='margin-left:-30%;margin-top:-3px;padding:0;'><tr><td><img src='/front/assets/images/icons/crime.png' width=60></td><td width='10'></td><td>Уголовное право<h4>Уголовный кодекс РК</h4></td></tr></table>">
                            <div class="container">
                                <div data-swiper-parallax="300" class="slide-title">
                                    <h2>Уголовный кодекс РК</h2>
                                </div>
                                <div data-swiper-parallax="400" class="slide-text">
                                    <p>Онлайн обсуждение</p>
                                </div>
                                <div class="clearfix"></div>
                                <div data-swiper-parallax="500" class="slide-btns">
                                    <a href="NPApage.html" class="theme-btn">Перейти</a>
                                </div>
                                <div class="video-btns">
                                    <a href="https://www.youtube.com/embed/7e90gBu4pas?autoplay=1" class="video-btn" data-type="iframe" tabindex="0"></a>
                                </div>
                            </div>
                        </div> <!-- end slide-inner -->
                    </div><!-- end swiper-slide -->

                    <div class="swiper-slide">
                        <div class="slide-inner slide-bg-image" data-background="/front/assets/images/slider/slide-4.jpg" data-text="<table style='margin-left:-30%;margin-top:-3px;padding:0;'><tr><td><img src='/front/assets/images/icons/money.png' width=60></td><td width='10'></td><td>Предпринимательство<h4>Предпринимательский кодекс РК</h4></td></tr></table>">
                            <div class="container">
                                <div data-swiper-parallax="300" class="slide-title">
                                    <h2>Предпринимательский кодекс</h2>
                                </div>
                                <div data-swiper-parallax="400" class="slide-text">
                                    <p>Платформа онлайн-обсуждения</p>
                                </div>
                                <div class="clearfix"></div>
                                <div data-swiper-parallax="500" class="slide-btns">
                                    <a href="NPApage.html" class="theme-btn">Перейти</a>
                                </div>
                                <div class="video-btns">
                                    <a href="https://www.youtube.com/embed/7e90gBu4pas?autoplay=1" class="video-btn" data-type="iframe" tabindex="0"></a>
                                </div>
                            </div>
                        </div> <!-- end slide-inner -->
                    </div><!-- end swiper-slide -->
                </div>
                <!-- end swiper-wrapper -->

                <!-- swipper controls -->
                <div class="swiper-cust-pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </section>
        <!-- end of hero slider -->


        <!-- start about-section -->
        <section class="about-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-12">
                        <div class="left-col">
                            <div class="section-title">
                                <div class="icon">
                                    <i class="fi flaticon-home-3"></i>
                                </div>
                                <span>Основная информация</span>
                                <h2>О платформе</h2>
                                <p>Начата работа над созданием перечня доктрин с описанием для нужд правоприменения – «ELEMENTA». Название выбрано по аналогии с «Началами» («Elementa») Эвклида — главным его трудом, фундаментальной основой математики в античное время. Такое название избегает копирования других юристов и трудов (Глосса, многочисленные комментарии, Институции и так далее), но связывает проект с античностью, давшей начало современной цивилизации, указывает на его научность и фундаментальность, является звучным и хорошо запоминающимся. Схема изложения ELEMENTA предлагается институционная: лица, вещи, обязательства. Возможно добавление раздела «Общие положения». Руководителем данного проекта является adjunct professor Университета КАЗГЮУ имени М.С.Нарикбаева Арман Шайкенов.</p>
                                <h2>Партнеры</h2>
                                <img src="/front/assets/images/partners/p1.png" width=500>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>


                <!-- start site-footer -->
        <footer class="site-footer">
            <div class="social-newsletter-area">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <div class="social-newsletter-content clearfix">
                                <div class="social-area">
                                    <ul class="clearfix">
                                        <li><a href="#"><i class="ti-facebook"></i></a></li>
                                        <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                                        <li><a href="#"><i class="ti-linkedin"></i></a></li>
                                        <li><a href="#"><i class="ti-instagram"></i></a></li>
                                    </ul>
                                </div>
                                <div class="logo-area">
                                    <img src="/front/assets/images/logokazguu.png" width="120" alt>
                                </div>
                                <div class="newsletter-area">
                                    <div class="inner">
                                        <h3>Рассылка</h3>
                                        <form>
                                            <div class="input-1">
                                                <input type="email" class="form-control" placeholder="Эл. почта *" required="">
                                            </div>
                                            <div class="submit clearfix">
                                                <button type="submit"><i class="fi flaticon-paper-plane"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lower-footer">
                <div class="container">
                    <div class="row">
                        <div class="separator"></div>
                        <div class="col col-xs-12">
                            <p class="copyright">Copyright &copy; 2020 KAZGUU University. All rights reserved.</p>
                            <div class="extra-link">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>



    </div>
    <!-- end of page-wrapper -->



    <!-- All JavaScript files
    ================================================== -->
    <script src="/front/assets/js/jquery.min.js"></script>
    <script src="/front/assets/js/bootstrap.min.js"></script>

    <!-- Plugins for this template -->
    <script src="/front/assets/js/jquery-plugin-collection.js"></script>

    <!-- Custom script for this template -->
    <script src="/front/assets/js/script.js"></script>
</body>
</html>
