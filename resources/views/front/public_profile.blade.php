@extends('front.layouts.page_layout')
@section('content')
    

           <!-- start page-title -->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>{{ $user_data['0']->surname }} {{ $user_data['0']->name }} {{ $user_data['0']->last_name }}</h2>
                        <p>
                             @if($user_data['0']->roles->contains(3))
                                Статус: обозреватель
                             @else
                                 Статус: эксперт
                             @endif
                        </p>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end page-title -->

<!-- start team-sigle-section -->
        <section class="team-sigle-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-12">
                        <div class="attorney-single-content">
                            <div class="attorney">
                                    <table class="table table-striped">
                                    @foreach($positions as $position)
                                                <tr>
                                                 <td style="padding: 20px;" class="position_{{$position['id']}}">
                                                    @if($position->approved == "moder")
                                                        <div class="alert alert-warning">
                                                                <ul>
                                                                    <li>На модерации</li>
                                                                </ul>
                                                            </div>
                                                    @else
                                                        <div class="alert alert-success">
                                                                <ul>
                                                                    <li>Опубликовано</li>
                                                                </ul>
                                                            </div>
                                                    @endif
                                                    @if(isset($position->author->photo))
                                                        <img id="discussion_avatar" src="{{$position->author->photo['url']}}"> &nbsp;&nbsp;&nbsp;
                                                    @else
                                                        <img id="discussion_avatar" src="/front/assets/images/avatar_none.png"> &nbsp;&nbsp;&nbsp; 
                                                    @endif

                                                    <text style="font-size: 1rem;font-weight: 400;margin: 0 0 0.2em;color: #c9b38c;">{{$position->author->surname}}
                                                    {{$position->author->name}} 
                                                    {{$position->author->last_name}}</text>
                                                    <br><br>
                                                    {!!$position['position']!!}
                                                    <?php $arg_num = 1; ?>

                                                     <a style="cursor:pointer;" onClick="load_doctrine({{$position['id']}});"><b>Доктрина по позиции</b></a> 
                                                            |
                                                            <a style="cursor:pointer;color:red;" onClick="load_contra({{$position['id']}});"><b>Позиции против</b></a>

                                                            <hr>

                                                            @foreach ($position->positionArguments as $argument)
                                                                <img src="/front/assets/images/tick.jpg" width=20> <b>Аргумент {{$arg_num}}: </b>
                                                                {!!$argument['argument']!!}
                                                                <?php $arg_num++; ?>
                                                                <br>
                                                            @endforeach 
                                                </td>
                                                </tr>
                                            @endforeach
                                        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end team-sigle-section -->



@endsection

@section('js')
<script type="text/javascript">
    function load_doctrine (position_id){
                $('.doctrine_content').load("/doctrine/"+position_id, function(){
                    $('#doctrine').modal({show:true});
                });
            }
            function load_contra (position_id){
                $('.doctrine_content').load("/contra_pos/"+position_id, function(){
                    $('#doctrine').modal({show:true});
                });
            }
</script>
@endsection

@section('modals')
<div class="modal fade doctrine" id="doctrine" tabindex="-1" role="dialog" aria-labelledby="doctrine" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding: 40px !important">
                    <h2></h2>
                    <p class="doctrine_content"></p>
            </div>
        </div>
</div>
@endsection