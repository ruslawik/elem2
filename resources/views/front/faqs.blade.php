@extends('front.layouts.page_layout')
@section('content')
        <!-- start page-title -->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>{{__('navmenu.faqs_uzun')}}</h2>
                        <p>{{__('navmenu.read_nizhe')}}</p>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>        
        <!-- end page-title -->


        <!-- start blog-single-section -->
        <section class="blog-single-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-12">
                        <div class="blog-content">
                            <div class="post format-standard-image">
                                <div class="date-entry-meta">
                                    <div class="cat">{{__('navmenu.suraktarga')}}</div>
                                </div>
                                <h2>{{__('navmenu.faqs_uzun')}}</h2>
                                <p>
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
