@extends('front.layouts.page_layout')
@section('content')


           <!-- start page-title -->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>{{ __('navmenu.vxod_reg')}}</h2>
                        <p>{{ __('navmenu.kir_or_reg')}}</p>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end page-title -->

<!-- start shop-single-section -->
        <section class="shop-single-section" style="margin-bottom: 50px;">
            <div class="container">
                <div class="row">
                        <div class="product-info">
                        <div class="col col-md-3">
                        </div>

                        <div class="col col-md-6 review-form-wrapper">

                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active"><a href="/loginform" style="cursor:pointer;">{{ __('navmenu.kir')}}</a></li>
                                <li><a href="/registration_form" style="cursor:pointer;">{{ __('navmenu.reg')}}</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade">
                                    <div class="review-form">
                                                <form>
                                                    <div>
                                                        <input type="email" class="form-control" placeholder="{{ __('navmenu.email')}} *" required>
                                                    </div>
                                                    <div>
                                                        <input type="password" class="form-control" placeholder="{{ __('navmenu.password')}} *" required>
                                                    </div>
                                                        <div class="submit">
                                                            <button type="submit" class="theme-btn">{{ __('navmenu.kir')}}</button>
                                                        </div>
                                                </form>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade in active">
                                    <div class="review-form">
                                        <div class="alert alert-info" role="alert">
                                            <h4 class="alert-heading">Уважаемый пользователь!</h4>
<p>Просим Вас пройти регистрацию на платформе – это недолго.</p>
<p>Зарегистрированные пользователи получат возможность не только просматривать контент, но и активно пользоваться возможностями платформы, комментировать и выражать аргументированные позиции.
</p>
<p>
Роли делятся на два вида – «Обозреватель» и «Эксперт».</p><p>

Обозревателю в дискуссии доступно только комментирование тех или иных правовых институтов, норм или доктрин. Доступна регистрация с помощью профиля в Facebook.</p>
<p>

Эксперту в дискуссии доступно не только комментирование тех или иных правовых институтов, норм или доктрин, но и выражение аргументированных позиций. Позиции имеют обязательный атрибут «PRO» или «CONTRA» и служат важным элементом общедоступного контента платформы, так как имеют не только научный, но и обучающий эффект. Для регистрации в качестве эксперта потребуется загрузить отсканированные версии дипломов об образовании/ученой степени.</p>
<hr>
<p class="mb-0">Для регистрации организации с правом администрирования конкретных нормативных правовых актов или их проектов обратитесь к администратору платформы по e-mail: pen.sergei@kazguu.kz</p>
                                        </div>
                                        @if (\Session::has('success'))
                                                <div class="alert alert-success">
                                                        <ul>
                                                            <li>{!! \Session::get('success') !!}</li>
                                                        </ul>
                                                </div>
                                        @else

                                        @if($errors->any())
                                            {!! implode('', $errors->all('<div id="errors" style="color:red;">:message</div>')) !!}
                                        @endif
                                        <br>
                                                <form name="reg_form" method="POST" action="{{$action}}" enctype="multipart/form-data">
                                                    @csrf
                                                    <!--<input type="hidden" id="roles" name="roles[]" value="3">!-->
                                                    <div>
                                                        <select name="roles[]" class="form-control" onchange="change_reg_form();" id="roles">
                                                            <option value="3">{{ __('navmenu.obozrev')}}</option>
                                                            <option value="4">{{ __('navmenu.expert')}}</option>
                                                        </select>
                                                    </div>
                                                    <div>
                                                        {{ __('navmenu.name')}}
                                                        <input name="name" type="text" class="form-control" placeholder="Кайрат *" value="{{ old('name', '') }}" required>
                                                    </div>
                                                    <div>
                                                        {{ __('navmenu.surname')}}
                                                        <input name="surname" type="text" class="form-control" placeholder="Сериков *" value="{{ old('surname', '') }}" required>
                                                    </div>
                                                    <div>
                                                        {{ __('navmenu.lastname')}}
                                                        <input name="last_name" type="text" class="form-control" value="{{ old('last_name', '') }}"placeholder="Канатулы">
                                                    </div>
                                                    <div>
                                                        {{ __('navmenu.phone')}}
                                                        <input name="phone" type="text" class="form-control" placeholder="+7 777 77 77 *" value="{{ old('phone', '') }}" required>
                                                    </div>
                                                    <div>
                                                        {{ __('navmenu.email')}}
                                                        <input name="email" type="email" class="form-control" placeholder="{{ __('navmenu.email')}} *" value="{{ old('email', '') }}" required>
                                                    </div>
                                                    <div>
                                                        {{ __('navmenu.password')}}
                                                        <input name="password" type="password" class="form-control" required>
                                                    </div>
                                                    <div id="additional_reg_fields" style="display:none;">
                                                    <div>
                                                        {{ __('navmenu.zhumys')}}
                                                        <input name="job_place" type="text" class="form-control" value="{{ old('job_place', '') }}" placeholder="Университет КАЗГЮУ">
                                                    </div>
                                                    <br>
                                                    <div class="form-group">
                <label for="education_diploma">{{ trans('cruds.user.fields.education_diploma') }}</label>
                <div class="needsclick dropzone {{ $errors->has('education_diploma') ? 'is-invalid' : '' }}" id="education_diploma-dropzone">
                </div>
                @if($errors->has('education_diploma'))
                    <div class="invalid-feedback">
                        {{ $errors->first('education_diploma') }}
                    </div>
                @endif
            </div>
            <br>
            <div class="form-group">
                <label for="degree_diploma">{{ trans('cruds.user.fields.degree_diploma') }}</label>
                <div class="needsclick dropzone {{ $errors->has('degree_diploma') ? 'is-invalid' : '' }}" id="degree_diploma-dropzone">
                </div>
                @if($errors->has('degree_diploma'))
                    <div class="invalid-feedback">

                    </div>
                @endif
            </div>
                                                    </div>

                <input type="hidden" id="degree_hidden" value="0">
                <input type="hidden" id="education_hidden" value="0">
                                                    <!--
                                                    <div>
                                                        <textarea class="form-control" placeholder="Review *"></textarea>
                                                    </div>!-->


                                                            <button type="submit" class="theme-btn" value="{{ __('navmenu.reg')}}">{{ __('navmenu.reg')}}</button>

                                                </form>
                                            @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end of shop-single-section -->


@endsection

@section('js')
    <script>
        $(document).ready(function() {
                $('form[name=reg_form]').submit(function() {
                    if($("#roles").val() == 4 && ($("#degree_hidden").val() == 0 || $("#education_hidden").val() == 0)){
                        alert("Не загружен скан одного из дипломов!");
                        return false;
                    }
                });
            });
    </script>
    <script>
        function change_reg_form() {
            $("#additional_reg_fields").toggle();
        }
    </script>

    <script>
    Dropzone.options.educationDiplomaDropzone = {
    url: '{{ route('front.reg.storeMedia') }}',
    maxFilesize: 30, // MB
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 4
    },
    success: function (file, response) {
      $('form').find('input[name="education_diploma"]').remove()
      $('form').append('<input type="hidden" name="education_diploma" value="' + response.name + '">')
      $('#education_hidden').val(1);
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="education_diploma"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
<script>
    Dropzone.options.degreeDiplomaDropzone = {
    url: '{{ route('front.reg.storeMedia') }}',
    maxFilesize: 30, // MB
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 4
    },
    success: function (file, response) {
      $('form').find('input[name="degree_diploma"]').remove()
      $('form').append('<input type="hidden" name="degree_diploma" value="' + response.name + '">')
      $('#degree_hidden').val(1);
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="degree_diploma"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection
