@extends('front.layouts.page_layout')
@section('content')
         <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                         <a name="top"></a>  
                         <h2>{{ $institute_data[0]['name'] }}</h2>
                        <p>{{ $institute_data[0]->npa['name'] }}</p>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>        
        <!-- end page-title -->

        <!-- start service-single-section -->
        <section class="service-single-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-9 col-md-push-3">
                        <div class="service-single-content">
                            <a name="top2"></a>
                            <h4>{{ $institute_data[0]->npa['name'] }}: обсуждение "{{ $institute_data[0]['name'] }}" в "{{$discuss_rule_data['0']['name']}}"</h4>
                            <hr>
                             @foreach ($discuss_rules as $rule)
                                @if($rule->id == $discuss_rule_data['0']['id'])
                                    <a class="theme-btn discussion_button_current"><img src="{{$rule->avatar['url']}}" width="20px">&nbsp;&nbsp;{{$rule->name}}</a>
                                @else
                                    <a href="/institute/{{$institute_data[0]['id']}}/discussion/{{$rule->id}}/#top2" class="theme-btn discussion_button"><img src="{{$rule->avatar['url']}}" width="20px">&nbsp;&nbsp;{{$rule->name}}</a>
                                @endif
                             @endforeach
                             <hr>
                             {!!$discuss_rule_data['0']['rule']!!}
                                        <button class="theme-btn" data-toggle="modal" data-target=".bd-example-modal-lg">Написать позицию</button>
                                        <br><br>
                                    <table class="table">
                                        <tr>
                                            <td width=50% style="border:none !important;">
                                        <table class="table table-striped" cellspacing="20">
                                            <tr>
                                                <td style="color:green;">Позиции PRO</td>
                                            </tr>
                                            @foreach($positions as $position)
                                                @if($position['pro_contra'] == 1)
                                                    <tr>
                                                        <td class="PROet_{{$position['id']}}">
                                                            <table style="margin-bottom: 5px;">
                                                                <tr>
                                                                    <td>
                                                                        @if(isset($position->author->photo))
                                                            <img id="discussion_avatar" src="{{$position->author->photo['url']}}"> &nbsp;&nbsp;&nbsp;
                                                            @else
                                                               <img id="discussion_avatar" src="/front/assets/images/avatar_none.png"> &nbsp;&nbsp;&nbsp; 
                                                            @endif
                                                                    </td>
                                                                    <td>
                                                            <text style="font-size: 1rem;font-weight: 400; color:#c9b38c;margin: 0 0 0.2em;">
                                                            <a style="font-size: 1rem;font-weight: 400;margin: 0 0 0.2em;color: #c9b38c;" href="/wall/{{$position->author->id}}">
                                                            {{$position->author->surname}}
                                                            {{$position->author->name}} 
                                                            {{$position->author->last_name}}
                                                            </a>
                                                            </text>
                                                            <br>
                                                            {{$position['created_at']}}
                                                            </td>
                                                        </tr>
                                                </table>
                                                            <a style="cursor:pointer;" onClick="load_doctrine({{$position['id']}});"><b>Доктрина</b></a> 
                                                            |
                                                            <a style="cursor:pointer;color:red;" onClick="load_contra({{$position['id']}},{{$position['pro_contra']}} );"><b>Позиции против</b></a>

                                                            <div style="margin-top: 5px;">
                                                            {!!$position['position']!!}
                                                            <?php $arg_num = 1; ?>
                                                            @foreach ($position->positionArguments as $argument)
                                                                <img src="/front/assets/images/tick.jpg" width=20> <b>Аргумент {{$arg_num}}: </b>
                                                                {!!$argument['argument']!!}
                                                                <?php $arg_num++; ?>
                                                                <br>
                                                            @endforeach
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </table>
                                    </td>
                                    <td width=50% style="border:none !important;">
                                        <table class="table table-striped" cellspacing="20">
                                            <tr>
                                                <td style="color:red;">Позиции CONTRA</td>
                                            </tr>
                                            @foreach($positions as $position)
                                                @if($position['pro_contra'] == 2)
                                                <tr>
                                                 <td class="CONTRAet_{{$position['id']}}">
                                                   <table style="margin-bottom: 5px;">
                                                                <tr>
                                                                    <td>
                                                                        @if(isset($position->author->photo))
                                                            <img id="discussion_avatar" src="{{$position->author->photo['url']}}"> &nbsp;&nbsp;&nbsp;
                                                            @else
                                                               <img id="discussion_avatar" src="/front/assets/images/avatar_none.png"> &nbsp;&nbsp;&nbsp; 
                                                            @endif
                                                                    </td>
                                                                    <td>
                                                            <text style="font-size: 1rem;font-weight: 400; color:#c9b38c;margin: 0 0 0.2em;">
                                                            <a style="font-size: 1rem;font-weight: 400;margin: 0 0 0.2em;color: #c9b38c;" href="/wall/{{$position->author->id}}">
                                                            {{$position->author->surname}}
                                                            {{$position->author->name}} 
                                                            {{$position->author->last_name}}
                                                            </a>
                                                            </text>
                                                            <br>
                                                            {{$position['created_at']}}
                                                            </td>
                                                        </tr>
                                                </table>
                                                <a style="cursor:pointer;" onClick="load_doctrine({{$position['id']}});"><b>Доктрина</b></a> 
                                                            |
                                                            <a style="cursor:pointer;color:red;" onClick="load_contra({{$position['id']}},{{$position['pro_contra']}} );"><b>Позиции против</b></a>
                                                             <div style="margin-top: 5px;">
                                                    {!!$position['position']!!}
                                                    <?php $arg_num = 1; ?>

                                                            @foreach ($position->positionArguments as $argument)
                                                                <img src="/front/assets/images/tick.jpg" width=20> <b>Аргумент {{$arg_num}}: </b>
                                                                {!!$argument['argument']!!}
                                                                <?php $arg_num++; ?>
                                                                <br>
                                                            @endforeach 
                                                        </div>
                                                </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        </table>
                                    </td></tr></table>
                            </div>
                    </div>
                    <div class="col col-md-3 col-md-pull-9">
                        <div class="service-sidebar">
                            <div class="widget service-list-widget">
                                <ul>
                                    @foreach ($institute_data['0']->npa->npaInstitutes as $institute)
                                        @if($institute_data['0']['id'] == $institute['id'])
                                          <li class="current"><a href="/institute/{{$institute['id']}}">{{ $institute['name'] }}</a></li>
                                        @else
                                           <li><a href="/institute/{{$institute['id']}}">{{ $institute['name'] }}</a></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                            <div class="widget contact-widget">
                                <div>
                                    <h4>Нужна помощь?</h4>
                                    <p>Вы можете обратиться в Telegram-бот по адресу gkrk.kz для получения помощи в вопросах участия в обсуждении или иных, интересующих Вас темах.</p>
                                    <a href="#">Перейти</a>
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end service-single-section -->
@endsection
@section('js')
    <script type="text/javascript">
            $(".PROet").mouseover(function(){
                $(this).css("background-color", "#67E677");
                $(this).css("color", "black");
                $(this).css("font-weight:", "normal !important");
                $(this).css("padding:", "20px");
                $(this).find("text").css("color", "#5B5B5B");
                $(".CONTRAet").css("background-color", "#ff7373");
                $(".CONTRAet").css("color", "white");
                $(".CONTRAet").find("text").css("color", "white");
            });
            $(".PROet").mouseout(function(){
                $(".PROet").css("background-color", "inherit"); 
                $(".PROet").css("color", "inherit");
                $(".PROet").css("font-weight:", "inherit");
                $(".PROet").css("padding:", "inherit");
                $(this).find("text").css("color", "#c9b38c");
                $(".CONTRAet").css("background-color", "inherit");
                $(".CONTRAet").css("color", "inherit");
                $(".CONTRAet").find("text").css("color", "#c9b38c");
            });
            function load_doctrine (position_id){
                $('.additional_content').html("");
                $('.doctrine_content').load("/doctrine/"+position_id, function(){
                    $('#doctrine').modal({show:true});
                });
            }
            function load_contra (position_id, now_pos_is){
                $('.additional_content').html("");
                $('.additional_content').append('<button class="theme-btn" onClick="close_contra_modal('+position_id+','+now_pos_is+');">Добавить позицию против</button><hr>');

                $('.doctrine_content').load("/contra_pos/"+position_id, function(){
                    $('#doctrine').modal({show:true});
                });
            }
            function close_contra_modal (position_id, now_pos_is){
                $("#doctrine").modal("hide");
                $(".bd-example-modal-lg-2").modal("show");
                $("#position_text_vs").load("/position_text/"+position_id);
                $("#position_id_vs").val(position_id);
                if(now_pos_is == 1){
                    $('#pro_contra_second option[value=2]').attr('selected','selected');
                    $('#pro_contra_hidden').val(2);
                    //$("#pro_contra_second").val(2);
                    $('#pro_contra_second').attr('disabled', 'disabled');
                }else{
                    //$("#pro_contra_second").val(1);
                    $('#pro_contra_hidden').val(1);
                    $('#pro_contra_second option[value=1]').attr('selected','selected');
                    $('#pro_contra_second').attr('disabled', 'disabled');
                }
            }
            $(document).ready(function () {
                    var allEditors = document.querySelectorAll('.ckeditor');
                    for (var i = 0; i < allEditors.length; ++i) {
                        ClassicEditor.create(
                                allEditors[i],
                                {
                                   toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
                                }
                        );
                    }
            });

            function add_argument(){
                var arg_num = $("#arguments_number").val();
                arg_num++;
                $("#arguments_to_insert").append('<input type="text" name="arg_'+arg_num+'" class="form-control" placeholder="Аргумент '+arg_num+'"><br>');
                $("#arguments_number").val(arg_num);
            }

            function add_argument_contra(){
                var arg_num = $("#arguments_number_contra").val();
                arg_num++;
                $("#arguments_to_insert_contra").append('<input type="text" name="arg_'+arg_num+'" class="form-control" placeholder="Аргумент '+arg_num+'"><br>');
                $("#arguments_number_contra").val(arg_num);
            }

            @if (\Session::has('success'))
                $(document).ready(function () {
                    $("#success_sent").modal("show");
                });
            @endif
    </script>

    <style>
         .PROet{
            cursor: pointer;
         }
         .tab-content{
            font-weight: normal;
         }
         .review-form input{
            background: #fbfbfb !important;
                    border-radius: 0 !important;
                    box-shadow: none !important;
                    height: 40px !important;
                    border: 2px solid #efefef !important;
         }
    </style>
@endsection
@section('modals')
<div class="modal fade bd-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding: 40px !important">
                <h2>Написать свою позицию</h2>
                @if(Auth::check())
                    <div class="review-form">
                    <form id="pos_form" method="POST" action="/add_position">
                    @csrf
                    <input type="hidden" name="discuss_rule_id" value="{{$discuss_rule_data['0']['id']}}">
                    <select name="pro_contra" class="form-control">
                        <option value="1">PRO</option>
                        <option value="2">CONTRA</option>
                    </select>
                    <br>
                    Текст позиции:<br>
                    <textarea name="position_text" class="form-control ckeditor"></textarea><br>
                    Доктринальные источники:<br>
                    <textarea name="doctrine_text" class="form-control ckeditor"></textarea><br>
                    <div id="arguments_to_insert">
                        <input type="hidden" name="arg_number" id="arguments_number" value="1">
                        <input type="text" name="arg_1" id="arg_1" class="form-control" placeholder="Аргумент 1" required>
                        <br>
                    </div>
                    <a style="cursor: pointer;" onClick="add_argument();">Добавить аргумент</a>
                    <br><hr>
                    <button type="submit" class="theme-btn">Отправить</button>
                    </form>
                    </div>
                @else
                    <br>
                    <h4>Для участия в обсуждении необходимо <a href="/registration_form">зарегистрироваться</a></h4>
                @endif

                </div>
            </div>
        </div>

<div class="modal fade bd-example-modal-lg-2" style="overflow: auto;" id="bd-example-modal-lg-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel2" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding: 40px !important">
                <h2>Добавить свою позицию против</h2>
                @if(Auth::check())
                    <div class="review-form">
                    <form method="POST" action="/add_position">
                    @csrf
                    <input type="hidden" name="position_id_vs" id="position_id_vs" value="">
                    <input type="hidden" name="discuss_rule_id" value="{{$discuss_rule_data['0']['id']}}">
                    <hr>
                    <p id="position_text_vs"></p>
                    <hr>
                    <select id="pro_contra_second" class="form-control">
                        <option value="1">PRO</option>
                        <option value="2">CONTRA</option>
                    </select>
                    <input type="hidden" name="pro_contra" value="" id="pro_contra_hidden">
                    <br>
                    Текст позиции:<br>
                    <textarea name="position_text" class="form-control ckeditor"></textarea><br>
                    Доктринальные источники:<br>
                    <textarea name="doctrine_text" class="form-control ckeditor"></textarea><br>
                    <div id="arguments_to_insert_contra">
                        <input type="hidden" name="arg_number" id="arguments_number_contra" value="1">
                        <input type="text" name="arg_1" id="arg_1" class="form-control" placeholder="Аргумент 1">
                        <br>
                    </div>
                    <a style="cursor: pointer;" onClick="add_argument_contra();">Добавить аргумент</a>
                    <br><hr>
                    <button type="submit" class="theme-btn">Отправить</button>
                    </form>
                    </div>
                @else
                    <br>
                    <h4>Для участия в обсуждении необходимо <a href="/registration_form">зарегистрироваться</a></h4>
                @endif

                </div>
            </div>
        </div>

<div class="modal fade doctrine" id="doctrine" tabindex="-1" role="dialog" aria-labelledby="doctrine" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding: 40px !important">
                    <p class="additional_content"></p>
                    <h2></h2>
                    <p class="doctrine_content"></p>
            </div>
        </div>
</div>

<div class="modal fade success_sent" id="success_sent" tabindex="-1" role="dialog" aria-labelledby="doctrine" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding: 40px !important">
                    <p class="additional_content"></p>
                    <h2></h2>
                    <p class="success_sent_content">
                        @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{!! \Session::get('success') !!}</li>
                            </ul>
                        </div>
                @endif
                    </p>
            </div>
        </div>
</div>
@endsection
