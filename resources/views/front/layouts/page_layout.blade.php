<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="irstheme">

    <title> Обсуждение законодательства </title>

    <link href="/front/assets/css/themify-icons.css" rel="stylesheet">
    <link href="/front/assets/css/flaticon.css" rel="stylesheet">
    <link href="/front/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/front/assets/css/animate.css" rel="stylesheet">
    <link href="/front/assets/css/owl.carousel.css" rel="stylesheet">
    <link href="/front/assets/css/owl.theme.css" rel="stylesheet">
    <link href="/front/assets/css/slick.css" rel="stylesheet">
    <link href="/front/assets/css/slick-theme.css" rel="stylesheet">
    <link href="/front/assets/css/swiper.min.css" rel="stylesheet">
    <link href="/front/assets/css/owl.transitions.css" rel="stylesheet">
    <link href="/front/assets/css/jquery.fancybox.css" rel="stylesheet">
    <link href="/front/assets/css/style.css" rel="stylesheet">
    <link href="/front/assets/css/newStyle.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(70059703, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/70059703" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>

<body>

    <!-- start page-wrapper -->
    <div class="page-wrapper">

    <!-- start preloader -->
    <div class="preloader">
        <div class="sk-chase">
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
        </div>
    </div>
    <!-- end preloader -->


        @include("front._partials.navbar")


        @yield("content")


        <!-- start site-footer -->
        <footer class="site-footer">
            <div class="social-newsletter-area">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <div class="social-newsletter-content clearfix">
                                <div class="social-area">
                                    <ul class="clearfix">
                                        <li><a href="#"><i class="ti-facebook"></i></a></li>
                                        <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                                        <li><a href="#"><i class="ti-linkedin"></i></a></li>
                                        <li><a href="#"><i class="ti-instagram"></i></a></li>
                                    </ul>
                                </div>
                                <div class="logo-area">
                                    <img src="//front/assets/images/logokazguu.png" width="120" alt>
                                </div>
                                <div class="newsletter-area">
                                    <div class="inner">
                                        <h3>{{__('navmenu.rassylka')}}</h3>
                                        <form>
                                            <div class="input-1">
                                                <input type="email" class="form-control" placeholder=" {{__('navmenu.email')}} *" required="">
                                            </div>
                                            <div class="submit clearfix">
                                                <button type="submit"><i class="fi flaticon-paper-plane"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lower-footer">
                <div class="container">
                    <div class="row">
                        <div class="separator"></div>
                        <div class="col col-xs-12">
                            <p class="copyright">Copyright &copy; 2020 KAZGUU University. All rights reserved.</p>
                            <div class="extra-link">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end site-footer -->
    </div>
    <!-- end of page-wrapper -->



    <!-- All JavaScript files
    ================================================== -->
    <script src="/front/assets/js/jquery.min.js"></script>
    <script src="/front/assets/js/bootstrap.min.js"></script>

    <!-- Plugins for this template -->
    <script src="/front/assets/js/jquery-plugin-collection.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.5/tinymce.min.js"></script>
    <!-- Custom script for this template -->
    <script src="/front/assets/js/script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

    <!-- Plugins for this template -->
    <script src="/front/assets/js/jquery-plugin-collection.js"></script>

    <script src="/front/assets/js/newScript.js"></script>
    @yield("js")
    <script src="/front/assets/js/flickity.js"></script>
    @yield("modals")
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '{your-app-id}',
                cookie     : true,
                xfbml      : true,
                version    : '{api-version}'
            });

            FB.AppEvents.logPageView();

        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
</body>
</html>

