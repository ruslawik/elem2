<!-- Start header -->
    <style>
        @media  only screen and (max-width: 600px) {
                                                                #mobile_langs {
                                                                    display: block !important;
                                                                }
                                                            }
    </style>
        <header id="header" class="site-header header-style-1">

            <nav class="navigation navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img src="/front/assets/images/logokazguu.png" width="100" style="margin-top: -10px !important;" alt></a>
                        <br>
                        <span id="mobile_langs" style="display: none;">
                        <a style="font-weight:bold;color:white !important;float:right;margin-right:80px;" href="/local/ru">Русский</a>&nbsp;&nbsp;&nbsp;
                        <a style="font-weight:bold; color:white !important; float:right;" href="/local/kz">Қазақша&nbsp;&nbsp;&nbsp;</a>
                         </span>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navbar-right navigation-holder">
                        <button class="close-navbar"><i class="ti-close"></i></button>
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="/">{{ __('navmenu.main')}}</a>
                            </li>
                            <li>
                                <a href="/all-docs/{{$navbar['first_category']->id}}">{{__('navmenu.discussing_npa')}}</a>
                            </li>
                            <li>
                                <a href="/library">{{ __('navmenu.doctrine_library')}}</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">{{ __('navmenu.about')}}</a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="/about">{{ __('navmenu.about')}}</a>
                                        <a href="/team">{{ __('navmenu.team')}}</a>
                                        <a href="/base_docs">{{ __('navmenu.base_docs')}}</a>
                                        <a href="/faqs">{{ __('navmenu.faq')}}</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <?php
                                    if(app()->getLocale() == "ru"){
                                    ?>
                                    <!--
                                        <a href="/local/ru"><img src="/front/assets/images/icons/flag_ru.png" width=30px></a>!-->
                                        <a href="/local/ru">РУС</a>
                                    <?php
                                    }else{
                                    ?>
                                       <!--
                                        <a href="/local/ru"><img src="/front/assets/images/icons/flag_kz.svg" width=30px></a>!-->
                                        <a href="/local/kz">ҚАЗ</a>
                                    <?php
                                    } 
                                    ?>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="/local/kz"><font color="black">Қазақша</font></a>
                                        <a href="/local/ru"><font color="black">Русский</font></a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                @if(Auth::check())
                                    <a class="theme-btn" href="/cabinet">{{ __('navmenu.zhekecabinet')}}</a>
                                @else
                                    <a class="theme-btn" href="/loginform">{{ __('navmenu.vxod_reg')}}</a>
                                @endif
                            </li>
                        </ul>
                    </div><!-- end of nav-collapse -->
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->
