@extends('front.layouts.page_layout')
@section('content')
        <!-- start page-title -->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>{{ $user_data->surname }} {{ $user_data->name }} {{ $user_data->last_name }}</h2>
                        <!--<p>Личный кабинет</p>!-->
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <button class="theme-btn" type="submit">Выйти</button>
                        </form>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end page-title -->

<!-- start team-sigle-section -->
        <section class="team-sigle-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-4">
                        <div class="team-single-sidebar">
                            <div class="widget attorney-widget">
                                <h3>Меню</h3>
                                <ul>
                                    <li class="current"><a href="/cabinet">Мои данные</a></li>
                                    <li><a href="/cabinet/positions">Мои позиции</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col col-md-8">
                        <div class="attorney-single-content">
                            <div class="attorney">
                                <div class="attorney-single-info">
                                    <div class="alert alert-danger" id="add_proverka" style="display: none;">
                                                        <ul>
                                                            <li>При смене роли с обозревателя на эксперта Вам потребуется прохождение проверки аккаунта модератором. На время проверки Вы не будете иметь доступа к аккаунту.</li>
                                                        </ul>
                                                </div>
                                @if (\Session::has('success'))
                                                <div class="alert alert-success">
                                                        <ul>
                                                            <li>{!! \Session::get('success') !!}</li>
                                                        </ul>
                                                </div>
                                @endif
                                <form name="reg_form" method="POST" action="{{ route("user.update", [$user_data->id]) }}" enctype="multipart/form-data">
                                                    @method('PUT')
                                                    @csrf
                                                    <!--<input type="hidden" id="roles" name="roles[]" value="3">!-->
                                                    <div>
                                                        <input type="hidden" value="@foreach($user_data->roles as $role){{$role->pivot->role_id}}@endforeach" id="user_role">
                                                        Ваш статус
                                                        <select name="roles[]" class="form-control" onchange="change_reg_form();" id="roles">
                                                            @if($user_data->roles->contains(1))
                                                                <option value="1">ADMIN</option>
                                                            @else
                                                                <option value="3" {{$user_data->roles->contains(3) ? 'selected' : ''}}>Обозреватель</option>
                                                                <option value="4" {{$user_data->roles->contains(4) ? 'selected' : ''}}>Эксперт</option>
                                                            @endif
                                                        </select>
                                                        @if($errors->has('roles'))
                    <div class="invalid-feedback">
                        {{ $errors->first('roles') }}
                    </div>
                @endif
                                                    </div>
                                                    <div>
                                                        Имя
                                                        <input name="name" type="text" class="form-control" value="{{ $user_data->name }}" required>
                                                         @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                                                    </div>
                                                   
                                                    <div>
                                                        Фамилия
                                                        <input name="surname" type="text" class="form-control" value="{{ $user_data->surname }}" required>
                                                        @if($errors->has('surname'))
                    <div class="invalid-feedback">
                        {{ $errors->first('surname') }}
                    </div>
                @endif
                                                    </div>
                                                    
                                                    <div>
                                                        Отчество (если имеется)
                                                        <input name="last_name" type="text" class="form-control" value="{{ $user_data->last_name }}">
                                                         @if($errors->has('last_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('last_name') }}
                    </div>
                @endif
                                                    </div>
                                                    <div>
                                                        Мобильный телефон
                                                        <input name="phone" type="text" class="form-control" value="{{ $user_data->phone }}" required>
                                                         @if($errors->has('phone'))
                    <div class="invalid-feedback">
                        {{ $errors->first('phone') }}
                    </div>
                @endif
                                                    </div>
                                                    <div>
                                                        Электронная почта
                                                        <input name="email" type="email" class="form-control" value="{{ $user_data->email }}" disabled>
                                                         @if($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                @endif
                                                    </div>
                                                    <div>
                                                        Ваш пароль
                                                        <input name="password" type="password" class="form-control">
                                                         @if($errors->has('password'))
                    <div class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </div>
                @endif
                <br>
                    <div class="form-group">
                <label for="photo">{{ trans('cruds.user.fields.photo') }}</label>
                <div class="needsclick dropzone {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="photo-dropzone">
                </div>
                @if($errors->has('photo'))
                    <div class="invalid-feedback">
                        {{ $errors->first('photo') }}
                    </div>
                @endif
                <span class="help-block">Максимальный размер 2МБ, 1000x1000 пикселей. Загрузите квадрат.</span>
                </div>
                    </div>
                                                    <div id="additional_reg_fields" style="display:none;">
                                                    <div>
                                                        Место работы
                                                        <input name="job_place" type="text" class="form-control" value="{{ $user_data->job_place }}">
                                                         @if($errors->has('job_place'))
                    <div class="invalid-feedback">
                        {{ $errors->first('job_place') }}
                    </div>
                @endif
                                                    </div>
                                                    <br>
            <div>
                <label for="education_diploma">{{ trans('cruds.user.fields.education_diploma') }}</label>
                <div class="needsclick dropzone {{ $errors->has('education_diploma') ? 'is-invalid' : '' }}" id="education_diploma-dropzone">
                </div>
                @if($errors->has('education_diploma'))
                    <div class="invalid-feedback">
                        {{ $errors->first('education_diploma') }}
                    </div>
                @endif
                @if(isset($user_data->education_diploma))
                    <a href="{{$user_data->education_diploma->getUrl()}}" target="_blank">Скачать файл</a>
                @endif
                @if($errors->has('education_diploma'))
                    <div class="invalid-feedback">
                        {{ $errors->first('education_diploma') }}
                    </div>
                @endif
            </div>
            <br>
            <div>
                <label for="degree_diploma">{{ trans('cruds.user.fields.degree_diploma') }}</label>
                <div class="needsclick dropzone {{ $errors->has('degree_diploma') ? 'is-invalid' : '' }}" id="degree_diploma-dropzone">
                </div>
                @if($errors->has('degree_diploma'))
                    <div class="invalid-feedback">
                        {{ $errors->first('degree_diploma') }}
                    </div>
                @endif
                @if(isset($user_data->degree_diploma))
                    <a href="{{$user_data->degree_diploma->getUrl()}}" target="_blank">Скачать файл</a>
                @endif
            </div>
                                                    

            </div>

                <input type="hidden" id="degree_hidden" value="{{isset($user_data->degree_diploma) ? '1':'0' }}">
                <input type="hidden" id="education_hidden" value="{{isset($user_data->education_diploma) ? '1':'0' }}">
                                                    <!--
                                                    <div>
                                                        <textarea class="form-control" placeholder="Review *"></textarea>
                                                    </div>!-->

                                                        <div>
                                                            <button type="submit" class="theme-btn">Обновить данные</button>
                                                        </div>
                                                      
                                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end team-sigle-section -->



@endsection

@section('js')
<script>
        $(document).ready(function() {
                $('form[name=reg_form]').submit(function() {
                    if($("#roles").val() == 4 && ($("#degree_hidden").val() == 0 || $("#education_hidden").val() == 0)){
                        alert("Не загружен скан одного из дипломов!");
                        return false;
                    }
                });
            });
        function change_reg_form() {
            $("#additional_reg_fields").toggle();

            var now_role_id = $("#user_role").val();
            if(now_role_id == 3 && $("#roles").val() == 4){
                $("#add_proverka").css("display", "block");
            }else{
                $("#add_proverka").css("display", "none");
            }

        }
        @if($user_data->roles->contains(4))
            $("#additional_reg_fields").css('display', 'block');
        @endif
    </script>

<script>
    Dropzone.options.photoDropzone = {
    url: '{{ route('front.reg.storeMedia') }}',
    maxFilesize: 10, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 10,
      width: 1000,
      height: 1000
    },
    success: function (file, response) {
      $('form').find('input[name="photo"]').remove()
      $('form').append('<input type="hidden" name="photo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="photo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($user_data) && $user_data->photo)
      var file = {!! json_encode($user_data->photo) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, file.preview)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="photo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>

<script>
    Dropzone.options.educationDiplomaDropzone = {
    url: '{{ route('front.reg.storeMedia') }}',
    maxFilesize: 4, // MB
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 4
    },
    success: function (file, response) {
      $('form').find('input[name="education_diploma"]').remove()
      $('form').append('<input type="hidden" name="education_diploma" value="' + response.name + '">')
      $('#education_hidden').val(1);
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="education_diploma"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
        @if(isset($user_data) && $user_data->education_diploma)
      var file = {!! json_encode($user_data->education_diploma) !!}
          this.options.addedfile.call(this, file)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="education_diploma" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
<script>
    Dropzone.options.degreeDiplomaDropzone = {
    url: '{{ route('front.reg.storeMedia') }}',
    maxFilesize: 4, // MB
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 4
    },
    success: function (file, response) {
      $('form').find('input[name="degree_diploma"]').remove()
      $('form').append('<input type="hidden" name="degree_diploma" value="' + response.name + '">')
      $('#degree_hidden').val(1);
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="degree_diploma"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
        @if(isset($user_data) && $user_data->degree_diploma)
      var file = {!! json_encode($user_data->degree_diploma) !!}
          this.options.addedfile.call(this, file)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="degree_diploma" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
        @endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection