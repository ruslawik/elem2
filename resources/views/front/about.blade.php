@extends('front.layouts.page_layout')
@section('content')
    <!-- start page-title -->

    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col col-xs-12">
                    <h2>{{__('navmenu.about')}}</h2>
                </div>
            </div> <!-- end row -->
        </div> <!-- end container -->
    </section>
    <!-- end page-title -->


    <!-- start blog-single-section -->
    <section class="blog-single-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col col-md-12">
                    <div class="blog-content">
                        
                        <div class="post format-standard-image">

                            {!!$about->text!!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
