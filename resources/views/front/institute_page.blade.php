@extends('front.layouts.page_layout')
@section('content')

	   <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>{{ $institute_data[0]['name'] }}</h2>
                        <p>{{ $institute_data[0]->npa['name'] }}</p>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end page-title -->

        <!-- start service-single-section -->
        <section class="service-single-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-9 col-md-push-3">
                        <div class="service-single-content">
            <div class="container">   
                <div class="row">
                    <div class="col col-xs-9">
                        {!! $institute_data[0]['rule'] !!}
                        <hr>
                        @if($has_rules == 1)
                            <a id="acollapse" href="javascript:jQuery('#accordion .panel-collapse').collapse('hide'); jQuery('#acollapse').hide();jQuery('#aexpand').show();" style="display: none;"><button class="theme-btn">Свернуть все</button></a>
                            <a id="aexpand" href="javascript:jQuery('#accordion .panel-collapse').collapse('show'); jQuery('#aexpand').hide(); jQuery('#acollapse').show();"><button class="theme-btn">Развернуть все</button></a>
                        @else
                            <h4>В данный институт не добавлено ни одной нормы права для обсуждения. </h4>
                            <hr>
                        @endif
                        <div class="panel-group faq-accordion theme-accordion-s1" id="accordion">
                            <?php $d_id = 1; ?>
                            @foreach ($discuss_rules as $rule)
                             <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$d_id}}" aria-expanded="true"><img src="{{$rule->avatar['url']}}" width="40px"> {{ $rule->name }}</a>
                                </div>
                                <div id="collapse-{{$d_id}}" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>{!! $rule->rule !!}</p>
<a href="/institute/{{$institute_data[0]['id']}}/discussion/{{$rule->id}}/#top">Перейти к обсуждению</a>
                                    </div>
                                </div>
                            </div>
                            <?php $d_id++; ?>
                            @endforeach
                        </div>
                    </div>
                </div>    
            </div> <!-- end container -->
        <!-- end faq-pg-section --> 



                          
                        </div>
                    </div>
                    <div class="col col-md-3 col-md-pull-9">
                        <div class="service-sidebar">
                            <div class="widget service-list-widget">
                                <ul>
                                	@foreach ($institute_data['0']->npa->npaInstitutes as $institute)
                                        @if($institute_data['0']['id'] == $institute['id'])
                                		  <li class="current"><a href="/institute/{{$institute['id']}}">{{ $institute['name'] }}</a></li>
                                        @else
                                           <li><a href="/institute/{{$institute['id']}}">{{ $institute['name'] }}</a></li>
                                        @endif
                                	@endforeach
                                </ul>
                            </div>
                            <div class="widget contact-widget">
                                <div>
                                    <h4>Нужна помощь?</h4>
                                    <p>Вы можете обратиться в Telegram-бот по адресу gkrk.kz для получения помощи в вопросах участия в обсуждении или иных, интересующих Вас темах.</p>
                                    <a href="#">Перейти</a>
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end service-single-section -->


@endsection