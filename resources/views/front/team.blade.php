@extends('front.layouts.page_layout')
@section('content')

        <!-- start page-title -->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>{{__('navmenu.team')}}</h2>
                        <p>{{__('navmenu.osnova_team')}}</p>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>        
        <!-- end page-title -->


        <!-- start team-section -->
        <section class="team-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title-s3">
                            <div class="icon">
                                <i class="fi flaticon-suitcase"></i>
                            </div>
                            <span></span>
                            <h2>Информация о членах команды</h2><br><br>
                            <p>
Автор идеи: Нарикбаев Талгат Максутович, Пен Сергей Геннадьевич<br><br>

Продюсер проекта: Нарикбаев Талгат Максутович<br><br>
Администратор контента: Бабаджанян Естер Левоновна<br><br>
Руководитель проекта "Новый ГК РК": Абилова Майгуль Нестаевна<br><br>

Авторы предложений "Новый ГК РК": профессорско-преподавательский состав Высшей школы права Университета КАЗГЮУ имени М.С.Нарикбаева - Идрышева Сара Кимадиевна, Хасенов Муслим Ханатович, Ешниязов Нуржан Сергалиевич, Калдыбаев Аскар Куралбаевич, Естемиров Марат Асанович, Кудиярова Улбала Ерзатовна<br><br>

Руководитель проекта "Библиотека доктрин ELEMENTA": Шайкенов Арман Нагашбаевич<br><br>

Руководитель проекта "АППК РК": Мельник Роман Сергеевич
                            </p>
                        </div>
                    </div>
                </div> 
                <!--
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="team-grids team-slider">
                            <div class="grid">
                                <div class="img-holder">
                                    <img src="/front/assets/images/team/img-1.jpg" alt>
                                </div>
                                <div class="details">
                                    <div class="social">
                                        <ul>
                                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                                            <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                                            <li><a href="#"><i class="ti-pinterest"></i></a></li>
                                        </ul>
                                    </div>
                                    <h3><a href="#">Майгуль Нестаевна АБИЛОВА </a></h3>
                                    <p>Координатор-руководитель рабочей группы, PhD, Assistant Professor, KAZGUU Law School</p>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="img-holder">
                                    <img src="/front/assets/images/team/img-2.jpg" alt>
                                </div>
                                <div class="details">
                                    <div class="social">
                                        <ul>
                                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                                            <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                                            <li><a href="#"><i class="ti-pinterest"></i></a></li>
                                        </ul>
                                    </div>
                                    <h3><a href="#">Сара Кимадиевна ИДРЫШЕВА </a></h3>
                                    <p>д.ю.н., профессор департамента частного права, KAZGUU Law School</p>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="img-holder">
                                    <img src="/front/assets/images/team/img-3.jpg" alt>
                                </div>
                                <div class="details">
                                    <div class="social">
                                        <ul>
                                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                                            <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                                            <li><a href="#"><i class="ti-pinterest"></i></a></li>
                                        </ul>
                                    </div>
                                    <h3><a href="#">Марат Асанович ЕСТЕМИРОВ </a></h3>
                                    <p>PhD, Assistant Professor, KAZGUU Law School</p>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="img-holder">
                                    <img src="/front/assets/images/team/img-4.jpg" alt>
                                </div>
                                <div class="details">
                                    <div class="social">
                                        <ul>
                                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                                            <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                                            <li><a href="#"><i class="ti-pinterest"></i></a></li>
                                        </ul>
                                    </div>
                                    <h3><a href="#">Ешниязов Нуржан Сергалиевич</a></h3>
                                    <p>PhD, Assistant Professor, KAZGUU Law School</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
                !-->       
            </div> 
        </section>
        <!-- end team-section -->

@endsection