@extends('front.layouts.page_layout')
@section('content')


           <!-- start page-title -->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>{{ __('navmenu.vxod_reg')}}</h2>
                        <p>{{ __('navmenu.kir_or_reg')}}</p>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end page-title -->

<!-- start shop-single-section -->
        <section class="shop-single-section" style="margin-bottom: 50px;">
            <div class="container">
                <div class="row">
                        <div class="product-info">
                                    <div class="col col-md-3">
                                    </div>

                        <div class="col col-md-6 review-form-wrapper">

                                <ul class="nav nav-tabs" role="tablist">
                                <li class="active"><a href="/loginform" style="cursor:pointer;">{{ __('navmenu.kir')}}</a></li>
                                <li><a href="/registration_form" style="cursor:pointer;">{{ __('navmenu.reg')}}</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active">
                                    <div class="review-form">
                                        @if(session('message'))
                                            <div class="alert alert-danger" role="alert">
                                                {{ session('message') }}
                                            </div>
                                        @endif
                                                <form method="POST" action="{{ route('login') }}">
                                                    @csrf
                                                    <div>
                                                        <input type="email" class="form-control" placeholder="{{ __('navmenu.email')}} *" name="email" required>
                                                        @if($errors->has('email'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('email') }}
                                                        </div>
                                                        @endif
                                                    </div>
                                                    <div>
                                                        <input type="password" name="password" class="form-control" placeholder="{{ __('navmenu.password')}} *" required>
                                                        @if($errors->has('password'))
                                                            <div class="invalid-feedback">
                                                                {{ $errors->first('password') }}
                                                            </div>
                                                            @endif
                                                    </div>
                                                        <div class="submit">
                                                            <button type="submit" class="theme-btn">{{ __('navmenu.kir')}}</button>
                                                        </div>
                                                </form>
                                            <br>
                                            <hr>
                                            <a href="{{ route('social.login', 'facebook') }}" class="btn btn-primary">{{ __('navmenu.kir_facebook')}}</a>

                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="02">
                                    <div class="review-form">
                                        <br>
                                                <form method="POST" action="{{$action}}" enctype="multipart/form-data">
                                                    @csrf
                                                    <!--<input type="hidden" id="roles" name="roles[]" value="3">!-->
                                                    <div>
                                                        <select name="roles[]" class="form-control" onchange="change_reg_form();">
                                                            <option value="3">Обозреватель</option>
                                                            <option value="4">Эксперт</option>
                                                        </select>
                                                    </div>
                                                    <div>
                                                        Имя
                                                        <input name="name" type="text" class="form-control" placeholder="Кайрат *" value="{{ old('name', '') }}" required>
                                                    </div>
                                                    <div>
                                                        Фамилия
                                                        <input name="surname" type="text" class="form-control" placeholder="Сериков *" value="{{ old('surname', '') }}" required>
                                                    </div>
                                                    <div>
                                                        Отчество (если имеется)
                                                        <input name="last_name" type="text" class="form-control" value="{{ old('last_name', '') }}"placeholder="Канатулы">
                                                    </div>
                                                    <div>
                                                        Мобильный телефон
                                                        <input name="phone" type="text" class="form-control" placeholder="+7 777 77 77 *" value="{{ old('phone', '') }}" required>
                                                    </div>
                                                    <div>
                                                        Электронная почта
                                                        <input name="email" type="email" class="form-control" placeholder="Email *" value="{{ old('email', '') }}" required>
                                                    </div>
                                                    <div id="additional_reg_fields" style="display:none;">
                                                    <div>
                                                        Место работы
                                                        <input name="job_place" type="text" class="form-control" value="{{ old('job_place', '') }}" placeholder="Университет КАЗГЮУ">
                                                    </div>
                                                    <br>
                                                    <div>
                                                        Скан диплома о высшем образовании
                                                        <input name="education_diploma" type="file" class="form-control" required>
                                                    </div>
                                                    <br>
                                                    <div>
                                                        Скан диплома об ученой степени/звании
                                                        <input name="degree_diploma" type="file" class="form-control" required>
                                                    </div>
                                                    </div>
                                                    <!--
                                                    <div>
                                                        <textarea class="form-control" placeholder="Review *"></textarea>
                                                    </div>!-->

                                                        <div class="submit">
                                                            <input type="submit" class="theme-btn" value="Регистрация">
                                                        </div>
                                                </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end of shop-single-section -->


@endsection

@section('js')
    <script>
        function change_reg_form() {
            $("#additional_reg_fields").toggle();
            var now_role = $("#roles").val();
            if(now_role==3){
                $("#roles").val(4);
            }else{
                $("#roles").val(3);
            }
        }
    </script>
@endsection
