@extends('front.layouts.page_layout')
@section('content')
        <!-- start page-title -->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>Ошибка</h2>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end page-title -->

<!-- start team-sigle-section -->
        <section class="team-sigle-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-12">
                        <div class="attorney-single-content">
                            <center>
                                <h3>{{$error}}</h3>
                                <button onclick="goBack()">Назад</button>

<script>
function goBack() {
  window.history.back();
}
</script>
                            </center>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end team-sigle-section -->
@endsection
