@extends('front.layouts.page_layout')
@section('content')
    <div class="loader">
        <img src="{{asset('front/assets/images/spinner.svg')}}">
    </div>
    <!-- start page-title -->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col col-xs-12">
                    <h2>{{$now_npa[0]->name}}</h2>
                </div>
            </div> <!-- end row -->
        </div> <!-- end container -->
    </section>
    <!-- end page-title -->
                <?php
                    function integerToRoman($integer)
                    {
                        // Convert the integer into an integer (just to make sure)
                        $integer = intval($integer);
                        $result = '';

                        // Create a lookup array that contains all of the Roman numerals.
                        $lookup = array('M' => 1000,
                                        'CM' => 900,
                                        'D' => 500,
                                        'CD' => 400,
                                        'C' => 100,
                                        'XC' => 90,
                                        'L' => 50,
                                        'XL' => 40,
                                        'X' => 10,
                                        'IX' => 9,
                                        'V' => 5,
                                        'IV' => 4,
                                        'I' => 1);

                        foreach($lookup as $roman => $value){
                                // Determine the number of matches
                                $matches = intval($integer/$value);

                                // Add the same number of characters to the string
                                $result .= str_repeat($roman,$matches);

                                // Set the integer to be the remainder of the integer and the value
                                $integer = $integer % $value;
                        }

                        // The Roman numeral should be built, return it
                        return $result;
                    }
                ?>
    <!-- start top_buttons -->
    <div class="scroller"></div>
    <section class="top_buttons type_2">
        <div class="container">
            <input type="hidden" class="active_institute_id" value="{{$now_institute[0]->id}}">
            <div class="dragscroll disable_scrollbar scroller_from_top">
                <div class="topButtons_slider  default-skin">
                    <?php $i = 1; ?>
                    @foreach($all_institutes as $institute)
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 top_button_item "
                         id="{{$institute->id}}" >
                        <a data-href="/doc/institutional/{{$now_npa[0]->id}}/institute/{{$institute->id}}">
                            <button type="button" class="item @if($institute->id == $now_institute[0]->id || $now_institute[0]->parent_institute_id == $institute->id) active @endif">
                                <div class="num"><?php echo integerToRoman($i);?></div>
                                <h2>{{$institute->name}}</h2>
                                <p>@if($institute->type == "baza") Базовый @else Инициативный @endif</p>
                            </button>
                        </a>
                    </div>
                    <?php $i++; ?>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- end top_buttons -->

    @if($has_children == 1)
    <section class="principles">
        <div class="container">
            <div class="grid">
                @foreach($children_institutes as $institute)
                    <a href="javascript:void(0);" id="{{$institute->id}}">{{$institute->name}}</a>
                @endforeach
            </div>
        </div>
    </section>
    @endif

    <section class="performance">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="tab_buttons">
                        <?php $i = -1; ?>
                        @foreach($jurisdictions as $jurisdiction)
                        <?php $i++; ?>
                        <button type="button" data-tab-open="jur_block_{{$jurisdiction->id}}" <?php if($i==0)echo 'class="active"';?>>
                            <span class="icon">{{$jurisdiction->avatar}}</span>
                            <span class="name">{{$jurisdiction->name}}</span>
                        </button>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-8 col-lg-8 performance_column">
                    <?php $i=-1; ?>
                    @foreach($jurisdictions as $jurisdiction)
                    <?php $i++; ?>
                    <div class="tab_block <?php if($i==0)echo 'active';?>" data-tab="jur_block_{{$jurisdiction->id}}">
                        <div class="tab_content">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="scroll_box">
                                        <div class="item">
                                            <h2 class="block_title">Академическое представление</h2>
                                            @foreach($academic_discuss_rules as $academ_rule)
                                                @if($academ_rule->jurisdiction_id == $jurisdiction->id)
                                            {!!$academ_rule->rule!!}
                                            <div class="btns">
                                                <a href="#compare" data-type="academ" data-id="{{$jurisdiction->id}}" class="compare_btn" data-tab-open="comparison">
                                                    <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M39.933 24.3899L39.939 24.3879L33.3717 10.0587L34.9644 9.26601L34.3684 8.07268L32.8157 8.84601L32.6057 8.38801C32.4522 8.05335 32.0565 7.90644 31.7219 8.05985C31.5767 8.12643 31.4603 8.24285 31.3937 8.38801L31.2064 8.79735C30.0764 8.27252 28.8457 8.00019 27.5998 7.99935H21.9998V6.66603H23.3331V5.33271H22.1871L22.6665 3.41472C23.0345 1.94198 22.139 0.449745 20.6662 0.0816651C19.1935 -0.286332 17.7013 0.609161 17.3332 2.0819C17.2239 2.5194 17.2239 2.97714 17.3332 3.41464L17.8125 5.33262H16.6665V6.66595H17.9998V7.99927H12.4032C11.1561 8.00027 9.92409 8.27327 8.79326 8.79926L8.60593 8.38993C8.45252 8.05527 8.05677 7.90835 7.72211 8.06177C7.57694 8.12835 7.46052 8.24477 7.39394 8.38993L7.18394 8.84793L5.63129 8.07127L5.0353 9.26593L6.62795 10.0619L0.060675 24.3879C0.0201753 24.475 -0.000491135 24.5699 8.86038e-06 24.6659C0.0029255 27.2419 2.09057 29.3296 4.66663 29.3325H11.3332C13.9093 29.3296 15.9969 27.2419 15.9999 24.6659C15.9982 24.5701 15.9754 24.4759 15.9332 24.3899L9.34792 10.0067C10.3052 9.56234 11.3479 9.33234 12.4032 9.33267H17.9998V33.7231L15.7239 35.9991H13.3332C11.4932 36.0013 10.0021 37.4924 9.99992 39.3324C9.99992 39.7006 10.2984 39.9991 10.6666 39.9991H29.3331C29.7012 39.9991 29.9997 39.7006 29.9997 39.3324C29.9976 37.4924 28.5064 36.0013 26.6664 35.9991H24.2758L21.9998 33.7231V9.33267H27.5998C28.6551 9.33234 29.6978 9.56234 30.6551 10.0067L24.0605 24.3879C24.02 24.475 23.9993 24.5699 23.9998 24.6659C24.0027 27.2419 26.0904 29.3296 28.6664 29.3325H35.333C37.9091 29.3296 39.9967 27.2419 39.9996 24.6659C39.9981 24.5701 39.9753 24.4759 39.933 24.3899ZM11.3332 27.9992H4.66663C3.08315 27.9974 1.71874 26.8836 1.4 25.3325H14.5999C14.2812 26.8836 12.9167 27.9974 11.3332 27.9992ZM14.2945 23.9992H1.70533L7.82927 10.6387C7.9411 10.6687 8.05877 10.6687 8.1706 10.6387L14.2945 23.9992ZM18.8845 1.87673C19.4037 1.26074 20.3239 1.18232 20.9399 1.70149C21.0032 1.75482 21.0618 1.81348 21.1152 1.87673C21.3848 2.21973 21.4801 2.66839 21.3731 3.09139L20.8125 5.33271H19.1872L18.6265 3.09139C18.5196 2.66839 18.6149 2.21973 18.8845 1.87673ZM19.3332 7.99935V6.66603H20.6665V7.99935H19.3332ZM20.6665 9.33267V33.3325H19.3332V9.33267H20.6665ZM26.6664 37.3325C27.5138 37.3335 28.2688 37.8673 28.5524 38.6658H11.4472C11.7308 37.8673 12.4859 37.3335 13.3332 37.3325H26.6664ZM22.3905 35.9991H17.6092L18.9425 34.6658H21.0572L22.3905 35.9991ZM31.8277 10.6407C31.9378 10.6794 32.0586 10.6744 32.1651 10.6267L38.2943 23.9992H25.7051L31.8277 10.6407ZM35.333 27.9992H28.6664C27.0829 27.9974 25.7185 26.8836 25.3998 25.3325H38.5997C38.281 26.8836 36.9165 27.9974 35.333 27.9992Z"
                                                              fill="#C9B48C"/>
                                                    </svg>
                                                    Сравнить
                                                </a>
                                                <a href="#discussion" onClick="load_discussion({{$academ_rule->id}}, 'discusses');" data-tab-open="discussion" class="discuss_btn">
                                                    <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M36 4H18.6667C16.4584 4.00217 14.6688 5.79175 14.6667 8V16H4C1.79175 16.0022 0.00216667 17.7918 0 20V29.3333C0.00241667 31.2844 1.41 32.9501 3.33333 33.278V35.3333C3.33342 35.7015 3.63192 35.9999 4.00017 35.9998C4.17692 35.9998 4.34633 35.9296 4.47133 35.8047L6.94267 33.3333H21.3333C23.5416 33.3312 25.3312 31.5416 25.3333 29.3333V21.3333H33.0573L35.5287 23.8047C35.7891 24.065 36.2112 24.0649 36.4715 23.8045C36.5964 23.6795 36.6667 23.51 36.6667 23.3333V21.278C38.59 20.9501 39.9976 19.2844 40 17.3333V8C39.9978 5.79175 38.2083 4.00217 36 4ZM24 29.3333C24 30.8061 22.8061 32 21.3333 32H6.66667C6.48983 32 6.32033 32.0703 6.19533 32.1953L4.66667 33.724V32.6667C4.66667 32.2985 4.36817 32 4 32C2.52725 32 1.33333 30.8061 1.33333 29.3333V20C1.33333 18.5273 2.52725 17.3333 4 17.3333H21.3333C22.8061 17.3333 24 18.5273 24 20V29.3333ZM38.6667 17.3333C38.6667 18.8061 37.4728 20 36 20C35.6318 20 35.3333 20.2985 35.3333 20.6667V21.724L33.8047 20.1953C33.6797 20.0703 33.5102 20 33.3333 20H25.3333C25.3312 17.7918 23.5416 16.0022 21.3333 16H16V8C16 6.52725 17.1939 5.33333 18.6667 5.33333H36C37.4728 5.33333 38.6667 6.52725 38.6667 8V17.3333Z"
                                                              fill="white"/>
                                                        <path d="M7.33203 22.667C6.22745 22.667 5.33203 23.5624 5.33203 24.667C5.33203 25.7716 6.22745 26.667 7.33203 26.667C8.43661 26.667 9.33203 25.7716 9.33203 24.667C9.33203 23.5624 8.43661 22.667 7.33203 22.667ZM7.33203 25.3337C6.96386 25.3337 6.66536 25.0352 6.66536 24.667C6.66536 24.2988 6.96386 24.0003 7.33203 24.0003C7.7002 24.0003 7.9987 24.2988 7.9987 24.667C7.9987 25.0352 7.7002 25.3337 7.33203 25.3337Z"
                                                              fill="white"/>
                                                        <path d="M12.668 22.667C11.5634 22.667 10.668 23.5624 10.668 24.667C10.668 25.7716 11.5634 26.667 12.668 26.667C13.7726 26.667 14.668 25.7716 14.668 24.667C14.668 23.5624 13.7726 22.667 12.668 22.667ZM12.668 25.3337C12.2998 25.3337 12.0013 25.0352 12.0013 24.667C12.0013 24.2988 12.2998 24.0003 12.668 24.0003C13.0361 24.0003 13.3346 24.2988 13.3346 24.667C13.3346 25.0352 13.0361 25.3337 12.668 25.3337Z"
                                                              fill="white"/>
                                                        <path d="M18 22.667C16.8954 22.667 16 23.5624 16 24.667C16 25.7716 16.8954 26.667 18 26.667C19.1046 26.667 20 25.7716 20 24.667C20 23.5624 19.1046 22.667 18 22.667ZM18 25.3337C17.6318 25.3337 17.3333 25.0352 17.3333 24.667C17.3333 24.2988 17.6318 24.0003 18 24.0003C18.3682 24.0003 18.6667 24.2988 18.6667 24.667C18.6667 25.0352 18.3682 25.3337 18 25.3337Z"
                                                              fill="white"/>
                                                        <path d="M30.668 8H18.668V9.33334H30.668V8Z" fill="white"/>
                                                        <path d="M36 8H32V9.33334H36V8Z" fill="white"/>
                                                        <path d="M22.668 10.667H18.668V12.0003H22.668V10.667Z"
                                                              fill="white"/>
                                                        <path d="M32 10.667H24V12.0003H32V10.667Z" fill="white"/>
                                                        <path d="M36.0013 13.333H18.668V14.6663H36.0013V13.333Z"
                                                              fill="white"/>
                                                        <path d="M30.6654 16H25.332V17.3333H30.6654V16Z" fill="white"/>
                                                        <path d="M36 16H32V17.3333H36V16Z" fill="white"/>
                                                        <path d="M35.9987 10.667H33.332V12.0003H35.9987V10.667Z"
                                                              fill="white"/>
                                                    </svg>
                                                    Обсудить
                                                    <span class="absolute">
                                                    <span class="statistics green">{{$academ_rule->pro_positions_count($academ_rule->id)}}</span>
                                                    <span class="statistics red">{{$academ_rule->contra_positions_count($academ_rule->id)}}</span>
                                                </span>
                                                </a>
                                            </div>
                                            <div class="stick"></div>
                                            @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="scroll_box">
                                        <div class="item">
                                            <h2 class="block_title">Нормативное представление</h2>
                                            @foreach($normativ_discuss_rules as $normativ_rule)
                                                @if($normativ_rule->jurisdiction_id == $jurisdiction->id)
                                                <div class="box">
                                                    {!!$normativ_rule->rule!!}

                                                    <div class="btns">
                                                        <a href="#compare"  data-type="normativ" data-id="{{$jurisdiction->id}}"  class="compare_btn" data-tab-open="comparison">
                                                            <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M39.933 24.3899L39.939 24.3879L33.3717 10.0587L34.9644 9.26601L34.3684 8.07268L32.8157 8.84601L32.6057 8.38801C32.4522 8.05335 32.0565 7.90644 31.7219 8.05985C31.5767 8.12643 31.4603 8.24285 31.3937 8.38801L31.2064 8.79735C30.0764 8.27252 28.8457 8.00019 27.5998 7.99935H21.9998V6.66603H23.3331V5.33271H22.1871L22.6665 3.41472C23.0345 1.94198 22.139 0.449745 20.6662 0.0816651C19.1935 -0.286332 17.7013 0.609161 17.3332 2.0819C17.2239 2.5194 17.2239 2.97714 17.3332 3.41464L17.8125 5.33262H16.6665V6.66595H17.9998V7.99927H12.4032C11.1561 8.00027 9.92409 8.27327 8.79326 8.79926L8.60593 8.38993C8.45252 8.05527 8.05677 7.90835 7.72211 8.06177C7.57694 8.12835 7.46052 8.24477 7.39394 8.38993L7.18394 8.84793L5.63129 8.07127L5.0353 9.26593L6.62795 10.0619L0.060675 24.3879C0.0201753 24.475 -0.000491135 24.5699 8.86038e-06 24.6659C0.0029255 27.2419 2.09057 29.3296 4.66663 29.3325H11.3332C13.9093 29.3296 15.9969 27.2419 15.9999 24.6659C15.9982 24.5701 15.9754 24.4759 15.9332 24.3899L9.34792 10.0067C10.3052 9.56234 11.3479 9.33234 12.4032 9.33267H17.9998V33.7231L15.7239 35.9991H13.3332C11.4932 36.0013 10.0021 37.4924 9.99992 39.3324C9.99992 39.7006 10.2984 39.9991 10.6666 39.9991H29.3331C29.7012 39.9991 29.9997 39.7006 29.9997 39.3324C29.9976 37.4924 28.5064 36.0013 26.6664 35.9991H24.2758L21.9998 33.7231V9.33267H27.5998C28.6551 9.33234 29.6978 9.56234 30.6551 10.0067L24.0605 24.3879C24.02 24.475 23.9993 24.5699 23.9998 24.6659C24.0027 27.2419 26.0904 29.3296 28.6664 29.3325H35.333C37.9091 29.3296 39.9967 27.2419 39.9996 24.6659C39.9981 24.5701 39.9753 24.4759 39.933 24.3899ZM11.3332 27.9992H4.66663C3.08315 27.9974 1.71874 26.8836 1.4 25.3325H14.5999C14.2812 26.8836 12.9167 27.9974 11.3332 27.9992ZM14.2945 23.9992H1.70533L7.82927 10.6387C7.9411 10.6687 8.05877 10.6687 8.1706 10.6387L14.2945 23.9992ZM18.8845 1.87673C19.4037 1.26074 20.3239 1.18232 20.9399 1.70149C21.0032 1.75482 21.0618 1.81348 21.1152 1.87673C21.3848 2.21973 21.4801 2.66839 21.3731 3.09139L20.8125 5.33271H19.1872L18.6265 3.09139C18.5196 2.66839 18.6149 2.21973 18.8845 1.87673ZM19.3332 7.99935V6.66603H20.6665V7.99935H19.3332ZM20.6665 9.33267V33.3325H19.3332V9.33267H20.6665ZM26.6664 37.3325C27.5138 37.3335 28.2688 37.8673 28.5524 38.6658H11.4472C11.7308 37.8673 12.4859 37.3335 13.3332 37.3325H26.6664ZM22.3905 35.9991H17.6092L18.9425 34.6658H21.0572L22.3905 35.9991ZM31.8277 10.6407C31.9378 10.6794 32.0586 10.6744 32.1651 10.6267L38.2943 23.9992H25.7051L31.8277 10.6407ZM35.333 27.9992H28.6664C27.0829 27.9974 25.7185 26.8836 25.3998 25.3325H38.5997C38.281 26.8836 36.9165 27.9974 35.333 27.9992Z"
                                                                      fill="#C9B48C"/>
                                                            </svg>
                                                            Сравнить
                                                        </a>
                                                        <a href="#discussion" onClick="load_discussion({{$normativ_rule->id}}, 'discusses');" data-tab-open="discussion" class="discuss_btn">
                                                            <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M36 4H18.6667C16.4584 4.00217 14.6688 5.79175 14.6667 8V16H4C1.79175 16.0022 0.00216667 17.7918 0 20V29.3333C0.00241667 31.2844 1.41 32.9501 3.33333 33.278V35.3333C3.33342 35.7015 3.63192 35.9999 4.00017 35.9998C4.17692 35.9998 4.34633 35.9296 4.47133 35.8047L6.94267 33.3333H21.3333C23.5416 33.3312 25.3312 31.5416 25.3333 29.3333V21.3333H33.0573L35.5287 23.8047C35.7891 24.065 36.2112 24.0649 36.4715 23.8045C36.5964 23.6795 36.6667 23.51 36.6667 23.3333V21.278C38.59 20.9501 39.9976 19.2844 40 17.3333V8C39.9978 5.79175 38.2083 4.00217 36 4ZM24 29.3333C24 30.8061 22.8061 32 21.3333 32H6.66667C6.48983 32 6.32033 32.0703 6.19533 32.1953L4.66667 33.724V32.6667C4.66667 32.2985 4.36817 32 4 32C2.52725 32 1.33333 30.8061 1.33333 29.3333V20C1.33333 18.5273 2.52725 17.3333 4 17.3333H21.3333C22.8061 17.3333 24 18.5273 24 20V29.3333ZM38.6667 17.3333C38.6667 18.8061 37.4728 20 36 20C35.6318 20 35.3333 20.2985 35.3333 20.6667V21.724L33.8047 20.1953C33.6797 20.0703 33.5102 20 33.3333 20H25.3333C25.3312 17.7918 23.5416 16.0022 21.3333 16H16V8C16 6.52725 17.1939 5.33333 18.6667 5.33333H36C37.4728 5.33333 38.6667 6.52725 38.6667 8V17.3333Z"
                                                                      fill="white"/>
                                                                <path d="M7.33203 22.667C6.22745 22.667 5.33203 23.5624 5.33203 24.667C5.33203 25.7716 6.22745 26.667 7.33203 26.667C8.43661 26.667 9.33203 25.7716 9.33203 24.667C9.33203 23.5624 8.43661 22.667 7.33203 22.667ZM7.33203 25.3337C6.96386 25.3337 6.66536 25.0352 6.66536 24.667C6.66536 24.2988 6.96386 24.0003 7.33203 24.0003C7.7002 24.0003 7.9987 24.2988 7.9987 24.667C7.9987 25.0352 7.7002 25.3337 7.33203 25.3337Z"
                                                                      fill="white"/>
                                                                <path d="M12.668 22.667C11.5634 22.667 10.668 23.5624 10.668 24.667C10.668 25.7716 11.5634 26.667 12.668 26.667C13.7726 26.667 14.668 25.7716 14.668 24.667C14.668 23.5624 13.7726 22.667 12.668 22.667ZM12.668 25.3337C12.2998 25.3337 12.0013 25.0352 12.0013 24.667C12.0013 24.2988 12.2998 24.0003 12.668 24.0003C13.0361 24.0003 13.3346 24.2988 13.3346 24.667C13.3346 25.0352 13.0361 25.3337 12.668 25.3337Z"
                                                                      fill="white"/>
                                                                <path d="M18 22.667C16.8954 22.667 16 23.5624 16 24.667C16 25.7716 16.8954 26.667 18 26.667C19.1046 26.667 20 25.7716 20 24.667C20 23.5624 19.1046 22.667 18 22.667ZM18 25.3337C17.6318 25.3337 17.3333 25.0352 17.3333 24.667C17.3333 24.2988 17.6318 24.0003 18 24.0003C18.3682 24.0003 18.6667 24.2988 18.6667 24.667C18.6667 25.0352 18.3682 25.3337 18 25.3337Z"
                                                                      fill="white"/>
                                                                <path d="M30.668 8H18.668V9.33334H30.668V8Z" fill="white"/>
                                                                <path d="M36 8H32V9.33334H36V8Z" fill="white"/>
                                                                <path d="M22.668 10.667H18.668V12.0003H22.668V10.667Z"
                                                                      fill="white"/>
                                                                <path d="M32 10.667H24V12.0003H32V10.667Z" fill="white"/>
                                                                <path d="M36.0013 13.333H18.668V14.6663H36.0013V13.333Z"
                                                                      fill="white"/>
                                                                <path d="M30.6654 16H25.332V17.3333H30.6654V16Z"
                                                                      fill="white"/>
                                                                <path d="M36 16H32V17.3333H36V16Z" fill="white"/>
                                                                <path d="M35.9987 10.667H33.332V12.0003H35.9987V10.667Z"
                                                                      fill="white"/>
                                                            </svg>
                                                            Обсудить
                                                            <span class="absolute">
                                                                <span class="statistics green">{{$normativ_rule->pro_positions_count($normativ_rule->id)}}</span>
                                                                <span class="statistics red">{{$normativ_rule->contra_positions_count($normativ_rule->id)}}</span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="stick"></div>
                                            @endif
                                            @endforeach
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section id="discussion" class="discussion tab_block" data-tab="discussion">

    </section>

    <section id="compare" class="comparison tab_block" data-tab="comparison">
        <input type="hidden" id="total_comp_boxes" value="1">
        <div class="container">
            <h1 class="section_title">Сравнительно-правовой анализ</h1>

            <div class="d-flex dragscroll activated">
                <div class="comparison_box add_compare_box" id="comp_box_1">
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
        <script>
            function load_contra_positions(position_id, pro_contra, type) {
                $.ajax({
                    url: "/get_contra_positions",
                    type: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "position_id": position_id,
                        "pro_contra": pro_contra,
                        "type": type,
                    },
                    success: function (response) {
                        $("#positions_to_ajax_load").html(response);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#positions_to_ajax_load").offset().top
                }, 1000);
                $("#position_add_form").css('display', 'none');
            }

            function scroll_to_form(pro_contra_selected, name_surname, vs_position_id) {
                if (pro_contra_selected == "pro") {
                    $("#select_pro_contra").val('pro');
                    $("#select_pro_contra").prop('disabled', 'disabled');
                    $("#text_pro_contra").val('pro');
                } else {
                    $("#select_pro_contra").val('contra');
                    $("#select_pro_contra").prop('disabled', 'disabled');
                    $("#text_pro_contra").val('contra');
                }
                $("#vs_position_id").val(vs_position_id);
                $("#versus_whom").text("против позиции автора - " + name_surname);
                $("#position_add_form").css('display', 'block');
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#position_add_form").offset().top
                }, 800);
            }

            function scroll_to_form_2(pro_contra) {
                if(pro_contra == "contra"){
                    $("#select_pro_contra").val("contra");
                }else{
                    $("#select_pro_contra").val("pro");
                }
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#position_add_form").offset().top
                }, 800);
            }

            function load_doctrine(position_id) {

                $.ajax({
                    url: "/get_doctrine",
                    type: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "position_id": position_id,
                    },
                    success: function (response) {
                        $("#doctrine_modal #text_doctrine_modal").html(response);
                        $("#doctrine_modal").modal("show");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });

            }
            function close_box(box_id) {
                $('#comp_box_' + box_id).remove();
                $('#arrow_box_' + box_id).remove();
                total = +$('#total_comp_boxes').val();
                for (var i = 1; i <= total; i++) {
                    if ($("#arrow_box_" + i).is(':first-child')) {
                        $("#arrow_box_" + i).remove();
                    }
                }
            }

            function add_compare_box_jurisdictions(now_institute) {

                $.ajax({
                    url: "/get_compare_box_jurisdictions",
                    type: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "now_institute": now_institute,
                    },
                    success: function (response) {
                        comp_box_id = +$('#total_comp_boxes').val();
                        $('#comp_box_' + comp_box_id).html(response);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });

            }
            function load_discussion(rule_id, type) {

                $.ajax({
                    url: "/get_discussion",
                    type: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "rule_id": rule_id,
                        "type": type,
                    },
                    success: function (response) {
                        $("#discussion").show();
                        $("#discussion").html(response);
                        var allEditors = document.querySelectorAll('.ckeditor');
                        for (var i = 0; i < allEditors.length; ++i) {
                            ClassicEditor.create(
                                allEditors[i], {}
                            );
                        }
                        $('.tab_buttons button, .btns a').on('click', function () {
                            $(this).addClass('active').siblings('button').removeClass('active');
                            $('.tab_block[data-tab=' + $(this).data("tab-open") + ']').addClass('active').siblings('.tab_block').removeClass('active');
                        });



                        $([document.documentElement, document.body]).animate({
                            scrollTop: $("#discussion_block_all_positions").offset().top
                        }, 500);
                        $("#select_pro_contra").prop('disabled', false);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }

                });
            }
            $(document).ready(function() {
                $('body').on('click', '.add_argument_btn', function () {
                    $('.add_argument .head').append(`<div class="theargument">
                            <button class="close">
                            <svg viewBox="0 0 329.26933 329" xmlns="http://www.w3.org/2000/svg"><path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg>
                            </button>
                            <div class="name">Аргумент <span class="index"></span></div>
                            <textarea id="argument_for_position"></textarea>
                            </div>`);

                    $('.theargument').each(function (index) {
                        index++;
                        $(this).find('.index').text(index);
                        $(this).find("#argument_for_position").attr('name', 'argument_' + index);
                        $("#total_arguments").val(index);
                    });
                });

                $('body').on('click', '.theargument .close', function () {
                    index = 0;
                    $(this).parent('.theargument').remove();
                    $('.theargument').each(function () {
                        index++;
                        $(this).find('.index').text(index);
                        $(this).find("#argument_for_position").attr('name', 'argument_' + index);
                        $("#total_arguments").val(index);
                    });
                });
                @if(session()->has('success'))
                    $("#bd-example-modal-lg").modal("show");
                @endif
                $('.top_button_item').click(function () {
                    $('.comparison').hide();
                    $('.comparison .dragscroll').html(`<div class="comparison_box add_compare_box" id="comp_box_1"></div>`);
                    $('#total_comp_boxes').val('1');

                    if(!$(this).hasClass('loading')){

                        /* HIGHLIGHT CURRENT TAB */
                        $('.loader').addClass('active');
                        $('.top_button_item .item').removeClass('active');

                        /* DISABLE BUTTONS WHILE LOADING */
                        $('.top_button_item').addClass('loading');

                        $(this).find('.item').addClass('active');

                        /* CLEAR OLD DATA */
                        $('.performance .tab_buttons').html('');
                        $('.performance_column').html('');
                        $('.academic').html('');
                        $('.normative').html('');
                        $('.principles .grid').html('');

                        $('section.discussion').hide();
                        $('.performance .alert-warning').remove();

                        $.ajax({
                            url: "/doc/fetch-institutional/{{$now_npa[0]->id}}/institute/" + $(this).attr('id'),
                            type: "get",
                            success: function (response) {

                                /* OUTPUT LINKS */
                                if(typeof response.children_institutes != "undefined"){
                                    $('.principles').show();
                                    for(var i = 0; i < response.children_institutes.length; i++){
                                        $('.principles .grid').append(`
                                        <a href="javascript:void(0);" id=` + response.children_institutes[i].id + `>` + response.children_institutes[i].name + `</a>
                                        `);
                                    }
                                }else{
                                    $('.principles').hide();
                                }
                                

                                /* OUTPUT JURISDICTIONS */
                                if(response.jurisdictions.length > 0){
                                    for(var i = 0; i < response.jurisdictions.length; i++){
                                        $('.performance .tab_buttons').append(`
                                                <button type="button" data-tab-open="` + response.jurisdictions[i].id + `" class="">
                                                    <span class="icon"><img src="` + response.jurisdictions[i].avatar.url + `"></span>
                                                    <span class="name">` + response.jurisdictions[i].name + `</span>
                                                </button>
                                                `);
                                        $('.performance_column').append(`
                                            <div class="tab_block" data-tab="` + response.jurisdictions[i].id + `">
                                                <div class="tab_content">
                                                    <div class="row">
                                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div class="scroll_box ">
                                                                <div class="item">
                                                                    <h2 class="block_title">Академическое представление</h2>
                                                                    <div class="academic"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                                            <div class="scroll_box ">
                                                                <div class="item">
                                                                    <h2 class="block_title">Нормативное представление</h2>
                                                                    <div class="normative"></div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>`
                                        );
                                    }
                                }else{
                                    $('.performance').append(`<div class="alert alert-warning container text-center">Выберите один из институтов</div>`);
                                }


                                $('body').find('.performance_column .tab_block').each(function(){
                                    /* OUTPUT ACADEMIC RULES PRACTISES */
                                    for(var i = 0; i < response.academic_discuss_rules.length; i++) {
                                        if (response.academic_discuss_rules[i].jurisdiction_id == $(this).data('tab')) {

                                            var pro_count = response.academic_discuss_rules[i].positions.reduce(function(n, item) {
                                                return n + ('pro' === item.pro_contra);
                                            }, 0)
                                            var contra_count = response.academic_discuss_rules[i].positions.reduce(function(n, item) {
                                                return n + ('contra' === item.pro_contra);
                                            }, 0);

                                            $(this).find('.academic').append(response.academic_discuss_rules[i].rule + `
                                            <div class="btns">

                                                <a href="#compare" data-type="academ" data-id="` + response.academic_discuss_rules[i].jurisdiction_id + `"  class="compare_btn" data-tab-open="comparison">
                                                        <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M39.933 24.3899L39.939 24.3879L33.3717 10.0587L34.9644 9.26601L34.3684 8.07268L32.8157 8.84601L32.6057 8.38801C32.4522 8.05335 32.0565 7.90644 31.7219 8.05985C31.5767 8.12643 31.4603 8.24285 31.3937 8.38801L31.2064 8.79735C30.0764 8.27252 28.8457 8.00019 27.5998 7.99935H21.9998V6.66603H23.3331V5.33271H22.1871L22.6665 3.41472C23.0345 1.94198 22.139 0.449745 20.6662 0.0816651C19.1935 -0.286332 17.7013 0.609161 17.3332 2.0819C17.2239 2.5194 17.2239 2.97714 17.3332 3.41464L17.8125 5.33262H16.6665V6.66595H17.9998V7.99927H12.4032C11.1561 8.00027 9.92409 8.27327 8.79326 8.79926L8.60593 8.38993C8.45252 8.05527 8.05677 7.90835 7.72211 8.06177C7.57694 8.12835 7.46052 8.24477 7.39394 8.38993L7.18394 8.84793L5.63129 8.07127L5.0353 9.26593L6.62795 10.0619L0.060675 24.3879C0.0201753 24.475 -0.000491135 24.5699 8.86038e-06 24.6659C0.0029255 27.2419 2.09057 29.3296 4.66663 29.3325H11.3332C13.9093 29.3296 15.9969 27.2419 15.9999 24.6659C15.9982 24.5701 15.9754 24.4759 15.9332 24.3899L9.34792 10.0067C10.3052 9.56234 11.3479 9.33234 12.4032 9.33267H17.9998V33.7231L15.7239 35.9991H13.3332C11.4932 36.0013 10.0021 37.4924 9.99992 39.3324C9.99992 39.7006 10.2984 39.9991 10.6666 39.9991H29.3331C29.7012 39.9991 29.9997 39.7006 29.9997 39.3324C29.9976 37.4924 28.5064 36.0013 26.6664 35.9991H24.2758L21.9998 33.7231V9.33267H27.5998C28.6551 9.33234 29.6978 9.56234 30.6551 10.0067L24.0605 24.3879C24.02 24.475 23.9993 24.5699 23.9998 24.6659C24.0027 27.2419 26.0904 29.3296 28.6664 29.3325H35.333C37.9091 29.3296 39.9967 27.2419 39.9996 24.6659C39.9981 24.5701 39.9753 24.4759 39.933 24.3899ZM11.3332 27.9992H4.66663C3.08315 27.9974 1.71874 26.8836 1.4 25.3325H14.5999C14.2812 26.8836 12.9167 27.9974 11.3332 27.9992ZM14.2945 23.9992H1.70533L7.82927 10.6387C7.9411 10.6687 8.05877 10.6687 8.1706 10.6387L14.2945 23.9992ZM18.8845 1.87673C19.4037 1.26074 20.3239 1.18232 20.9399 1.70149C21.0032 1.75482 21.0618 1.81348 21.1152 1.87673C21.3848 2.21973 21.4801 2.66839 21.3731 3.09139L20.8125 5.33271H19.1872L18.6265 3.09139C18.5196 2.66839 18.6149 2.21973 18.8845 1.87673ZM19.3332 7.99935V6.66603H20.6665V7.99935H19.3332ZM20.6665 9.33267V33.3325H19.3332V9.33267H20.6665ZM26.6664 37.3325C27.5138 37.3335 28.2688 37.8673 28.5524 38.6658H11.4472C11.7308 37.8673 12.4859 37.3335 13.3332 37.3325H26.6664ZM22.3905 35.9991H17.6092L18.9425 34.6658H21.0572L22.3905 35.9991ZM31.8277 10.6407C31.9378 10.6794 32.0586 10.6744 32.1651 10.6267L38.2943 23.9992H25.7051L31.8277 10.6407ZM35.333 27.9992H28.6664C27.0829 27.9974 25.7185 26.8836 25.3998 25.3325H38.5997C38.281 26.8836 36.9165 27.9974 35.333 27.9992Z"
                                                                  fill="#C9B48C"/>
                                                        </svg>
                                                        Сравнить
                                                    </a>
                                                    <a href="#discussion" onClick="load_discussion(` + response.academic_discuss_rules[i].id + `, 'discusses');" data-tab-open="discussion" class="discuss_btn">
                                                        <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M36 4H18.6667C16.4584 4.00217 14.6688 5.79175 14.6667 8V16H4C1.79175 16.0022 0.00216667 17.7918 0 20V29.3333C0.00241667 31.2844 1.41 32.9501 3.33333 33.278V35.3333C3.33342 35.7015 3.63192 35.9999 4.00017 35.9998C4.17692 35.9998 4.34633 35.9296 4.47133 35.8047L6.94267 33.3333H21.3333C23.5416 33.3312 25.3312 31.5416 25.3333 29.3333V21.3333H33.0573L35.5287 23.8047C35.7891 24.065 36.2112 24.0649 36.4715 23.8045C36.5964 23.6795 36.6667 23.51 36.6667 23.3333V21.278C38.59 20.9501 39.9976 19.2844 40 17.3333V8C39.9978 5.79175 38.2083 4.00217 36 4ZM24 29.3333C24 30.8061 22.8061 32 21.3333 32H6.66667C6.48983 32 6.32033 32.0703 6.19533 32.1953L4.66667 33.724V32.6667C4.66667 32.2985 4.36817 32 4 32C2.52725 32 1.33333 30.8061 1.33333 29.3333V20C1.33333 18.5273 2.52725 17.3333 4 17.3333H21.3333C22.8061 17.3333 24 18.5273 24 20V29.3333ZM38.6667 17.3333C38.6667 18.8061 37.4728 20 36 20C35.6318 20 35.3333 20.2985 35.3333 20.6667V21.724L33.8047 20.1953C33.6797 20.0703 33.5102 20 33.3333 20H25.3333C25.3312 17.7918 23.5416 16.0022 21.3333 16H16V8C16 6.52725 17.1939 5.33333 18.6667 5.33333H36C37.4728 5.33333 38.6667 6.52725 38.6667 8V17.3333Z"
                                                                  fill="white"/>
                                                            <path d="M7.33203 22.667C6.22745 22.667 5.33203 23.5624 5.33203 24.667C5.33203 25.7716 6.22745 26.667 7.33203 26.667C8.43661 26.667 9.33203 25.7716 9.33203 24.667C9.33203 23.5624 8.43661 22.667 7.33203 22.667ZM7.33203 25.3337C6.96386 25.3337 6.66536 25.0352 6.66536 24.667C6.66536 24.2988 6.96386 24.0003 7.33203 24.0003C7.7002 24.0003 7.9987 24.2988 7.9987 24.667C7.9987 25.0352 7.7002 25.3337 7.33203 25.3337Z"
                                                                  fill="white"/>
                                                            <path d="M12.668 22.667C11.5634 22.667 10.668 23.5624 10.668 24.667C10.668 25.7716 11.5634 26.667 12.668 26.667C13.7726 26.667 14.668 25.7716 14.668 24.667C14.668 23.5624 13.7726 22.667 12.668 22.667ZM12.668 25.3337C12.2998 25.3337 12.0013 25.0352 12.0013 24.667C12.0013 24.2988 12.2998 24.0003 12.668 24.0003C13.0361 24.0003 13.3346 24.2988 13.3346 24.667C13.3346 25.0352 13.0361 25.3337 12.668 25.3337Z"
                                                                  fill="white"/>
                                                            <path d="M18 22.667C16.8954 22.667 16 23.5624 16 24.667C16 25.7716 16.8954 26.667 18 26.667C19.1046 26.667 20 25.7716 20 24.667C20 23.5624 19.1046 22.667 18 22.667ZM18 25.3337C17.6318 25.3337 17.3333 25.0352 17.3333 24.667C17.3333 24.2988 17.6318 24.0003 18 24.0003C18.3682 24.0003 18.6667 24.2988 18.6667 24.667C18.6667 25.0352 18.3682 25.3337 18 25.3337Z"
                                                                  fill="white"/>
                                                            <path d="M30.668 8H18.668V9.33334H30.668V8Z" fill="white"/>
                                                            <path d="M36 8H32V9.33334H36V8Z" fill="white"/>
                                                            <path d="M22.668 10.667H18.668V12.0003H22.668V10.667Z"
                                                                  fill="white"/>
                                                            <path d="M32 10.667H24V12.0003H32V10.667Z" fill="white"/>
                                                            <path d="M36.0013 13.333H18.668V14.6663H36.0013V13.333Z"
                                                                  fill="white"/>
                                                            <path d="M30.6654 16H25.332V17.3333H30.6654V16Z" fill="white"/>
                                                            <path d="M36 16H32V17.3333H36V16Z" fill="white"/>
                                                            <path d="M35.9987 10.667H33.332V12.0003H35.9987V10.667Z"
                                                                  fill="white"/>
                                                        </svg>
                                                        Обсудить
                                                        <span class="absolute">
                                                            <span class="statistics green">` + pro_count + `</span>
                                                            <span class="statistics red">` + contra_count + `</span>
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="stick"></div>
                                        `);
                                        }
                                    }

                                    /* OUTPUT NORMATIVE RULES PRACTISES */
                                    for(var i = 0; i < response.normativ_discuss_rules.length; i++) {
                                        var pro_count = response.normativ_discuss_rules[i].positions.reduce(function(n, item) {
                                            return n + ('pro' === item.pro_contra);
                                        }, 0)
                                        var contra_count = response.normativ_discuss_rules[i].positions.reduce(function(n, item) {
                                            return n + ('contra' === item.pro_contra);
                                        }, 0);
                                        if (response.normativ_discuss_rules[i].jurisdiction_id == $(this).data('tab')) {
                                            $(this).find('.normative').append(response.normativ_discuss_rules[i].rule + `
                                            <div class="btns">
                                                <a href="#compare" data-type="normativ" data-id="` + response.normativ_discuss_rules[i].jurisdiction_id + `"  class="compare_btn" data-tab-open="comparison">
                                                    <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M39.933 24.3899L39.939 24.3879L33.3717 10.0587L34.9644 9.26601L34.3684 8.07268L32.8157 8.84601L32.6057 8.38801C32.4522 8.05335 32.0565 7.90644 31.7219 8.05985C31.5767 8.12643 31.4603 8.24285 31.3937 8.38801L31.2064 8.79735C30.0764 8.27252 28.8457 8.00019 27.5998 7.99935H21.9998V6.66603H23.3331V5.33271H22.1871L22.6665 3.41472C23.0345 1.94198 22.139 0.449745 20.6662 0.0816651C19.1935 -0.286332 17.7013 0.609161 17.3332 2.0819C17.2239 2.5194 17.2239 2.97714 17.3332 3.41464L17.8125 5.33262H16.6665V6.66595H17.9998V7.99927H12.4032C11.1561 8.00027 9.92409 8.27327 8.79326 8.79926L8.60593 8.38993C8.45252 8.05527 8.05677 7.90835 7.72211 8.06177C7.57694 8.12835 7.46052 8.24477 7.39394 8.38993L7.18394 8.84793L5.63129 8.07127L5.0353 9.26593L6.62795 10.0619L0.060675 24.3879C0.0201753 24.475 -0.000491135 24.5699 8.86038e-06 24.6659C0.0029255 27.2419 2.09057 29.3296 4.66663 29.3325H11.3332C13.9093 29.3296 15.9969 27.2419 15.9999 24.6659C15.9982 24.5701 15.9754 24.4759 15.9332 24.3899L9.34792 10.0067C10.3052 9.56234 11.3479 9.33234 12.4032 9.33267H17.9998V33.7231L15.7239 35.9991H13.3332C11.4932 36.0013 10.0021 37.4924 9.99992 39.3324C9.99992 39.7006 10.2984 39.9991 10.6666 39.9991H29.3331C29.7012 39.9991 29.9997 39.7006 29.9997 39.3324C29.9976 37.4924 28.5064 36.0013 26.6664 35.9991H24.2758L21.9998 33.7231V9.33267H27.5998C28.6551 9.33234 29.6978 9.56234 30.6551 10.0067L24.0605 24.3879C24.02 24.475 23.9993 24.5699 23.9998 24.6659C24.0027 27.2419 26.0904 29.3296 28.6664 29.3325H35.333C37.9091 29.3296 39.9967 27.2419 39.9996 24.6659C39.9981 24.5701 39.9753 24.4759 39.933 24.3899ZM11.3332 27.9992H4.66663C3.08315 27.9974 1.71874 26.8836 1.4 25.3325H14.5999C14.2812 26.8836 12.9167 27.9974 11.3332 27.9992ZM14.2945 23.9992H1.70533L7.82927 10.6387C7.9411 10.6687 8.05877 10.6687 8.1706 10.6387L14.2945 23.9992ZM18.8845 1.87673C19.4037 1.26074 20.3239 1.18232 20.9399 1.70149C21.0032 1.75482 21.0618 1.81348 21.1152 1.87673C21.3848 2.21973 21.4801 2.66839 21.3731 3.09139L20.8125 5.33271H19.1872L18.6265 3.09139C18.5196 2.66839 18.6149 2.21973 18.8845 1.87673ZM19.3332 7.99935V6.66603H20.6665V7.99935H19.3332ZM20.6665 9.33267V33.3325H19.3332V9.33267H20.6665ZM26.6664 37.3325C27.5138 37.3335 28.2688 37.8673 28.5524 38.6658H11.4472C11.7308 37.8673 12.4859 37.3335 13.3332 37.3325H26.6664ZM22.3905 35.9991H17.6092L18.9425 34.6658H21.0572L22.3905 35.9991ZM31.8277 10.6407C31.9378 10.6794 32.0586 10.6744 32.1651 10.6267L38.2943 23.9992H25.7051L31.8277 10.6407ZM35.333 27.9992H28.6664C27.0829 27.9974 25.7185 26.8836 25.3998 25.3325H38.5997C38.281 26.8836 36.9165 27.9974 35.333 27.9992Z"
                                                              fill="#C9B48C"/>
                                                    </svg>
                                                    Сравнить
                                                </a>
                                                <a href="#discussion" onClick="load_discussion(` + response.normativ_discuss_rules[i].id + `, 'discusses');" data-tab-open="discussion" class="discuss_btn">
                                                    <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M36 4H18.6667C16.4584 4.00217 14.6688 5.79175 14.6667 8V16H4C1.79175 16.0022 0.00216667 17.7918 0 20V29.3333C0.00241667 31.2844 1.41 32.9501 3.33333 33.278V35.3333C3.33342 35.7015 3.63192 35.9999 4.00017 35.9998C4.17692 35.9998 4.34633 35.9296 4.47133 35.8047L6.94267 33.3333H21.3333C23.5416 33.3312 25.3312 31.5416 25.3333 29.3333V21.3333H33.0573L35.5287 23.8047C35.7891 24.065 36.2112 24.0649 36.4715 23.8045C36.5964 23.6795 36.6667 23.51 36.6667 23.3333V21.278C38.59 20.9501 39.9976 19.2844 40 17.3333V8C39.9978 5.79175 38.2083 4.00217 36 4ZM24 29.3333C24 30.8061 22.8061 32 21.3333 32H6.66667C6.48983 32 6.32033 32.0703 6.19533 32.1953L4.66667 33.724V32.6667C4.66667 32.2985 4.36817 32 4 32C2.52725 32 1.33333 30.8061 1.33333 29.3333V20C1.33333 18.5273 2.52725 17.3333 4 17.3333H21.3333C22.8061 17.3333 24 18.5273 24 20V29.3333ZM38.6667 17.3333C38.6667 18.8061 37.4728 20 36 20C35.6318 20 35.3333 20.2985 35.3333 20.6667V21.724L33.8047 20.1953C33.6797 20.0703 33.5102 20 33.3333 20H25.3333C25.3312 17.7918 23.5416 16.0022 21.3333 16H16V8C16 6.52725 17.1939 5.33333 18.6667 5.33333H36C37.4728 5.33333 38.6667 6.52725 38.6667 8V17.3333Z"
                                                              fill="white"/>
                                                        <path d="M7.33203 22.667C6.22745 22.667 5.33203 23.5624 5.33203 24.667C5.33203 25.7716 6.22745 26.667 7.33203 26.667C8.43661 26.667 9.33203 25.7716 9.33203 24.667C9.33203 23.5624 8.43661 22.667 7.33203 22.667ZM7.33203 25.3337C6.96386 25.3337 6.66536 25.0352 6.66536 24.667C6.66536 24.2988 6.96386 24.0003 7.33203 24.0003C7.7002 24.0003 7.9987 24.2988 7.9987 24.667C7.9987 25.0352 7.7002 25.3337 7.33203 25.3337Z"
                                                              fill="white"/>
                                                        <path d="M12.668 22.667C11.5634 22.667 10.668 23.5624 10.668 24.667C10.668 25.7716 11.5634 26.667 12.668 26.667C13.7726 26.667 14.668 25.7716 14.668 24.667C14.668 23.5624 13.7726 22.667 12.668 22.667ZM12.668 25.3337C12.2998 25.3337 12.0013 25.0352 12.0013 24.667C12.0013 24.2988 12.2998 24.0003 12.668 24.0003C13.0361 24.0003 13.3346 24.2988 13.3346 24.667C13.3346 25.0352 13.0361 25.3337 12.668 25.3337Z"
                                                              fill="white"/>
                                                        <path d="M18 22.667C16.8954 22.667 16 23.5624 16 24.667C16 25.7716 16.8954 26.667 18 26.667C19.1046 26.667 20 25.7716 20 24.667C20 23.5624 19.1046 22.667 18 22.667ZM18 25.3337C17.6318 25.3337 17.3333 25.0352 17.3333 24.667C17.3333 24.2988 17.6318 24.0003 18 24.0003C18.3682 24.0003 18.6667 24.2988 18.6667 24.667C18.6667 25.0352 18.3682 25.3337 18 25.3337Z"
                                                              fill="white"/>
                                                        <path d="M30.668 8H18.668V9.33334H30.668V8Z" fill="white"/>
                                                        <path d="M36 8H32V9.33334H36V8Z" fill="white"/>
                                                        <path d="M22.668 10.667H18.668V12.0003H22.668V10.667Z"
                                                              fill="white"/>
                                                        <path d="M32 10.667H24V12.0003H32V10.667Z" fill="white"/>
                                                        <path d="M36.0013 13.333H18.668V14.6663H36.0013V13.333Z"
                                                              fill="white"/>
                                                        <path d="M30.6654 16H25.332V17.3333H30.6654V16Z"
                                                              fill="white"/>
                                                        <path d="M36 16H32V17.3333H36V16Z" fill="white"/>
                                                        <path d="M35.9987 10.667H33.332V12.0003H35.9987V10.667Z"
                                                              fill="white"/>
                                                    </svg>
                                                    Обсудить
                                                    <span class="absolute">
                                                        <span class="statistics green">` + pro_count + `</span>
                                                            <span class="statistics red">` + contra_count + `</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="stick"></div>
                                        `);
                                        }
                                    }
                                });
                                
                                $('.top_button_item').removeClass('loading');

                                $('.loader').removeClass('active');
                                /* MAKE FIRST JURISDICTION ACTIVE */
                                $('.tab_buttons button:first-child').trigger('click');
                            }
                        });
                        $('.active_institute_id').val($(this).attr('id'));
                    }
                });
                $('body').on('click', '.principles .grid a', function () {
                    if(!$(this).hasClass('loading')){

                        /* HIGHLIGHT CURRENT TAB */
                        $('.principles .grid a').addClass('loading');
                    
                        $('.comparison').hide();
                        $('.comparison .dragscroll').html(`<div class="comparison_box add_compare_box" id="comp_box_1"></div>`);
                        $('#total_comp_boxes').val('1');

                        $('.loader').addClass('active');
                        $('.principles .grid a').removeClass('active');
                        $(this).addClass('active');

                        /* CLEAR OLD DATA */
                        $('.performance_column').html('');
                        $('.academic').html('');
                        $('.normative').html('');
                        $('.performance .alert-warning').remove();
                        $('.performance .tab_buttons').html('');
                        $('section.discussion').hide();

                        $.ajax({
                            url: "/doc/fetch-institutional/{{$now_npa[0]->id}}/institute/" + $(this).attr('id'),
                            type: "get",
                            success: function (response) {
                                $('.loader').removeClass('active');
                                /* OUTPUT JURISDICTIONS */
                                if(response.jurisdictions.length > 0){
                                    for(var i = 0; i < response.jurisdictions.length; i++){
                                        $('.performance .tab_buttons').append(`
                                    <button type="button" data-tab-open="` + response.jurisdictions[i].id + `" class="">
                                        <span class="icon"><img src="` + response.jurisdictions[i].avatar.url + `"></span>
                                        <span class="name">` + response.jurisdictions[i].name + `</span>
                                    </button>
                                    `);
                                        $('.performance_column').append(`
                                    <div class="tab_block" data-tab="` + response.jurisdictions[i].id + `">
                                        <div class="tab_content">
                                            <div class="row">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="scroll_box ">
                                                        <div class="item">
                                                            <h2 class="block_title">Академическое представление</h2>
                                                            <div class="academic"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="scroll_box ">
                                                        <div class="item">
                                                            <h2 class="block_title">Нормативное представление</h2>
                                                            <div class="normative"></div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`
                                        );
                                    }
                                }else{
                                    $('.performance').append(`<div class="alert alert-warning container text-center">Выберите один из институтов</div>`);
                                }



                                $('body').find('.performance_column .tab_block').each(function(){

                                    /* OUTPUT ACADEMIC RULES PRACTISES */
                                    for(var i = 0; i < response.academic_discuss_rules.length; i++) {
                                        if (response.academic_discuss_rules[i].jurisdiction_id == $(this).data('tab')) {

                                            var pro_count = response.academic_discuss_rules[i].positions.reduce(function(n, item) {
                                                return n + ('pro' === item.pro_contra);
                                            }, 0)
                                            var contra_count = response.academic_discuss_rules[i].positions.reduce(function(n, item) {
                                                return n + ('contra' === item.pro_contra);
                                            }, 0);

                                            $(this).find('.academic').append(response.academic_discuss_rules[i].rule + `
                                            <div class="btns">

                                                <a href="#compare" data-type="academ" data-id="` + response.academic_discuss_rules[i].jurisdiction_id + `"  class="compare_btn" data-tab-open="comparison">
                                                        <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M39.933 24.3899L39.939 24.3879L33.3717 10.0587L34.9644 9.26601L34.3684 8.07268L32.8157 8.84601L32.6057 8.38801C32.4522 8.05335 32.0565 7.90644 31.7219 8.05985C31.5767 8.12643 31.4603 8.24285 31.3937 8.38801L31.2064 8.79735C30.0764 8.27252 28.8457 8.00019 27.5998 7.99935H21.9998V6.66603H23.3331V5.33271H22.1871L22.6665 3.41472C23.0345 1.94198 22.139 0.449745 20.6662 0.0816651C19.1935 -0.286332 17.7013 0.609161 17.3332 2.0819C17.2239 2.5194 17.2239 2.97714 17.3332 3.41464L17.8125 5.33262H16.6665V6.66595H17.9998V7.99927H12.4032C11.1561 8.00027 9.92409 8.27327 8.79326 8.79926L8.60593 8.38993C8.45252 8.05527 8.05677 7.90835 7.72211 8.06177C7.57694 8.12835 7.46052 8.24477 7.39394 8.38993L7.18394 8.84793L5.63129 8.07127L5.0353 9.26593L6.62795 10.0619L0.060675 24.3879C0.0201753 24.475 -0.000491135 24.5699 8.86038e-06 24.6659C0.0029255 27.2419 2.09057 29.3296 4.66663 29.3325H11.3332C13.9093 29.3296 15.9969 27.2419 15.9999 24.6659C15.9982 24.5701 15.9754 24.4759 15.9332 24.3899L9.34792 10.0067C10.3052 9.56234 11.3479 9.33234 12.4032 9.33267H17.9998V33.7231L15.7239 35.9991H13.3332C11.4932 36.0013 10.0021 37.4924 9.99992 39.3324C9.99992 39.7006 10.2984 39.9991 10.6666 39.9991H29.3331C29.7012 39.9991 29.9997 39.7006 29.9997 39.3324C29.9976 37.4924 28.5064 36.0013 26.6664 35.9991H24.2758L21.9998 33.7231V9.33267H27.5998C28.6551 9.33234 29.6978 9.56234 30.6551 10.0067L24.0605 24.3879C24.02 24.475 23.9993 24.5699 23.9998 24.6659C24.0027 27.2419 26.0904 29.3296 28.6664 29.3325H35.333C37.9091 29.3296 39.9967 27.2419 39.9996 24.6659C39.9981 24.5701 39.9753 24.4759 39.933 24.3899ZM11.3332 27.9992H4.66663C3.08315 27.9974 1.71874 26.8836 1.4 25.3325H14.5999C14.2812 26.8836 12.9167 27.9974 11.3332 27.9992ZM14.2945 23.9992H1.70533L7.82927 10.6387C7.9411 10.6687 8.05877 10.6687 8.1706 10.6387L14.2945 23.9992ZM18.8845 1.87673C19.4037 1.26074 20.3239 1.18232 20.9399 1.70149C21.0032 1.75482 21.0618 1.81348 21.1152 1.87673C21.3848 2.21973 21.4801 2.66839 21.3731 3.09139L20.8125 5.33271H19.1872L18.6265 3.09139C18.5196 2.66839 18.6149 2.21973 18.8845 1.87673ZM19.3332 7.99935V6.66603H20.6665V7.99935H19.3332ZM20.6665 9.33267V33.3325H19.3332V9.33267H20.6665ZM26.6664 37.3325C27.5138 37.3335 28.2688 37.8673 28.5524 38.6658H11.4472C11.7308 37.8673 12.4859 37.3335 13.3332 37.3325H26.6664ZM22.3905 35.9991H17.6092L18.9425 34.6658H21.0572L22.3905 35.9991ZM31.8277 10.6407C31.9378 10.6794 32.0586 10.6744 32.1651 10.6267L38.2943 23.9992H25.7051L31.8277 10.6407ZM35.333 27.9992H28.6664C27.0829 27.9974 25.7185 26.8836 25.3998 25.3325H38.5997C38.281 26.8836 36.9165 27.9974 35.333 27.9992Z"
                                                                  fill="#C9B48C"/>
                                                        </svg>
                                                        Сравнить
                                                    </a>
                                                    <a href="#discussion" onClick="load_discussion(` + response.academic_discuss_rules[i].id + `, 'discusses');" data-tab-open="discussion" class="discuss_btn">
                                                        <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                                             xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M36 4H18.6667C16.4584 4.00217 14.6688 5.79175 14.6667 8V16H4C1.79175 16.0022 0.00216667 17.7918 0 20V29.3333C0.00241667 31.2844 1.41 32.9501 3.33333 33.278V35.3333C3.33342 35.7015 3.63192 35.9999 4.00017 35.9998C4.17692 35.9998 4.34633 35.9296 4.47133 35.8047L6.94267 33.3333H21.3333C23.5416 33.3312 25.3312 31.5416 25.3333 29.3333V21.3333H33.0573L35.5287 23.8047C35.7891 24.065 36.2112 24.0649 36.4715 23.8045C36.5964 23.6795 36.6667 23.51 36.6667 23.3333V21.278C38.59 20.9501 39.9976 19.2844 40 17.3333V8C39.9978 5.79175 38.2083 4.00217 36 4ZM24 29.3333C24 30.8061 22.8061 32 21.3333 32H6.66667C6.48983 32 6.32033 32.0703 6.19533 32.1953L4.66667 33.724V32.6667C4.66667 32.2985 4.36817 32 4 32C2.52725 32 1.33333 30.8061 1.33333 29.3333V20C1.33333 18.5273 2.52725 17.3333 4 17.3333H21.3333C22.8061 17.3333 24 18.5273 24 20V29.3333ZM38.6667 17.3333C38.6667 18.8061 37.4728 20 36 20C35.6318 20 35.3333 20.2985 35.3333 20.6667V21.724L33.8047 20.1953C33.6797 20.0703 33.5102 20 33.3333 20H25.3333C25.3312 17.7918 23.5416 16.0022 21.3333 16H16V8C16 6.52725 17.1939 5.33333 18.6667 5.33333H36C37.4728 5.33333 38.6667 6.52725 38.6667 8V17.3333Z"
                                                                  fill="white"/>
                                                            <path d="M7.33203 22.667C6.22745 22.667 5.33203 23.5624 5.33203 24.667C5.33203 25.7716 6.22745 26.667 7.33203 26.667C8.43661 26.667 9.33203 25.7716 9.33203 24.667C9.33203 23.5624 8.43661 22.667 7.33203 22.667ZM7.33203 25.3337C6.96386 25.3337 6.66536 25.0352 6.66536 24.667C6.66536 24.2988 6.96386 24.0003 7.33203 24.0003C7.7002 24.0003 7.9987 24.2988 7.9987 24.667C7.9987 25.0352 7.7002 25.3337 7.33203 25.3337Z"
                                                                  fill="white"/>
                                                            <path d="M12.668 22.667C11.5634 22.667 10.668 23.5624 10.668 24.667C10.668 25.7716 11.5634 26.667 12.668 26.667C13.7726 26.667 14.668 25.7716 14.668 24.667C14.668 23.5624 13.7726 22.667 12.668 22.667ZM12.668 25.3337C12.2998 25.3337 12.0013 25.0352 12.0013 24.667C12.0013 24.2988 12.2998 24.0003 12.668 24.0003C13.0361 24.0003 13.3346 24.2988 13.3346 24.667C13.3346 25.0352 13.0361 25.3337 12.668 25.3337Z"
                                                                  fill="white"/>
                                                            <path d="M18 22.667C16.8954 22.667 16 23.5624 16 24.667C16 25.7716 16.8954 26.667 18 26.667C19.1046 26.667 20 25.7716 20 24.667C20 23.5624 19.1046 22.667 18 22.667ZM18 25.3337C17.6318 25.3337 17.3333 25.0352 17.3333 24.667C17.3333 24.2988 17.6318 24.0003 18 24.0003C18.3682 24.0003 18.6667 24.2988 18.6667 24.667C18.6667 25.0352 18.3682 25.3337 18 25.3337Z"
                                                                  fill="white"/>
                                                            <path d="M30.668 8H18.668V9.33334H30.668V8Z" fill="white"/>
                                                            <path d="M36 8H32V9.33334H36V8Z" fill="white"/>
                                                            <path d="M22.668 10.667H18.668V12.0003H22.668V10.667Z"
                                                                  fill="white"/>
                                                            <path d="M32 10.667H24V12.0003H32V10.667Z" fill="white"/>
                                                            <path d="M36.0013 13.333H18.668V14.6663H36.0013V13.333Z"
                                                                  fill="white"/>
                                                            <path d="M30.6654 16H25.332V17.3333H30.6654V16Z" fill="white"/>
                                                            <path d="M36 16H32V17.3333H36V16Z" fill="white"/>
                                                            <path d="M35.9987 10.667H33.332V12.0003H35.9987V10.667Z"
                                                                  fill="white"/>
                                                        </svg>
                                                        Обсудить
                                                        <span class="absolute">
                                                            <span class="statistics green">` + pro_count + `</span>
                                                            <span class="statistics red">` + contra_count + `</span>
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="stick"></div>
                                        `);
                                        }
                                    }

                                    /* OUTPUT NORMATIVE RULES PRACTISES */
                                    for(var i = 0; i < response.normativ_discuss_rules.length; i++) {
                                        var pro_count = response.normativ_discuss_rules[i].positions.reduce(function(n, item) {
                                            return n + ('pro' === item.pro_contra);
                                        }, 0)
                                        var contra_count = response.normativ_discuss_rules[i].positions.reduce(function(n, item) {
                                            return n + ('contra' === item.pro_contra);
                                        }, 0);
                                        if (response.normativ_discuss_rules[i].jurisdiction_id == $(this).data('tab')) {
                                            $(this).find('.normative').append(response.normativ_discuss_rules[i].rule + `
                                            <div class="btns">
                                                <a href="#compare" data-type="normativ" data-id="` + response.normativ_discuss_rules[i].jurisdiction_id + `"  class="compare_btn" data-tab-open="comparison">
                                                    <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M39.933 24.3899L39.939 24.3879L33.3717 10.0587L34.9644 9.26601L34.3684 8.07268L32.8157 8.84601L32.6057 8.38801C32.4522 8.05335 32.0565 7.90644 31.7219 8.05985C31.5767 8.12643 31.4603 8.24285 31.3937 8.38801L31.2064 8.79735C30.0764 8.27252 28.8457 8.00019 27.5998 7.99935H21.9998V6.66603H23.3331V5.33271H22.1871L22.6665 3.41472C23.0345 1.94198 22.139 0.449745 20.6662 0.0816651C19.1935 -0.286332 17.7013 0.609161 17.3332 2.0819C17.2239 2.5194 17.2239 2.97714 17.3332 3.41464L17.8125 5.33262H16.6665V6.66595H17.9998V7.99927H12.4032C11.1561 8.00027 9.92409 8.27327 8.79326 8.79926L8.60593 8.38993C8.45252 8.05527 8.05677 7.90835 7.72211 8.06177C7.57694 8.12835 7.46052 8.24477 7.39394 8.38993L7.18394 8.84793L5.63129 8.07127L5.0353 9.26593L6.62795 10.0619L0.060675 24.3879C0.0201753 24.475 -0.000491135 24.5699 8.86038e-06 24.6659C0.0029255 27.2419 2.09057 29.3296 4.66663 29.3325H11.3332C13.9093 29.3296 15.9969 27.2419 15.9999 24.6659C15.9982 24.5701 15.9754 24.4759 15.9332 24.3899L9.34792 10.0067C10.3052 9.56234 11.3479 9.33234 12.4032 9.33267H17.9998V33.7231L15.7239 35.9991H13.3332C11.4932 36.0013 10.0021 37.4924 9.99992 39.3324C9.99992 39.7006 10.2984 39.9991 10.6666 39.9991H29.3331C29.7012 39.9991 29.9997 39.7006 29.9997 39.3324C29.9976 37.4924 28.5064 36.0013 26.6664 35.9991H24.2758L21.9998 33.7231V9.33267H27.5998C28.6551 9.33234 29.6978 9.56234 30.6551 10.0067L24.0605 24.3879C24.02 24.475 23.9993 24.5699 23.9998 24.6659C24.0027 27.2419 26.0904 29.3296 28.6664 29.3325H35.333C37.9091 29.3296 39.9967 27.2419 39.9996 24.6659C39.9981 24.5701 39.9753 24.4759 39.933 24.3899ZM11.3332 27.9992H4.66663C3.08315 27.9974 1.71874 26.8836 1.4 25.3325H14.5999C14.2812 26.8836 12.9167 27.9974 11.3332 27.9992ZM14.2945 23.9992H1.70533L7.82927 10.6387C7.9411 10.6687 8.05877 10.6687 8.1706 10.6387L14.2945 23.9992ZM18.8845 1.87673C19.4037 1.26074 20.3239 1.18232 20.9399 1.70149C21.0032 1.75482 21.0618 1.81348 21.1152 1.87673C21.3848 2.21973 21.4801 2.66839 21.3731 3.09139L20.8125 5.33271H19.1872L18.6265 3.09139C18.5196 2.66839 18.6149 2.21973 18.8845 1.87673ZM19.3332 7.99935V6.66603H20.6665V7.99935H19.3332ZM20.6665 9.33267V33.3325H19.3332V9.33267H20.6665ZM26.6664 37.3325C27.5138 37.3335 28.2688 37.8673 28.5524 38.6658H11.4472C11.7308 37.8673 12.4859 37.3335 13.3332 37.3325H26.6664ZM22.3905 35.9991H17.6092L18.9425 34.6658H21.0572L22.3905 35.9991ZM31.8277 10.6407C31.9378 10.6794 32.0586 10.6744 32.1651 10.6267L38.2943 23.9992H25.7051L31.8277 10.6407ZM35.333 27.9992H28.6664C27.0829 27.9974 25.7185 26.8836 25.3998 25.3325H38.5997C38.281 26.8836 36.9165 27.9974 35.333 27.9992Z"
                                                              fill="#C9B48C"/>
                                                    </svg>
                                                    Сравнить
                                                </a>
                                                <a href="#discussion" onClick="load_discussion(` + response.normativ_discuss_rules[i].id + `, 'discusses');" data-tab-open="discussion" class="discuss_btn">
                                                    <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M36 4H18.6667C16.4584 4.00217 14.6688 5.79175 14.6667 8V16H4C1.79175 16.0022 0.00216667 17.7918 0 20V29.3333C0.00241667 31.2844 1.41 32.9501 3.33333 33.278V35.3333C3.33342 35.7015 3.63192 35.9999 4.00017 35.9998C4.17692 35.9998 4.34633 35.9296 4.47133 35.8047L6.94267 33.3333H21.3333C23.5416 33.3312 25.3312 31.5416 25.3333 29.3333V21.3333H33.0573L35.5287 23.8047C35.7891 24.065 36.2112 24.0649 36.4715 23.8045C36.5964 23.6795 36.6667 23.51 36.6667 23.3333V21.278C38.59 20.9501 39.9976 19.2844 40 17.3333V8C39.9978 5.79175 38.2083 4.00217 36 4ZM24 29.3333C24 30.8061 22.8061 32 21.3333 32H6.66667C6.48983 32 6.32033 32.0703 6.19533 32.1953L4.66667 33.724V32.6667C4.66667 32.2985 4.36817 32 4 32C2.52725 32 1.33333 30.8061 1.33333 29.3333V20C1.33333 18.5273 2.52725 17.3333 4 17.3333H21.3333C22.8061 17.3333 24 18.5273 24 20V29.3333ZM38.6667 17.3333C38.6667 18.8061 37.4728 20 36 20C35.6318 20 35.3333 20.2985 35.3333 20.6667V21.724L33.8047 20.1953C33.6797 20.0703 33.5102 20 33.3333 20H25.3333C25.3312 17.7918 23.5416 16.0022 21.3333 16H16V8C16 6.52725 17.1939 5.33333 18.6667 5.33333H36C37.4728 5.33333 38.6667 6.52725 38.6667 8V17.3333Z"
                                                              fill="white"/>
                                                        <path d="M7.33203 22.667C6.22745 22.667 5.33203 23.5624 5.33203 24.667C5.33203 25.7716 6.22745 26.667 7.33203 26.667C8.43661 26.667 9.33203 25.7716 9.33203 24.667C9.33203 23.5624 8.43661 22.667 7.33203 22.667ZM7.33203 25.3337C6.96386 25.3337 6.66536 25.0352 6.66536 24.667C6.66536 24.2988 6.96386 24.0003 7.33203 24.0003C7.7002 24.0003 7.9987 24.2988 7.9987 24.667C7.9987 25.0352 7.7002 25.3337 7.33203 25.3337Z"
                                                              fill="white"/>
                                                        <path d="M12.668 22.667C11.5634 22.667 10.668 23.5624 10.668 24.667C10.668 25.7716 11.5634 26.667 12.668 26.667C13.7726 26.667 14.668 25.7716 14.668 24.667C14.668 23.5624 13.7726 22.667 12.668 22.667ZM12.668 25.3337C12.2998 25.3337 12.0013 25.0352 12.0013 24.667C12.0013 24.2988 12.2998 24.0003 12.668 24.0003C13.0361 24.0003 13.3346 24.2988 13.3346 24.667C13.3346 25.0352 13.0361 25.3337 12.668 25.3337Z"
                                                              fill="white"/>
                                                        <path d="M18 22.667C16.8954 22.667 16 23.5624 16 24.667C16 25.7716 16.8954 26.667 18 26.667C19.1046 26.667 20 25.7716 20 24.667C20 23.5624 19.1046 22.667 18 22.667ZM18 25.3337C17.6318 25.3337 17.3333 25.0352 17.3333 24.667C17.3333 24.2988 17.6318 24.0003 18 24.0003C18.3682 24.0003 18.6667 24.2988 18.6667 24.667C18.6667 25.0352 18.3682 25.3337 18 25.3337Z"
                                                              fill="white"/>
                                                        <path d="M30.668 8H18.668V9.33334H30.668V8Z" fill="white"/>
                                                        <path d="M36 8H32V9.33334H36V8Z" fill="white"/>
                                                        <path d="M22.668 10.667H18.668V12.0003H22.668V10.667Z"
                                                              fill="white"/>
                                                        <path d="M32 10.667H24V12.0003H32V10.667Z" fill="white"/>
                                                        <path d="M36.0013 13.333H18.668V14.6663H36.0013V13.333Z"
                                                              fill="white"/>
                                                        <path d="M30.6654 16H25.332V17.3333H30.6654V16Z"
                                                              fill="white"/>
                                                        <path d="M36 16H32V17.3333H36V16Z" fill="white"/>
                                                        <path d="M35.9987 10.667H33.332V12.0003H35.9987V10.667Z"
                                                              fill="white"/>
                                                    </svg>
                                                    Обсудить
                                                    <span class="absolute">
                                                        <span class="statistics green">` + pro_count + `</span>
                                                            <span class="statistics red">` + contra_count + `</span>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="stick"></div>
                                        `);
                                        }
                                    }
                                })

                                $('.tab_buttons button:first-child').trigger('click');
                                $('.principles .grid a').removeClass('loading');


                            }
                        });

                        $('.active_institute_id').val($(this).attr('id'));
                    }
                });

                $('body').on('click', '.compare_btn, .add_compare_link', function () {

                    if(!$(this).hasClass('loading')){
                        $('.compare_btn').addClass('loading');
                        comp_box_id = +$('#total_comp_boxes').val();
                        $.ajax({
                            url: "/get_compare_box_institute",
                            type: "post",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "jurisdiction_id": $(this).data('id'),
                                "institute_id": $('.active_institute_id').val(),
                                "type": $(this).data('type'),
                                'comp_box_id': comp_box_id,
                            },
                            success: function (response) {
                                $('#comp_box_' + comp_box_id).html(response);
                                comp_box_id += 1;
                                $('#total_comp_boxes').val(comp_box_id);
                                $.ajax({
                                    url: "/get_compare_box_new",
                                    type: "post",
                                    data: {
                                        "_token": "{{ csrf_token() }}",
                                        "institute_id": $('.active_institute_id').val(),
                                        "box_id": comp_box_id,
                                    },
                                    success: function (response) {
                                        $('.comparison .d-flex').append(response);
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        console.log(textStatus, errorThrown);
                                    }
                                });
                                $('.comparison').show();
                                $('.compare_btn').removeClass('loading');
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log(textStatus, errorThrown);
                            }
                        });
                    }
                });


                $(".scroller").scroll(function(){
                    $(".dragscroll")
                        .scrollLeft($(".scroller").scrollLeft());
                });
                $(".dragscroll").scroll(function(){
                    $(".scroller")
                        .scrollLeft($(".dragscroll").scrollLeft());
                });
                var html = `<div style='height: 6px;width:` + ($('.dragscroll')[0].scrollWidth) + `px'></div>`;
                $('.scroller').append(html);
            });
            

            
        </script>
@endsection

@section('modals')
    <div class="modal fade bd-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding: 40px !important">
                    <h2>Спасибо!</h2>
                    <br>
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{!! \Session::get('success') !!}</li>
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="doctrine_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel2" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding: 40px !important">
                    <h2 id="title_doctrine_modal">Доктрина</h2>
                    <br>
                    <div id="text_doctrine_modal"></div>
                </div>
            </div>
        </div>
@endsection
