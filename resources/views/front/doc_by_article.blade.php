@extends('front.layouts.page_layout')
@section('content')
    <div class="loader">
        <img src="{{asset('front/assets/images/spinner.svg')}}">
    </div>
    <!-- start page-title -->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col col-xs-12">
                    <h3>{{$now_npa[0]->pod_name}}</h3>
                    <h2>{{$now_npa[0]->name}}</h2>
                </div>
            </div> <!-- end row -->
        </div> <!-- end container -->
    </section>
    <!-- end page-title -->

    <!-- start top_buttons -->
    <section class="top_buttons type_3">
        <input type="hidden" value="{{$now_article[0]->id}}" class="active_article">
        <div class="container">
            <div class="dragscroll">
                <div class="topButtons_slider">
                    @foreach($all_articles as $article)
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <a id="{{$article->id}}" href="javascript:void(0);" class="item @if($now_article[0]->id == $article->id) active @endif">
                            <div class="num">Статья {{$article->number}}</div>
                            <h2>{{$article->name}}</h2>
                            <p>
                                @if($article->type == "deist")
                                    Действующая
                                @else
                                    Предлагаемая
                                @endif
                            </p>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- end top_buttons -->

    <section class="articles">
        <div class="container">
            <div class="row">
                <p id="hide_show_comparing_articles"><a href="javascript:void(0);">Скрыть сравнение статей</a></p>
                <div style="clear: both;"></div>
                <hr>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 first_column">
                    <div class="scroll_box">
                        <div class="item">
                            <h3>Редакция законодательного акта</h3>
                            <div id="red_zad_act">{!!$now_article[0]->red_1!!}</div>
                            <div class="text_counter" id="red_zad_act_tecinfo">
                                <hr>
                                <span id="symb_count">Количество символов - <b>{{$now_article[0]->char_count('red_1')}}</b></span><br>
                                <span id="word_count">Количество слов - <b>{{$now_article[0]->word_count('red_1')}}</b></span>
                            </div>
                            <div class="btns">
                                <a style="cursor: pointer;" onClick="load_discussion('red1');" data-tab-open="discussion" class="discuss_btn">
                                    <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M36 4H18.6667C16.4584 4.00217 14.6688 5.79175 14.6667 8V16H4C1.79175 16.0022 0.00216667 17.7918 0 20V29.3333C0.00241667 31.2844 1.41 32.9501 3.33333 33.278V35.3333C3.33342 35.7015 3.63192 35.9999 4.00017 35.9998C4.17692 35.9998 4.34633 35.9296 4.47133 35.8047L6.94267 33.3333H21.3333C23.5416 33.3312 25.3312 31.5416 25.3333 29.3333V21.3333H33.0573L35.5287 23.8047C35.7891 24.065 36.2112 24.0649 36.4715 23.8045C36.5964 23.6795 36.6667 23.51 36.6667 23.3333V21.278C38.59 20.9501 39.9976 19.2844 40 17.3333V8C39.9978 5.79175 38.2083 4.00217 36 4ZM24 29.3333C24 30.8061 22.8061 32 21.3333 32H6.66667C6.48983 32 6.32033 32.0703 6.19533 32.1953L4.66667 33.724V32.6667C4.66667 32.2985 4.36817 32 4 32C2.52725 32 1.33333 30.8061 1.33333 29.3333V20C1.33333 18.5273 2.52725 17.3333 4 17.3333H21.3333C22.8061 17.3333 24 18.5273 24 20V29.3333ZM38.6667 17.3333C38.6667 18.8061 37.4728 20 36 20C35.6318 20 35.3333 20.2985 35.3333 20.6667V21.724L33.8047 20.1953C33.6797 20.0703 33.5102 20 33.3333 20H25.3333C25.3312 17.7918 23.5416 16.0022 21.3333 16H16V8C16 6.52725 17.1939 5.33333 18.6667 5.33333H36C37.4728 5.33333 38.6667 6.52725 38.6667 8V17.3333Z" fill="white"/>
                                        <path d="M7.33203 22.667C6.22745 22.667 5.33203 23.5624 5.33203 24.667C5.33203 25.7716 6.22745 26.667 7.33203 26.667C8.43661 26.667 9.33203 25.7716 9.33203 24.667C9.33203 23.5624 8.43661 22.667 7.33203 22.667ZM7.33203 25.3337C6.96386 25.3337 6.66536 25.0352 6.66536 24.667C6.66536 24.2988 6.96386 24.0003 7.33203 24.0003C7.7002 24.0003 7.9987 24.2988 7.9987 24.667C7.9987 25.0352 7.7002 25.3337 7.33203 25.3337Z" fill="white"/>
                                        <path d="M12.668 22.667C11.5634 22.667 10.668 23.5624 10.668 24.667C10.668 25.7716 11.5634 26.667 12.668 26.667C13.7726 26.667 14.668 25.7716 14.668 24.667C14.668 23.5624 13.7726 22.667 12.668 22.667ZM12.668 25.3337C12.2998 25.3337 12.0013 25.0352 12.0013 24.667C12.0013 24.2988 12.2998 24.0003 12.668 24.0003C13.0361 24.0003 13.3346 24.2988 13.3346 24.667C13.3346 25.0352 13.0361 25.3337 12.668 25.3337Z" fill="white"/>
                                        <path d="M18 22.667C16.8954 22.667 16 23.5624 16 24.667C16 25.7716 16.8954 26.667 18 26.667C19.1046 26.667 20 25.7716 20 24.667C20 23.5624 19.1046 22.667 18 22.667ZM18 25.3337C17.6318 25.3337 17.3333 25.0352 17.3333 24.667C17.3333 24.2988 17.6318 24.0003 18 24.0003C18.3682 24.0003 18.6667 24.2988 18.6667 24.667C18.6667 25.0352 18.3682 25.3337 18 25.3337Z" fill="white"/>
                                        <path d="M30.668 8H18.668V9.33334H30.668V8Z" fill="white"/>
                                        <path d="M36 8H32V9.33334H36V8Z" fill="white"/>
                                        <path d="M22.668 10.667H18.668V12.0003H22.668V10.667Z" fill="white"/>
                                        <path d="M32 10.667H24V12.0003H32V10.667Z" fill="white"/>
                                        <path d="M36.0013 13.333H18.668V14.6663H36.0013V13.333Z" fill="white"/>
                                        <path d="M30.6654 16H25.332V17.3333H30.6654V16Z" fill="white"/>
                                        <path d="M36 16H32V17.3333H36V16Z" fill="white"/>
                                        <path d="M35.9987 10.667H33.332V12.0003H35.9987V10.667Z" fill="white"/>
                                    </svg>
                                    Обсудить
                                    <span class="absolute">
                                                    <span class="statistics green">{{$now_article[0]->pro_positions_count($now_article[0]->id, 'red1')}}</span>
                                                    <span class="statistics red">{{$now_article[0]->contra_positions_count($now_article[0]->id, 'red1')}}</span>
                                                </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 second_column">
                    <div class="scroll_box">
                        <div class="item">
                            <h3>Редакция проекта</h3>
                            <div id="red_project">{!!$diff_red_2!!}</div>
                             <div class="text_counter" id="red_project_tecinfo">
                                <hr>
                                <span class="percentage">Процент изменения текста - <b>{{$percentage}} %</b></span><br>
                                <span id="symb_count">Количество символов - <b>{{$now_article[0]->char_count('red_2')}}</b></span><br>
                                <span id="word_count">Количество слов - <b>{{$now_article[0]->word_count('red_2')}}</b></span>
                            </div>
                            <div class="btns">
                                <a style="cursor: pointer;" onClick="load_discussion('red2');" data-tab-open="discussion" class="discuss_btn">
                                    <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M36 4H18.6667C16.4584 4.00217 14.6688 5.79175 14.6667 8V16H4C1.79175 16.0022 0.00216667 17.7918 0 20V29.3333C0.00241667 31.2844 1.41 32.9501 3.33333 33.278V35.3333C3.33342 35.7015 3.63192 35.9999 4.00017 35.9998C4.17692 35.9998 4.34633 35.9296 4.47133 35.8047L6.94267 33.3333H21.3333C23.5416 33.3312 25.3312 31.5416 25.3333 29.3333V21.3333H33.0573L35.5287 23.8047C35.7891 24.065 36.2112 24.0649 36.4715 23.8045C36.5964 23.6795 36.6667 23.51 36.6667 23.3333V21.278C38.59 20.9501 39.9976 19.2844 40 17.3333V8C39.9978 5.79175 38.2083 4.00217 36 4ZM24 29.3333C24 30.8061 22.8061 32 21.3333 32H6.66667C6.48983 32 6.32033 32.0703 6.19533 32.1953L4.66667 33.724V32.6667C4.66667 32.2985 4.36817 32 4 32C2.52725 32 1.33333 30.8061 1.33333 29.3333V20C1.33333 18.5273 2.52725 17.3333 4 17.3333H21.3333C22.8061 17.3333 24 18.5273 24 20V29.3333ZM38.6667 17.3333C38.6667 18.8061 37.4728 20 36 20C35.6318 20 35.3333 20.2985 35.3333 20.6667V21.724L33.8047 20.1953C33.6797 20.0703 33.5102 20 33.3333 20H25.3333C25.3312 17.7918 23.5416 16.0022 21.3333 16H16V8C16 6.52725 17.1939 5.33333 18.6667 5.33333H36C37.4728 5.33333 38.6667 6.52725 38.6667 8V17.3333Z" fill="white"/>
                                        <path d="M7.33203 22.667C6.22745 22.667 5.33203 23.5624 5.33203 24.667C5.33203 25.7716 6.22745 26.667 7.33203 26.667C8.43661 26.667 9.33203 25.7716 9.33203 24.667C9.33203 23.5624 8.43661 22.667 7.33203 22.667ZM7.33203 25.3337C6.96386 25.3337 6.66536 25.0352 6.66536 24.667C6.66536 24.2988 6.96386 24.0003 7.33203 24.0003C7.7002 24.0003 7.9987 24.2988 7.9987 24.667C7.9987 25.0352 7.7002 25.3337 7.33203 25.3337Z" fill="white"/>
                                        <path d="M12.668 22.667C11.5634 22.667 10.668 23.5624 10.668 24.667C10.668 25.7716 11.5634 26.667 12.668 26.667C13.7726 26.667 14.668 25.7716 14.668 24.667C14.668 23.5624 13.7726 22.667 12.668 22.667ZM12.668 25.3337C12.2998 25.3337 12.0013 25.0352 12.0013 24.667C12.0013 24.2988 12.2998 24.0003 12.668 24.0003C13.0361 24.0003 13.3346 24.2988 13.3346 24.667C13.3346 25.0352 13.0361 25.3337 12.668 25.3337Z" fill="white"/>
                                        <path d="M18 22.667C16.8954 22.667 16 23.5624 16 24.667C16 25.7716 16.8954 26.667 18 26.667C19.1046 26.667 20 25.7716 20 24.667C20 23.5624 19.1046 22.667 18 22.667ZM18 25.3337C17.6318 25.3337 17.3333 25.0352 17.3333 24.667C17.3333 24.2988 17.6318 24.0003 18 24.0003C18.3682 24.0003 18.6667 24.2988 18.6667 24.667C18.6667 25.0352 18.3682 25.3337 18 25.3337Z" fill="white"/>
                                        <path d="M30.668 8H18.668V9.33334H30.668V8Z" fill="white"/>
                                        <path d="M36 8H32V9.33334H36V8Z" fill="white"/>
                                        <path d="M22.668 10.667H18.668V12.0003H22.668V10.667Z" fill="white"/>
                                        <path d="M32 10.667H24V12.0003H32V10.667Z" fill="white"/>
                                        <path d="M36.0013 13.333H18.668V14.6663H36.0013V13.333Z" fill="white"/>
                                        <path d="M30.6654 16H25.332V17.3333H30.6654V16Z" fill="white"/>
                                        <path d="M36 16H32V17.3333H36V16Z" fill="white"/>
                                        <path d="M35.9987 10.667H33.332V12.0003H35.9987V10.667Z" fill="white"/>
                                    </svg>
                                    Обсудить
                                    <span class="absolute">
                                        <span class="statistics green">{{$now_article[0]->pro_positions_count($now_article[0]->id, 'red2')}}</span>
                                        <span class="statistics red">{{$now_article[0]->contra_positions_count($now_article[0]->id, 'red2')}}</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-lg-4 third_column">
                    <div class="scroll_box">
                        <div class="item">
                            <h3>Редакции предлагаемых изменений/дополнений</h3>
                            <div class="column">
                                @foreach ($suggestions_to_print as $suggestion)
                                    <div id="sug_{{$suggestion['id']}}">{!!$suggestion['suggestion']!!}</div>
                                    <div class="text_counter" id="sug_{{$suggestion['id']}}_tecinfo">
                                        <hr>
                                        <span>Процент изменения текста - <b>{{$suggestion['percentage']}} %</b></span><br>
                                        <span id="symb_count">Количество символов - <b>{{$suggestion['char_count']}}</b></span><br>
                                        <span id="word_count">Количество слов - <b>{{$suggestion['word_count']}}</b></span>
                                    </div>
                                    <div class="btns">
                                        <a style="cursor: pointer;" onClick="load_discussion_suggestions('{{$suggestion['id']}}');" data-tab-open="discussion" class="discuss_btn">
                                            <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M36 4H18.6667C16.4584 4.00217 14.6688 5.79175 14.6667 8V16H4C1.79175 16.0022 0.00216667 17.7918 0 20V29.3333C0.00241667 31.2844 1.41 32.9501 3.33333 33.278V35.3333C3.33342 35.7015 3.63192 35.9999 4.00017 35.9998C4.17692 35.9998 4.34633 35.9296 4.47133 35.8047L6.94267 33.3333H21.3333C23.5416 33.3312 25.3312 31.5416 25.3333 29.3333V21.3333H33.0573L35.5287 23.8047C35.7891 24.065 36.2112 24.0649 36.4715 23.8045C36.5964 23.6795 36.6667 23.51 36.6667 23.3333V21.278C38.59 20.9501 39.9976 19.2844 40 17.3333V8C39.9978 5.79175 38.2083 4.00217 36 4ZM24 29.3333C24 30.8061 22.8061 32 21.3333 32H6.66667C6.48983 32 6.32033 32.0703 6.19533 32.1953L4.66667 33.724V32.6667C4.66667 32.2985 4.36817 32 4 32C2.52725 32 1.33333 30.8061 1.33333 29.3333V20C1.33333 18.5273 2.52725 17.3333 4 17.3333H21.3333C22.8061 17.3333 24 18.5273 24 20V29.3333ZM38.6667 17.3333C38.6667 18.8061 37.4728 20 36 20C35.6318 20 35.3333 20.2985 35.3333 20.6667V21.724L33.8047 20.1953C33.6797 20.0703 33.5102 20 33.3333 20H25.3333C25.3312 17.7918 23.5416 16.0022 21.3333 16H16V8C16 6.52725 17.1939 5.33333 18.6667 5.33333H36C37.4728 5.33333 38.6667 6.52725 38.6667 8V17.3333Z" fill="white"/>
                                                <path d="M7.33203 22.667C6.22745 22.667 5.33203 23.5624 5.33203 24.667C5.33203 25.7716 6.22745 26.667 7.33203 26.667C8.43661 26.667 9.33203 25.7716 9.33203 24.667C9.33203 23.5624 8.43661 22.667 7.33203 22.667ZM7.33203 25.3337C6.96386 25.3337 6.66536 25.0352 6.66536 24.667C6.66536 24.2988 6.96386 24.0003 7.33203 24.0003C7.7002 24.0003 7.9987 24.2988 7.9987 24.667C7.9987 25.0352 7.7002 25.3337 7.33203 25.3337Z" fill="white"/>
                                                <path d="M12.668 22.667C11.5634 22.667 10.668 23.5624 10.668 24.667C10.668 25.7716 11.5634 26.667 12.668 26.667C13.7726 26.667 14.668 25.7716 14.668 24.667C14.668 23.5624 13.7726 22.667 12.668 22.667ZM12.668 25.3337C12.2998 25.3337 12.0013 25.0352 12.0013 24.667C12.0013 24.2988 12.2998 24.0003 12.668 24.0003C13.0361 24.0003 13.3346 24.2988 13.3346 24.667C13.3346 25.0352 13.0361 25.3337 12.668 25.3337Z" fill="white"/>
                                                <path d="M18 22.667C16.8954 22.667 16 23.5624 16 24.667C16 25.7716 16.8954 26.667 18 26.667C19.1046 26.667 20 25.7716 20 24.667C20 23.5624 19.1046 22.667 18 22.667ZM18 25.3337C17.6318 25.3337 17.3333 25.0352 17.3333 24.667C17.3333 24.2988 17.6318 24.0003 18 24.0003C18.3682 24.0003 18.6667 24.2988 18.6667 24.667C18.6667 25.0352 18.3682 25.3337 18 25.3337Z" fill="white"/>
                                                <path d="M30.668 8H18.668V9.33334H30.668V8Z" fill="white"/>
                                                <path d="M36 8H32V9.33334H36V8Z" fill="white"/>
                                                <path d="M22.668 10.667H18.668V12.0003H22.668V10.667Z" fill="white"/>
                                                <path d="M32 10.667H24V12.0003H32V10.667Z" fill="white"/>
                                                <path d="M36.0013 13.333H18.668V14.6663H36.0013V13.333Z" fill="white"/>
                                                <path d="M30.6654 16H25.332V17.3333H30.6654V16Z" fill="white"/>
                                                <path d="M36 16H32V17.3333H36V16Z" fill="white"/>
                                                <path d="M35.9987 10.667H33.332V12.0003H35.9987V10.667Z" fill="white"/>
                                            </svg>
                                            Обсудить
                                            <span class="absolute">
                                            <span class="statistics green">{{$suggestion['pro_positions_count']}}</span>
                                            <span class="statistics red">{{$suggestion['contra_positions_count']}}</span>
                                        </span>
                                        </a>
                                    </div>
                                    <div class="stick"></div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="discussion" class="discussion" data-tab="discussion">

    </section>
@endsection


@section('js')

    <script src="/front/assets/js/htmldiff.js"></script>
    <style>
        ins {
            text-decoration: none;
            background-color: #d4fcbc;
        }

        del {
            text-decoration: line-through;
            background-color: #fbb6c2;
            color: #555;
        }
    </style>
    <script type="text/javascript">
        @if(session()->has('success'))
        $(document).ready(function(){
            $("#bd-example-modal-lg").modal("show");
        });
        @endif

        $('#hide_show_comparing_articles').click(function(){
            $(this).toggleClass('active');
            if($(this).hasClass('active')){
                $(this).find('a').text('Сравнить статьи');
                $('.diff-html-removed').hide();
                $('.text_counter').hide();
                $('ins').css('background', 'unset');
            }else{
                $(this).find('a').text('Скрыть сравнение статей');
                $('.diff-html-removed').show();
                $('.text_counter').show();
                $('ins').css('background', '#d4fcbc');
            }
        });
        $('.topButtons_slider .item').click(function(){
            $('.active_article').val($(this).attr('id'));
            $('#hide_show_comparing_articles').removeClass('active').find('a').text('Скрыть сравнение статей');
            $('.topButtons_slider .item').removeClass('active');
            $(this).addClass('active');

            $('.loader').addClass('active');
            $('.discussion').hide();
            $('#red_zad_act').html('');
            $('#red_project').html('');
            $('.third_column .column').html('');

            $.ajax({
                type: 'GET',
                url: '/fetch-article/{{$now_npa[0]->id}}/' + $(this).attr('id'),
                success: function(response){

                    $('.loader').removeClass('active');

                    $('#red_zad_act').html(response.now_article.red_1);
                    $('#red_project').html(response.diff_red_2);

                    var word_count_1 = response.now_article.red_1.split(' ')
                        .filter(function(n) { return n != '' })
                        .length;
                    var char_count_1 = $('#red_zad_act').text().length;

                    var word_count_2 = response.now_article.red_2.split(' ')
                        .filter(function(n) { return n != '' })
                        .length;
                    var char_count_2 = $('#red_project').text().length;

                    /* FIRST COLUMN */
                    $('.first_column #symb_count b').html(char_count_1);
                    $('.first_column #word_count b').html(word_count_1);

                    $('.first_column .statistics.green').text(response.red_1_pro);
                    $('.first_column .statistics.red').text(response.red_1_contra);

                    /* SECOND COLUMN */
                    $('.second_column #symb_count b').html(char_count_2);
                    $('.second_column #word_count b').html(word_count_2);

                    $('.second_column .statistics.green').text(response.red_2_pro);
                    $('.second_column .statistics.red').text(response.red_2_contra);


                    $('.percentage b').text(response.percentage + '%');

                    for(var i = 0;i < response.suggestions_to_print.length; i++){
                        $('.third_column .column').append(`
                            <div id="sug_`+ response.suggestions_to_print[i].id + `">`+ response.suggestions_to_print[i].suggestion + `</div>
                            <div class="text_counter" id="sug_`+ response.suggestions_to_print[i].id + `_tecinfo">
                                <hr>
                                <span>Процент изменения текста - <b>`+ response.suggestions_to_print[i].percentage + ` %</b></span><br>
                                <span id="symb_count">Количество символов - <b>`+ response.suggestions_to_print[i].char_count + `</b></span><br>
                                <span id="word_count">Количество слов - <b>`+ response.suggestions_to_print[i].word_count + `</b></span>
                            </div>
                            <div class="btns">
                                <a style="cursor: pointer;" onClick="load_discussion_suggestions('`+ response.suggestions_to_print[i].id + `');" data-tab-open="discussion" class="discuss_btn">
                                    <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M36 4H18.6667C16.4584 4.00217 14.6688 5.79175 14.6667 8V16H4C1.79175 16.0022 0.00216667 17.7918 0 20V29.3333C0.00241667 31.2844 1.41 32.9501 3.33333 33.278V35.3333C3.33342 35.7015 3.63192 35.9999 4.00017 35.9998C4.17692 35.9998 4.34633 35.9296 4.47133 35.8047L6.94267 33.3333H21.3333C23.5416 33.3312 25.3312 31.5416 25.3333 29.3333V21.3333H33.0573L35.5287 23.8047C35.7891 24.065 36.2112 24.0649 36.4715 23.8045C36.5964 23.6795 36.6667 23.51 36.6667 23.3333V21.278C38.59 20.9501 39.9976 19.2844 40 17.3333V8C39.9978 5.79175 38.2083 4.00217 36 4ZM24 29.3333C24 30.8061 22.8061 32 21.3333 32H6.66667C6.48983 32 6.32033 32.0703 6.19533 32.1953L4.66667 33.724V32.6667C4.66667 32.2985 4.36817 32 4 32C2.52725 32 1.33333 30.8061 1.33333 29.3333V20C1.33333 18.5273 2.52725 17.3333 4 17.3333H21.3333C22.8061 17.3333 24 18.5273 24 20V29.3333ZM38.6667 17.3333C38.6667 18.8061 37.4728 20 36 20C35.6318 20 35.3333 20.2985 35.3333 20.6667V21.724L33.8047 20.1953C33.6797 20.0703 33.5102 20 33.3333 20H25.3333C25.3312 17.7918 23.5416 16.0022 21.3333 16H16V8C16 6.52725 17.1939 5.33333 18.6667 5.33333H36C37.4728 5.33333 38.6667 6.52725 38.6667 8V17.3333Z" fill="white"/>
                                        <path d="M7.33203 22.667C6.22745 22.667 5.33203 23.5624 5.33203 24.667C5.33203 25.7716 6.22745 26.667 7.33203 26.667C8.43661 26.667 9.33203 25.7716 9.33203 24.667C9.33203 23.5624 8.43661 22.667 7.33203 22.667ZM7.33203 25.3337C6.96386 25.3337 6.66536 25.0352 6.66536 24.667C6.66536 24.2988 6.96386 24.0003 7.33203 24.0003C7.7002 24.0003 7.9987 24.2988 7.9987 24.667C7.9987 25.0352 7.7002 25.3337 7.33203 25.3337Z" fill="white"/>
                                        <path d="M12.668 22.667C11.5634 22.667 10.668 23.5624 10.668 24.667C10.668 25.7716 11.5634 26.667 12.668 26.667C13.7726 26.667 14.668 25.7716 14.668 24.667C14.668 23.5624 13.7726 22.667 12.668 22.667ZM12.668 25.3337C12.2998 25.3337 12.0013 25.0352 12.0013 24.667C12.0013 24.2988 12.2998 24.0003 12.668 24.0003C13.0361 24.0003 13.3346 24.2988 13.3346 24.667C13.3346 25.0352 13.0361 25.3337 12.668 25.3337Z" fill="white"/>
                                        <path d="M18 22.667C16.8954 22.667 16 23.5624 16 24.667C16 25.7716 16.8954 26.667 18 26.667C19.1046 26.667 20 25.7716 20 24.667C20 23.5624 19.1046 22.667 18 22.667ZM18 25.3337C17.6318 25.3337 17.3333 25.0352 17.3333 24.667C17.3333 24.2988 17.6318 24.0003 18 24.0003C18.3682 24.0003 18.6667 24.2988 18.6667 24.667C18.6667 25.0352 18.3682 25.3337 18 25.3337Z" fill="white"/>
                                        <path d="M30.668 8H18.668V9.33334H30.668V8Z" fill="white"/>
                                        <path d="M36 8H32V9.33334H36V8Z" fill="white"/>
                                        <path d="M22.668 10.667H18.668V12.0003H22.668V10.667Z" fill="white"/>
                                        <path d="M32 10.667H24V12.0003H32V10.667Z" fill="white"/>
                                        <path d="M36.0013 13.333H18.668V14.6663H36.0013V13.333Z" fill="white"/>
                                        <path d="M30.6654 16H25.332V17.3333H30.6654V16Z" fill="white"/>
                                        <path d="M36 16H32V17.3333H36V16Z" fill="white"/>
                                        <path d="M35.9987 10.667H33.332V12.0003H35.9987V10.667Z" fill="white"/>
                                    </svg>
                                    Обсудить
                                    <span class="absolute">
                                    <span class="statistics green">`+ response.suggestions_to_print[i].pro_positions_count + `</span>
                                    <span class="statistics red">`+ response.suggestions_to_print[i].contra_positions_count + `</span>
                                </span>
                                </a>
                            </div>
                            <div class="stick"></div>
                        `);
                    }


                }

            })
        });
            function count_technical_info(div_id){
                $(document).ready(function(){
                    var txt = $('#'+div_id).text(), charCount = txt.length, wordCount =txt.replace( /[^\w ]/g, "" ).split( /\s+/ ).length;
                    $("#"+div_id+"tecinfo, #symb_count").html("Количество символов - <b>"+charCount+"</b><br>");
                    $("#"+div_id+"tecinfo, #word_count").html("Количество слов - <b>"+wordCount+"</b>");
                });
            }

            function load_discussion_suggestions(suggestion_id){
                $.ajax({
                    url: "/get_discussion_suggestion",
                    type: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "suggestion_id": suggestion_id,
                    },
                    success: function (response) {
                        $("#discussion").show();
                        $("#discussion").html(response);
                        var allEditors = document.querySelectorAll('.ckeditor');
                        for (var i = 0; i < allEditors.length; ++i) {
                            ClassicEditor.create(
                                allEditors[i], {
                                }
                            );
                        }
                        $('.tab_buttons button, .btns a').on('click', function () {
                        $(this).addClass('active').siblings('button').removeClass('active');
                        $('.tab_block[data-tab=' + $(this).data("tab-open") + ']').addClass('active').siblings('.tab_block').removeClass('active');
                        });
                        $([document.documentElement, document.body]).animate({
                            scrollTop: $("#discussion_block_all_positions").offset().top
                        }, 500);
                        $("#select_pro_contra").prop('disabled', false);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }

                });
            }

            function load_contra_positions_suggestions(position_id, pro_contra){
                $.ajax({
                    url: "/get_contra_positions_suggestions",
                    type: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "position_id": position_id,
                        "pro_contra": pro_contra,
                        "type": 'suggestion',
                    },
                    success: function (response) {
                        $("#positions_to_ajax_load").html(response);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#positions_to_ajax_load").offset().top
                }, 1000);
                $("#position_add_form").css('display', 'none');
            }
            function load_discussion(type){
                $.ajax({
                    url: "/get_discussion_by_article",
                    type: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "rule_id": $('.active_article').val(),
                        "type": type,
                    },
                    success: function (response) {
                        $("#discussion").show();
                        $("#discussion").html(response);
                        var allEditors = document.querySelectorAll('.ckeditor');
                        for (var i = 0; i < allEditors.length; ++i) {
                            ClassicEditor.create(
                                allEditors[i], {
                                }
                            );
                        }
                        $('.tab_buttons button, .btns a').on('click', function () {
                        $(this).addClass('active').siblings('button').removeClass('active');
                        $('.tab_block[data-tab=' + $(this).data("tab-open") + ']').addClass('active').siblings('.tab_block').removeClass('active');
                        });


                        $([document.documentElement, document.body]).animate({
                            scrollTop: $("#discussion_block_all_positions").offset().top
                        }, 500);
                        $("#select_pro_contra").prop('disabled', false);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }

                });
            }
            function load_contra_positions(position_id, pro_contra, type){
                $.ajax({
                    url: "/get_contra_positions",
                    type: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "position_id": position_id,
                        "pro_contra": pro_contra,
                        "type": type,
                    },
                    success: function (response) {
                        $("#positions_to_ajax_load").html(response);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#positions_to_ajax_load").offset().top
                }, 1000);
                $("#position_add_form").css('display', 'none');
            }
            function scroll_to_form(pro_contra_selected, name_surname, vs_position_id){
                if(pro_contra_selected == "pro"){
                    $("#select_pro_contra").val('pro');
                    $("#select_pro_contra").prop('disabled', 'disabled');
                    $("#text_pro_contra").val('pro');
                }else{
                    $("#select_pro_contra").val('contra');
                    $("#select_pro_contra").prop('disabled', 'disabled');
                    $("#text_pro_contra").val('contra');
                }
                $("#vs_position_id").val(vs_position_id);
                $("#versus_whom").text("против позиции автора - "+name_surname);
                $("#position_add_form").css('display', 'block');
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#position_add_form").offset().top
                }, 800);
            }
            function scroll_to_form_2(pro_contra) {
                if(pro_contra == "contra"){
                    $("#select_pro_contra").val("contra");
                }else{
                    $("#select_pro_contra").val("pro");
                }
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#position_add_form").offset().top
                }, 800);
            }

            function load_doctrine (position_id){

                $.ajax({
                    url: "/get_doctrine",
                    type: "post",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "position_id": position_id,
                    },
                    success: function (response) {
                        $("#doctrine_modal #text_doctrine_modal").html(response);
                        $("#doctrine_modal").modal("show");
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });

            }

        $('body').on('click', '.add_argument_btn', function () {

            $('.add_argument .head').append(`<div class="theargument">
                            <button class="close">
                            <svg viewBox="0 0 329.26933 329" xmlns="http://www.w3.org/2000/svg"><path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg>
                            </button>
                            <div class="name">Аргумент <span class="index"></span></div>
                            <textarea id="argument_for_position"></textarea>
                            </div>`);

            $(this).parents('form').find('.theargument').each(function (index) {
                index++;

                $(this).find('.index').text(index);
                $(this).find("#argument_for_position").attr('name', 'argument_' + index);
                $("#total_arguments").val(index);
            });
        });

        $('body').on('click', '.theargument .close', function () {
            index = 0;
            $(this).parent('.theargument').remove();
            $('.theargument').each(function(){
                index++;
                $(this).find('.index').text(index);
                $(this).find("#argument_for_position").attr('name', 'argument_'+index);
                $("#total_arguments").val(index);
            });
        });

    </script>
@endsection

@section('modals')
    <div class="modal fade bd-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding: 40px !important">
                    <h2>Спасибо!</h2>
                    <br>
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{!! \Session::get('success') !!}</li>
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-lg" id="doctrine_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel2" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding: 40px !important">
                    <h2 id="title_doctrine_modal">Доктрина</h2>
                    <br>
                    <div id="text_doctrine_modal"></div>
                </div>
            </div>
        </div>
@endsection
