@extends('front.layouts.page_layout')
@section('content')


           <!-- start page-title -->
        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <h2>{{ $user_data->surname }} {{ $user_data->name }} {{ $user_data->last_name }}</h2>
                        <!--<p>Личный кабинет</p>!-->
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <button class="theme-btn" type="submit">Выйти</button>
                        </form>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end page-title -->

<!-- start team-sigle-section -->
        <section class="team-sigle-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-4">
                        <div class="team-single-sidebar">
                            <div class="widget attorney-widget">
                                <h3>Меню</h3>
                                <ul>
                                    <li><a href="/cabinet">Мои данные</a></li>
                                    <li class="current"><a href="/cabinet/positions">Мои позиции</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col col-md-8">
                        <div class="attorney-single-content">
                            <div class="attorney">
                                    <table class="table table-striped">
                                    @foreach($positions as $position)
                                        <tr>
                                             <td style="padding: 20px;" class="position_{{$position['id']}}">
                                                @if($position->approved == "moder")
                                                    <div class="alert alert-warning">
                                                        На модерации
                                                    </div>
                                                @else
                                                    <div class="alert alert-success">
                                                        Опубликовано
                                                    </div>
                                                @endif
                                                @if(isset($position->author->photo))
                                                    <img id="discussion_avatar" src="{{$position->author->photo['url']}}"> &nbsp;&nbsp;&nbsp;
                                                @else
                                                    <img id="discussion_avatar" src="/front/assets/images/avatar_none.png"> &nbsp;&nbsp;&nbsp;
                                                @endif

                                                <text style="font-size: 1rem;font-weight: 400;margin: 0 0 0.2em;color: #c9b38c;">{{$position->author->surname}}
                                                {{$position->author->name}}
                                                {{$position->author->last_name}}</text>
                                                <br><br>
                                                <div class="position_text">{!!$position['position']!!}</div>
                                                <br>
                                                <hr>
                                                <b>Доктрина по позиции:</b>
                                                <div class="doctrine_text">{!!$position['doctrine_text']!!}</div>
                                                <hr>
                                                <br>
                                                <?php $arg_num = 1; ?>
                                                @foreach ($position->positionArguments as $argument)
                                                    <div class="argument_item">
                                                        <img src="/front/assets/images/tick.jpg" width=20>
                                                        <p><b>Аргумент {{$arg_num}}: </b>
                                                            <p class="main_text">{!!$argument['argument']!!}</p>
                                                        </p>
                                                        <?php $arg_num++; ?>
                                                    </div>
                                                    <br>

                                                @endforeach
                                                <br>
                                                <button class="theme-btn edit_btn" data-pro_contra="{{$position->pro_contra}}" id="{{$position->id}}" data-toggle="modal" data-target="#edit_modal" type="button">Изменить</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end team-sigle-section -->



@endsection

@section('js')
<script type="text/javascript">
    function load_doctrine (position_id){
        $('.doctrine_content').load("/load_doctrine/"+position_id, function(){
            $('#doctrine').modal({show:true});
        });
    }
</script>
@endsection

@section('modals')
<div class="modal fade doctrine" id="doctrine" tabindex="-1" role="dialog" aria-labelledby="doctrine" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding: 40px !important">
                    <h2></h2>
                    <p class="doctrine_content"></p>
            </div>
        </div>
</div>
<div class="modal fade edit_modal" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Редактировать позицию</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body tab_content modal_tab_content">
                <form action="/edit_position" method="POST" class="state_position" style="margin-top: 0;">
                        <div class="block_title">Изложить позицию <span id="versus_whom"></span></div>
                        @csrf
                        <input type="hidden" name="id" value="">
                        <select class="nice-select" name="pro_contra" id="select_pro_contra">
                            <option value="pro">Pro</option>
                            <option value="contra">Contra</option>
                        </select>
                        <div class="textarea">
                            <label for="position_text">Текст позиции:</label>
                            <div class="position_block">

                            </div>
                        </div>
                        <div class="textarea">
                            <label for="doctrinal_sources">Доктринальные источники:</label>
                            <div class="doctrine_block">

                            </div>
                        </div>
                        <input type="hidden" id="total_arguments" name="total_arguments" value=1>
                        <div class="add_argument">
                            <div class="head">
                            </div>
                            <div class="body">
                                <button type="button" class="add_argument_btn">
                                    <svg width="40" height="40" viewBox="0 0 40 40" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M20 8.75C20.3315 8.75 20.6495 8.8817 20.8839 9.11612C21.1183 9.35054 21.25 9.66848 21.25 10V20C21.25 20.3315 21.1183 20.6495 20.8839 20.8839C20.6495 21.1183 20.3315 21.25 20 21.25H10C9.66848 21.25 9.35054 21.1183 9.11612 20.8839C8.8817 20.6495 8.75 20.3315 8.75 20C8.75 19.6685 8.8817 19.3505 9.11612 19.1161C9.35054 18.8817 9.66848 18.75 10 18.75H18.75V10C18.75 9.66848 18.8817 9.35054 19.1161 9.11612C19.3505 8.8817 19.6685 8.75 20 8.75Z"
                                              fill="#C9B48C"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M18.75 20C18.75 19.6685 18.8817 19.3505 19.1161 19.1161C19.3505 18.8817 19.6685 18.75 20 18.75H30C30.3315 18.75 30.6495 18.8817 30.8839 19.1161C31.1183 19.3505 31.25 19.6685 31.25 20C31.25 20.3315 31.1183 20.6495 30.8839 20.8839C30.6495 21.1183 30.3315 21.25 30 21.25H21.25V30C21.25 30.3315 21.1183 30.6495 20.8839 30.8839C20.6495 31.1183 20.3315 31.25 20 31.25C19.6685 31.25 19.3505 31.1183 19.1161 30.8839C18.8817 30.6495 18.75 30.3315 18.75 30V20Z"
                                              fill="#C9B48C"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M20 37.5C24.6413 37.5 29.0925 35.6563 32.3744 32.3744C35.6563 29.0925 37.5 24.6413 37.5 20C37.5 15.3587 35.6563 10.9075 32.3744 7.62563C29.0925 4.34374 24.6413 2.5 20 2.5C15.3587 2.5 10.9075 4.34374 7.62563 7.62563C4.34374 10.9075 2.5 15.3587 2.5 20C2.5 24.6413 4.34374 29.0925 7.62563 32.3744C10.9075 35.6563 15.3587 37.5 20 37.5ZM20 40C25.3043 40 30.3914 37.8929 34.1421 34.1421C37.8929 30.3914 40 25.3043 40 20C40 14.6957 37.8929 9.60859 34.1421 5.85786C30.3914 2.10714 25.3043 0 20 0C14.6957 0 9.60859 2.10714 5.85786 5.85786C2.10714 9.60859 0 14.6957 0 20C0 25.3043 2.10714 30.3914 5.85786 34.1421C9.60859 37.8929 14.6957 40 20 40Z"
                                              fill="#C9B48C"/>
                                    </svg>
                                    Добавить аргумент
                                </button>
                            </div>
                        </div>
                        <button type="submit">Отправить</button>
                        <div class="required">
                            *при изложении позиции обязательно укажите как минимум один аргумент в ее
                            поддержку
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
@if (\Session::has('success'))
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="padding: 40px !important">
            <h2>Спасибо!</h2>
            <br>
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
        </div>
    </div>
@endif
<script>
    $('.add_argument_btn').click(function(){
       $('body').find('.edit_modal .head textarea').attr('name', 'arguments[]');
    });
    $('.edit_btn').click(function(){
        $('select[name=pro_contra] option[value=' + $(this).data('pro_contra') + ']').prop('selected', true);


        $('.edit_modal input[name=id]').val($(this).attr('id'));
        $('.edit_modal .position_block').html(``);
        $('.edit_modal .doctrine_block').html(``);
        $('.edit_modal .add_argument .head').html(``);

        $(this).parents('tr').find('.argument_item').each(function(index){
            $('.add_argument .head').append(`

                <div class="theargument">
                    <button class="close">
                        <svg viewBox="0 0 329.26933 329" xmlns="http://www.w3.org/2000/svg"><path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg>
                    </button>
                    <div class="name">Аргумент<span class="index">&nbsp;` + (index + 1) + `</span></div>
                    <textarea name="arguments[]">` + $(this).find('.main_text').text() + `</textarea>
                </div>
            `);
        });

        var position_text = `<textarea class="ckeditor" name="position_text">` + $(this).parents('tr').find('.position_text').html() + `</div>`;
        $('.edit_modal .position_block').append(position_text);
        console.log($(this).parents('tr').find('.doctrine_text').html());
        var doctrine_text = `<textarea class="ckeditor" name="doctrine_text">` + $(this).parents('tr').find('.doctrine_text').html() + `</div>`;
        $('.edit_modal .doctrine_block').append(doctrine_text);


        var allEditors = document.querySelectorAll('.ckeditor');
        for (var i = 0; i < allEditors.length; ++i) {
            ClassicEditor.create(
                allEditors[i], {}
            );
        }

        $('.edit_modal input[name=suggestion_id]').val($(this).attr('id'));

    })
</script>
@endsection
