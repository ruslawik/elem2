@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.doctrine.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.doctrines.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="razdel_id">{{ trans('cruds.doctrine.fields.razdel') }}</label>
                <select class="form-control select2 {{ $errors->has('razdel') ? 'is-invalid' : '' }}" name="razdel_id" id="razdel_id" required>
                    @foreach($razdels as $id => $razdel)
                        <option value="{{ $id }}" {{ old('razdel_id') == $id ? 'selected' : '' }}>{{ $razdel }}</option>
                    @endforeach
                </select>
                @if($errors->has('razdel'))
                    <div class="invalid-feedback">
                        {{ $errors->first('razdel') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.doctrine.fields.razdel_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.doctrine.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.doctrine.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="alter_name">{{ trans('cruds.doctrine.fields.alter_name') }}</label>
                <input class="form-control {{ $errors->has('alter_name') ? 'is-invalid' : '' }}" type="text" name="alter_name" id="alter_name" value="{{ old('alter_name', '') }}">
                @if($errors->has('alter_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('alter_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.doctrine.fields.alter_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="goal">{{ trans('cruds.doctrine.fields.goal') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('goal') ? 'is-invalid' : '' }}" name="goal" id="goal">{!! old('goal') !!}</textarea>
                @if($errors->has('goal'))
                    <div class="invalid-feedback">
                        {{ $errors->first('goal') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.doctrine.fields.goal_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="base">{{ trans('cruds.doctrine.fields.base') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('base') ? 'is-invalid' : '' }}" name="base" id="base">{!! old('base') !!}</textarea>
                @if($errors->has('base'))
                    <div class="invalid-feedback">
                        {{ $errors->first('base') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.doctrine.fields.base_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/doctrines/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', {{ $doctrine->id ?? 0 }});
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection