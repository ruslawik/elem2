@extends('layouts.admin')
@section('content')
@can('doctrine_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.doctrines.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.doctrine.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.doctrine.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Doctrine">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.doctrine.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.doctrine.fields.razdel') }}
                        </th>
                        <th>
                            {{ trans('cruds.doctrine.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.doctrine.fields.alter_name') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($doctrines as $key => $doctrine)
                        <tr data-entry-id="{{ $doctrine->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $doctrine->id ?? '' }}
                            </td>
                            <td>
                                {{ $doctrine->razdel->name ?? '' }}
                            </td>
                            <td>
                                {{ $doctrine->name ?? '' }}
                            </td>
                            <td>
                                {{ $doctrine->alter_name ?? '' }}
                            </td>
                            <td>
                                @can('doctrine_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.doctrines.show', $doctrine->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('doctrine_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.doctrines.edit', $doctrine->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('doctrine_delete')
                                    <form action="{{ route('admin.doctrines.destroy', $doctrine->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('doctrine_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.doctrines.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Doctrine:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection