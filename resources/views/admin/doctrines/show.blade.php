@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.doctrine.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.doctrines.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.doctrine.fields.id') }}
                        </th>
                        <td>
                            {{ $doctrine->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.doctrine.fields.razdel') }}
                        </th>
                        <td>
                            {{ $doctrine->razdel->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.doctrine.fields.name') }}
                        </th>
                        <td>
                            {{ $doctrine->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.doctrine.fields.alter_name') }}
                        </th>
                        <td>
                            {{ $doctrine->alter_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.doctrine.fields.goal') }}
                        </th>
                        <td>
                            {!! $doctrine->goal !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.doctrine.fields.base') }}
                        </th>
                        <td>
                            {!! $doctrine->base !!}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.doctrines.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection