@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.discuss.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.discusses.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.discuss.fields.id') }}
                        </th>
                        <td>
                            {{ $discuss->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.discuss.fields.npa') }}
                        </th>
                        <td>
                            {{ $discuss->npa->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.discuss.fields.institute') }}
                        </th>
                        <td>
                            {{ $discuss->institute->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.discuss.fields.jurisdiction') }}
                        </th>
                        <td>
                            {{ $discuss->jurisdiction->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.discuss.fields.post_type') }}
                        </th>
                        <td>
                            {{ App\Models\Discuss::POST_TYPE_SELECT[$discuss->post_type] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.discuss.fields.rule') }}
                        </th>
                        <td>
                            {!! $discuss->rule !!}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.discusses.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection