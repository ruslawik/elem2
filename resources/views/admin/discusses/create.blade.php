@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.discuss.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.discusses.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="npa_id">{{ trans('cruds.discuss.fields.npa') }}</label>
                <select class="form-control select2 {{ $errors->has('npa') ? 'is-invalid' : '' }}" name="npa_id" id="npa_id" required>
                    @foreach($npas as $id => $npa)
                        <option value="{{ $id }}" {{ old('npa_id') == $id ? 'selected' : '' }}>{{ $npa }}</option>
                    @endforeach
                </select>
                @if($errors->has('npa'))
                    <div class="invalid-feedback">
                        {{ $errors->first('npa') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.discuss.fields.npa_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="institute_id">{{ trans('cruds.discuss.fields.institute') }}</label>
                <select class="form-control select2 {{ $errors->has('institute') ? 'is-invalid' : '' }}" name="institute_id" id="institute_id" required>
                    <!--
                    @foreach($institutes as $id => $institute)
                        <option value="{{ $id }}" {{ old('institute_id') == $id ? 'selected' : '' }}>{{ $institute }}</option>
                    @endforeach
                    !-->
                </select>
                @if($errors->has('institute'))
                    <div class="invalid-feedback">
                        {{ $errors->first('institute') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.discuss.fields.institute_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="jurisdiction_id">{{ trans('cruds.discuss.fields.jurisdiction') }}</label>
                <select class="form-control select2 {{ $errors->has('jurisdiction') ? 'is-invalid' : '' }}" name="jurisdiction_id" id="jurisdiction_id" required>
                    @foreach($jurisdictions as $id => $jurisdiction)
                        <option value="{{ $id }}" {{ old('jurisdiction_id') == $id ? 'selected' : '' }}>{{ $jurisdiction }}</option>
                    @endforeach
                </select>
                @if($errors->has('jurisdiction'))
                    <div class="invalid-feedback">
                        {{ $errors->first('jurisdiction') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.discuss.fields.jurisdiction_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.discuss.fields.post_type') }}</label>
                <select class="form-control {{ $errors->has('post_type') ? 'is-invalid' : '' }}" name="post_type" id="post_type" required>
                    <option value disabled {{ old('post_type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Discuss::POST_TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('post_type', 'academ') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('post_type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('post_type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.discuss.fields.post_type_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="rule">{{ trans('cruds.discuss.fields.rule') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('rule') ? 'is-invalid' : '' }}" name="rule" id="rule">{!! old('rule') !!}</textarea>
                @if($errors->has('rule'))
                    <div class="invalid-feedback">
                        {{ $errors->first('rule') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.discuss.fields.rule_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/discusses/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', {{ $discuss->id ?? 0 }});
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});


  $(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
  });


$('#npa_id').on('select2:select', function (e) {
  var data = e.params.data;
  $("#institute_id").select2({
    ajax: {
        url: "/select2/institutes/"+data.id,
        type: "post",
        dataType: 'json',
        data: function (params) {
            return {
                searchTerm: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
    }
 });

});
});

</script>

@endsection