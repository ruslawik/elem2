@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.institute.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.institutes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.institute.fields.id') }}
                        </th>
                        <td>
                            {{ $institute->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.institute.fields.name') }}
                        </th>
                        <td>
                            {{ $institute->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.institute.fields.npa') }}
                        </th>
                        <td>
                            {{ $institute->npa->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.institute.fields.parent_institute') }}
                        </th>
                        <td>
                            {{ $institute->parent_institute->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.institute.fields.type') }}
                        </th>
                        <td>
                            {{ App\Models\Institute::TYPE_SELECT[$institute->type] ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.institutes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection