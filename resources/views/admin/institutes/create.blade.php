@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.institute.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.institutes.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.institute.fields.name') }} (макс. 52 символов)</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" maxlength="52" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.institute.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="npa_id">{{ trans('cruds.institute.fields.npa') }}</label>
                <select class="form-control select2 {{ $errors->has('npa') ? 'is-invalid' : '' }}" name="npa_id" id="npa_id" required>
                    @foreach($npas as $id => $npa)
                        <option value="{{ $id }}" {{ old('npa_id') == $id ? 'selected' : '' }}>{{ $npa }}</option>
                    @endforeach
                </select>
                @if($errors->has('npa'))
                    <div class="invalid-feedback">
                        {{ $errors->first('npa') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.institute.fields.npa_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="parent_institute_id">{{ trans('cruds.institute.fields.parent_institute') }}</label>
                <select class="form-control select2 {{ $errors->has('parent_institute') ? 'is-invalid' : '' }}" name="parent_institute_id" id="parent_institute_id">
                    <!--
                    @foreach($parent_institutes as $id => $parent_institute)
                        <option value="{{ $id }}" {{ old('parent_institute_id') == $id ? 'selected' : '' }}>{{ $parent_institute }}</option>
                    @endforeach
                    !-->
                </select>
                @if($errors->has('parent_institute'))
                    <div class="invalid-feedback">
                        {{ $errors->first('parent_institute') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.institute.fields.parent_institute_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.institute.fields.type') }}</label>
                <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type" required>
                    <option value disabled {{ old('type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Institute::TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('type', 'baza') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.institute.fields.type_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
  });


$('#npa_id').on('select2:select', function (e) {
  var data = e.params.data;
  $("#parent_institute_id").select2({
    ajax: {
        url: "/select2/institutes_parent/"+data.id,
        type: "post",
        dataType: 'json',
        data: function (params) {
            return {
                searchTerm: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
    }
 });

});
});

</script>
@endsection
