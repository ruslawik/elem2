@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.institute.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.institutes.update", [$institute->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.institute.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $institute->name) }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.institute.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="npa_id">{{ trans('cruds.institute.fields.npa') }}</label>
                <select class="form-control select2 {{ $errors->has('npa') ? 'is-invalid' : '' }}" name="npa_id" id="npa_id" required>
                    @foreach($npas as $id => $npa)
                        <option value="{{ $id }}" {{ (old('npa_id') ? old('npa_id') : $institute->npa->id ?? '') == $id ? 'selected' : '' }}>{{ $npa }}</option>
                    @endforeach
                </select>
                @if($errors->has('npa'))
                    <div class="invalid-feedback">
                        {{ $errors->first('npa') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.institute.fields.npa_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="parent_institute_id">{{ trans('cruds.institute.fields.parent_institute') }}</label>
                <select class="form-control select2 {{ $errors->has('parent_institute') ? 'is-invalid' : '' }}" name="parent_institute_id" id="parent_institute_id">
                    @foreach($parent_institutes as $id => $parent_institute)
                        <option value="{{ $id }}" {{ (old('parent_institute_id') ? old('parent_institute_id') : $institute->parent_institute->id ?? '') == $id ? 'selected' : '' }}>{{ $parent_institute }}</option>
                    @endforeach
                </select>
                @if($errors->has('parent_institute'))
                    <div class="invalid-feedback">
                        {{ $errors->first('parent_institute') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.institute.fields.parent_institute_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.institute.fields.type') }}</label>
                <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type" required>
                    <option value disabled {{ old('type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Institute::TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('type', $institute->type) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.institute.fields.type_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
        <form method="POST" action="{{route('admin.update-numeration')}}" enctype="multipart/form-data">
            @method('POST')
            @csrf
            <input type="hidden" name="institute_id" value="{{$institute->id}}">

            <h5>Порядковые номера в юрисдикциях</h5>

                <div class="table">
                    <table>
                        @if($created)
                            @foreach($law_systems as $system)
                                <tr>
                                    <td>
                                        <label>{{$system->system->name}}</label>
                                    </td>
                                    <td>
                                        <input type="number" name="numerations[]" value="{{$system->num}}">

                                        <input type="hidden" name="system_id[]" value="{{$system->system->id}}">

                                    </td>
                                </tr>
                            @endforeach

                        @else
                            @foreach($law_systems as $system)
                                <tr>
                                    <td>
                                        <label>{{$system->name}}</label>
                                    </td>
                                    <td>
                                        <input type="number" name="numerations[]" value="">

                                        <input type="hidden" name="system_id[]" value="{{$system->id}}">

                                    </td>
                                </tr>
                            @endforeach
                        @endif

                    </table>

                </div>

            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
