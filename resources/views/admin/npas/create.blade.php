@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.npa.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.npas.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.npa.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.npa.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name_kz">Наименование на казахском</label>
                <input class="form-control {{ $errors->has('name_kz') ? 'is-invalid' : '' }}" type="text" name="name_kz" id="name" value="{{ old('name_kz', '') }}" required>
                @if($errors->has('name_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_kz') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label class="required" for="pod_name">Подзаголовок</label>
                <input class="form-control {{ $errors->has('pod_name') ? 'is-invalid' : '' }}" type="text" name="pod_name" id="pod_name" value="{{ old('pod_name', '') }}" required>
                @if($errors->has('pod_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('pod_name') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label class="required" for="pod_name_kz">Подзаголовок на казахском</label>
                <input class="form-control {{ $errors->has('pod_name_kz') ? 'is-invalid' : '' }}" type="text" name="pod_name_kz" id="pod_name_kz" value="{{ old('pod_name_kz', '') }}" required>
                @if($errors->has('pod_name_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('pod_name_kz') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label class="required" for="category_id">{{ trans('cruds.npa.fields.category') }}</label>
                <select class="form-control select2 {{ $errors->has('category') ? 'is-invalid' : '' }}" name="category_id" id="category_id" required>
                    @foreach($categories as $id => $category)
                        <option value="{{ $id }}" {{ old('category_id') == $id ? 'selected' : '' }}>{{ $category }}</option>
                    @endforeach
                </select>
                @if($errors->has('category'))
                    <div class="invalid-feedback">
                        {{ $errors->first('category') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.npa.fields.category_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.npa.fields.type') }}</label>
                <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type" required>
                    <option value disabled {{ old('type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Npa::TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('type', 'institute') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.npa.fields.type_helper') }}</span>
            </div>
            @if(Auth::user()->roles->contains(2))
              Организация
                <select class="form-control" name="organization_id" id="organization_id" required>
                        <option value="{{auth()->user()->organization_id}}">{{auth()->user()->organization->name}}</option>
                </select>
                <br>
            @else
            <div class="form-group">
                <label class="required" for="organization_id">{{ trans('cruds.npa.fields.organization') }}</label>
                <select class="form-control select2 {{ $errors->has('organization') ? 'is-invalid' : '' }}" name="organization_id" id="organization_id" required>
                    @foreach($organizations as $id => $organization)
                        <option value="{{ $id }}" {{ old('organization_id') == $id ? 'selected' : '' }}>{{ $organization }}</option>
                    @endforeach
                </select>
                @if($errors->has('organization'))
                    <div class="invalid-feedback">
                        {{ $errors->first('organization') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.npa.fields.organization_helper') }}</span>
            </div>
            @endif
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection