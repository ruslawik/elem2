@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.npa.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.npas.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.npa.fields.id') }}
                        </th>
                        <td>
                            {{ $npa->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.npa.fields.name') }}
                        </th>
                        <td>
                            {{ $npa->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.npa.fields.category') }}
                        </th>
                        <td>
                            {{ $npa->category->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.npa.fields.type') }}
                        </th>
                        <td>
                            {{ App\Models\Npa::TYPE_SELECT[$npa->type] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.npa.fields.organization') }}
                        </th>
                        <td>
                            {{ $npa->organization->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.npas.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#npa_institutes" role="tab" data-toggle="tab">
                {{ trans('cruds.institute.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="npa_institutes">
            @includeIf('admin.npas.relationships.npaInstitutes', ['institutes' => $npa->npaInstitutes])
        </div>
    </div>
</div>

@endsection