@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.razdel.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.razdels.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.razdel.fields.id') }}
                        </th>
                        <td>
                            {{ $razdel->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.razdel.fields.name') }}
                        </th>
                        <td>
                            {{ $razdel->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.razdel.fields.parent') }}
                        </th>
                        <td>
                            {{ $razdel->parent->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.razdels.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection