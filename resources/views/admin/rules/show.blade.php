@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.rule.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.rules.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.rule.fields.id') }}
                        </th>
                        <td>
                            {{ $rule->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.rule.fields.npa') }}
                        </th>
                        <td>
                            {{ $rule->npa->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.rule.fields.number') }}
                        </th>
                        <td>
                            {{ $rule->number }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.rule.fields.name') }}
                        </th>
                        <td>
                            {{ $rule->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.rule.fields.red_1') }}
                        </th>
                        <td>
                            {!! $rule->red_1 !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.rule.fields.red_2') }}
                        </th>
                        <td>
                            {!! $rule->red_2 !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.rule.fields.type') }}
                        </th>
                        <td>
                            {{ App\Models\Rule::TYPE_SELECT[$rule->type] ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.rules.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection