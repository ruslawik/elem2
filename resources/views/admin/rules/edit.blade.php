@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.rule.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.rules.update", [$rule->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="npa_id">{{ trans('cruds.rule.fields.npa') }}</label>
                <select class="form-control select2 {{ $errors->has('npa') ? 'is-invalid' : '' }}" name="npa_id" id="npa_id" required>
                    @foreach($npas as $id => $npa)
                        <option value="{{ $id }}" {{ (old('npa_id') ? old('npa_id') : $rule->npa->id ?? '') == $id ? 'selected' : '' }}>{{ $npa }}</option>
                    @endforeach
                </select>
                @if($errors->has('npa'))
                    <div class="invalid-feedback">
                        {{ $errors->first('npa') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.rule.fields.npa_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="number">{{ trans('cruds.rule.fields.number') }}</label>
                <input class="form-control {{ $errors->has('number') ? 'is-invalid' : '' }}" type="number" name="number" id="number" value="{{ old('number', $rule->number) }}" step="1" required>
                @if($errors->has('number'))
                    <div class="invalid-feedback">
                        {{ $errors->first('number') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.rule.fields.number_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.rule.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $rule->name) }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.rule.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="red_1">{{ trans('cruds.rule.fields.red_1') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('red_1') ? 'is-invalid' : '' }}" name="red_1" id="red_1">{!! old('red_1', $rule->red_1) !!}</textarea>
                @if($errors->has('red_1'))
                    <div class="invalid-feedback">
                        {{ $errors->first('red_1') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.rule.fields.red_1_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="red_2">{{ trans('cruds.rule.fields.red_2') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('red_2') ? 'is-invalid' : '' }}" name="red_2" id="red_2">{!! old('red_2', $rule->red_2) !!}</textarea>
                @if($errors->has('red_2'))
                    <div class="invalid-feedback">
                        {{ $errors->first('red_2') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.rule.fields.red_2_helper') }}</span>
            </div>
            <div class="form-group">
                <label>{{ trans('cruds.rule.fields.type') }}</label>
                <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type">
                    <option value disabled {{ old('type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Rule::TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('type', $rule->type) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.rule.fields.type_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/rules/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', {{ $rule->id ?? 0 }});
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection