@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.suggestion.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.suggestions.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="npa_id">{{ trans('cruds.suggestion.fields.npa') }}</label>
                <select class="form-control select2 {{ $errors->has('npa') ? 'is-invalid' : '' }}" name="npa_id" id="npa_id" required>
                    @foreach($npas as $id => $npa)
                        <option value="{{ $id }}" {{ old('npa_id') == $id ? 'selected' : '' }}>{{ $npa }}</option>
                    @endforeach
                </select>
                @if($errors->has('npa'))
                    <div class="invalid-feedback">
                        {{ $errors->first('npa') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.suggestion.fields.npa_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="rule_id">{{ trans('cruds.suggestion.fields.rule') }}</label>
                <select class="form-control select2 {{ $errors->has('rule') ? 'is-invalid' : '' }}" name="rule_id" id="rule_id" required>
                   <!--
                    @foreach($rules as $id => $rule)
                        <option value="{{ $id }}" {{ old('rule_id') == $id ? 'selected' : '' }}>{{ $rule }}</option>
                    @endforeach
                    !-->
                </select>
                @if($errors->has('rule'))
                    <div class="invalid-feedback">
                        {{ $errors->first('rule') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.suggestion.fields.rule_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="suggestion">{{ trans('cruds.suggestion.fields.suggestion') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('suggestion') ? 'is-invalid' : '' }}" name="suggestion" id="suggestion">{!! old('suggestion') !!}</textarea>
                @if($errors->has('suggestion'))
                    <div class="invalid-feedback">
                        {{ $errors->first('suggestion') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.suggestion.fields.suggestion_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
  });


$('#npa_id').on('select2:select', function (e) {
  var data = e.params.data;
  $("#rule_id").select2({
    ajax: {
        url: "/select2/rules/"+data.id,
        type: "post",
        dataType: 'json',
        data: function (params) {
            return {
                searchTerm: params.term // search term
            };
        },
        processResults: function (response) {
            return {
                results: response
            };
        },
        cache: true
    }
 });

});
});


    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/suggestions/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', {{ $suggestion->id ?? 0 }});
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection