@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.category.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.categories.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.category.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.category.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name_kz">Наименование на казахском</label>
                <input class="form-control {{ $errors->has('name_kz') ? 'is-invalid' : '' }}" type="text" name="name_kz" id="name_kz" value="{{ old('name_kz', '') }}" required>
                @if($errors->has('name_kz'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name_kz') }}
                    </div>
                @endif
            </div>
             <div class="form-group">
                <label class="required" for="place_num">Порядковый номер в слайдере</label>
                <input class="form-control {{ $errors->has('place_num') ? 'is-invalid' : '' }}" type="text" name="place_num" id="place_num" value="{{ old('place_num', '') }}" required>
                @if($errors->has('place_num'))
                    <div class="invalid-feedback">
                        {{ $errors->first('place_num') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.category.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection