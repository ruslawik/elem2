@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.argument.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.arguments.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.argument.fields.id') }}
                        </th>
                        <td>
                            {{ $argument->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.argument.fields.argument') }}
                        </th>
                        <td>
                            {!! $argument->argument !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.argument.fields.position') }}
                        </th>
                        <td>
                            {{ $argument->position->title ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.arguments.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection