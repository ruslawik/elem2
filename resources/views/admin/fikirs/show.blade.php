@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.fikir.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.fikirs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.fikir.fields.id') }}
                        </th>
                        <td>
                            {{ $fikir->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fikir.fields.doctrine') }}
                        </th>
                        <td>
                            {{ $fikir->doctrine->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fikir.fields.fio') }}
                        </th>
                        <td>
                            {{ $fikir->fio }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fikir.fields.text') }}
                        </th>
                        <td>
                            {!! $fikir->text !!}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.fikirs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection