@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.fikir.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.fikirs.update", [$fikir->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="doctrine_id">{{ trans('cruds.fikir.fields.doctrine') }}</label>
                <select class="form-control select2 {{ $errors->has('doctrine') ? 'is-invalid' : '' }}" name="doctrine_id" id="doctrine_id" required>
                    @foreach($doctrines as $id => $doctrine)
                        <option value="{{ $id }}" {{ (old('doctrine_id') ? old('doctrine_id') : $fikir->doctrine->id ?? '') == $id ? 'selected' : '' }}>{{ $doctrine }}</option>
                    @endforeach
                </select>
                @if($errors->has('doctrine'))
                    <div class="invalid-feedback">
                        {{ $errors->first('doctrine') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.fikir.fields.doctrine_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="fio">{{ trans('cruds.fikir.fields.fio') }}</label>
                <input class="form-control {{ $errors->has('fio') ? 'is-invalid' : '' }}" type="text" name="fio" id="fio" value="{{ old('fio', $fikir->fio) }}" required>
                @if($errors->has('fio'))
                    <div class="invalid-feedback">
                        {{ $errors->first('fio') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.fikir.fields.fio_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="text">{{ trans('cruds.fikir.fields.text') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('text') ? 'is-invalid' : '' }}" name="text" id="text">{!! old('text', $fikir->text) !!}</textarea>
                @if($errors->has('text'))
                    <div class="invalid-feedback">
                        {{ $errors->first('text') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.fikir.fields.text_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/fikirs/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', {{ $fikir->id ?? 0 }});
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection