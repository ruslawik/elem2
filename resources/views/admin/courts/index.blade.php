@extends('layouts.admin')
@section('content')
@can('court_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.courts.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.court.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.court.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Court">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.court.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.court.fields.doctrine') }}
                        </th>
                        <th>
                            {{ trans('cruds.court.fields.law_system') }}
                        </th>
                        <th>
                            {{ trans('cruds.court.fields.type') }}
                        </th>
                        <th>
                            {{ trans('cruds.court.fields.file') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($courts as $key => $court)
                        <tr data-entry-id="{{ $court->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $court->id ?? '' }}
                            </td>
                            <td>
                                {{ $court->doctrine->name ?? '' }}
                            </td>
                            <td>
                                {{ $court->law_system->name ?? '' }}
                            </td>
                            <td>
                                {{ App\Models\Court::TYPE_SELECT[$court->type] ?? '' }}
                            </td>
                            <td>
                                @foreach($court->file as $key => $media)
                                    <a href="{{ $media->getUrl() }}" target="_blank">
                                        {{ trans('global.view_file') }}
                                    </a>
                                @endforeach
                            </td>
                            <td>
                                @can('court_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.courts.show', $court->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('court_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.courts.edit', $court->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('court_delete')
                                    <form action="{{ route('admin.courts.destroy', $court->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('court_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.courts.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Court:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection