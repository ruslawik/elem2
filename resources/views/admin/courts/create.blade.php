@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.court.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.courts.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="doctrine_id">{{ trans('cruds.court.fields.doctrine') }}</label>
                <select class="form-control select2 {{ $errors->has('doctrine') ? 'is-invalid' : '' }}" name="doctrine_id" id="doctrine_id" required>
                    @foreach($doctrines as $id => $doctrine)
                        <option value="{{ $id }}" {{ old('doctrine_id') == $id ? 'selected' : '' }}>{{ $doctrine }}</option>
                    @endforeach
                </select>
                @if($errors->has('doctrine'))
                    <div class="invalid-feedback">
                        {{ $errors->first('doctrine') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.court.fields.doctrine_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="law_system_id">{{ trans('cruds.court.fields.law_system') }}</label>
                <select class="form-control select2 {{ $errors->has('law_system') ? 'is-invalid' : '' }}" name="law_system_id" id="law_system_id" required>
                    @foreach($law_systems as $id => $law_system)
                        <option value="{{ $id }}" {{ old('law_system_id') == $id ? 'selected' : '' }}>{{ $law_system }}</option>
                    @endforeach
                </select>
                @if($errors->has('law_system'))
                    <div class="invalid-feedback">
                        {{ $errors->first('law_system') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.court.fields.law_system_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.court.fields.type') }}</label>
                <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type" required>
                    <option value disabled {{ old('type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Court::TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('type', 'osnova') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.court.fields.type_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="citata">{{ trans('cruds.court.fields.citata') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('citata') ? 'is-invalid' : '' }}" name="citata" id="citata">{!! old('citata') !!}</textarea>
                @if($errors->has('citata'))
                    <div class="invalid-feedback">
                        {{ $errors->first('citata') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.court.fields.citata_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="file">{{ trans('cruds.court.fields.file') }}</label>
                <div class="needsclick dropzone {{ $errors->has('file') ? 'is-invalid' : '' }}" id="file-dropzone">
                </div>
                @if($errors->has('file'))
                    <div class="invalid-feedback">
                        {{ $errors->first('file') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.court.fields.file_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/courts/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', {{ $court->id ?? 0 }});
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

<script>
    var uploadedFileMap = {}
Dropzone.options.fileDropzone = {
    url: '{{ route('admin.courts.storeMedia') }}',
    maxFilesize: 10, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 10
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="file[]" value="' + response.name + '">')
      uploadedFileMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedFileMap[file.name]
      }
      $('form').find('input[name="file[]"][value="' + name + '"]').remove()
    },
    init: function () {
@if(isset($court) && $court->file)
          var files =
            {!! json_encode($court->file) !!}
              for (var i in files) {
              var file = files[i]
              this.options.addedfile.call(this, file)
              file.previewElement.classList.add('dz-complete')
              $('form').append('<input type="hidden" name="file[]" value="' + file.file_name + '">')
            }
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection