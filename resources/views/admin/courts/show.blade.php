@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.court.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.courts.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.court.fields.id') }}
                        </th>
                        <td>
                            {{ $court->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.court.fields.doctrine') }}
                        </th>
                        <td>
                            {{ $court->doctrine->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.court.fields.law_system') }}
                        </th>
                        <td>
                            {{ $court->law_system->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.court.fields.type') }}
                        </th>
                        <td>
                            {{ App\Models\Court::TYPE_SELECT[$court->type] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.court.fields.citata') }}
                        </th>
                        <td>
                            {!! $court->citata !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.court.fields.file') }}
                        </th>
                        <td>
                            @foreach($court->file as $key => $media)
                                <a href="{{ $media->getUrl() }}" target="_blank">
                                    {{ trans('global.view_file') }}
                                </a>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.courts.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection