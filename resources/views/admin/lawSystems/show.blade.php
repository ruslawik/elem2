@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.lawSystem.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.law-systems.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.lawSystem.fields.id') }}
                        </th>
                        <td>
                            {{ $lawSystem->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lawSystem.fields.name') }}
                        </th>
                        <td>
                            {{ $lawSystem->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lawSystem.fields.avatar') }}
                        </th>
                        <td>
                            @if($lawSystem->avatar)
                                <a href="{{ $lawSystem->avatar->getUrl() }}" target="_blank" style="display: inline-block">
                                    <img src="{{ $lawSystem->avatar->getUrl('thumb') }}">
                                </a>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.law-systems.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection