
$('body').on('click', '.tab_buttons button, .btns a', function () {
    $(this).addClass('active').siblings('button').removeClass('active');
    $('body').find('.tab_block[data-tab=' + $(this).data("tab-open") + ']').addClass('active').siblings('.tab_block').removeClass('active');
});

$('body').on('click', '.buttons_list .li > button', function() {
    if (!$(this).hasClass("active")) {
        $(".buttons_list .li > button .arrow svg")
            .css('transform', 'rotate(0deg)');
        $(this)
            .find(".arrow svg")
            .css('transform', 'rotate(90deg)');
        $(".buttons_list .li > button").removeClass("active");
        $(this).addClass("active");
        $(".item_inside").slideUp(200);
        $(this)
            .siblings(".item_inside")
            .slideDown(200);
    }

});

function open_razdel(razdel_id){
    if ($("#"+razdel_id).hasClass("active")) {
        $("#"+razdel_id).removeClass("active");
        $("#"+razdel_id)
            .siblings(".item_inside")
            .slideUp(200);
        $(".buttons_list .li > button .arrow svg")
            .css('transform', 'rotate(0deg)');
    } else {
        $(".buttons_list .li > button .arrow svg")
            .css('transform', 'rotate(0deg)');
        $("#"+razdel_id)
            .find(".arrow svg")
            .css('transform', 'rotate(90deg)');
        $(".buttons_list .li > button").removeClass("active");
        $("#"+razdel_id).addClass("active");
        //$("#"+razdel_id+" a").attr('style', 'color: white !important;');
        $(".item_inside").slideUp(200);
        $("#"+razdel_id)
            .siblings(".item_inside")
            .slideDown(200);
    }
}

/*
$('body').on('click', '.comparison_box .notshow .close', function () {
    $(this).parents('.comparison_box').prev().remove();
    $(this).parents('.comparison_box').next().remove();
    $(this).parents('.comparison_box').remove();
});

$('body').on('click', '.performance_list .close', function () {
    let performance_list = $(this).parent('.performance_list');
    performance_list.fadeOut();
    setTimeout(function () {
        performance_list.siblings('.add_compare').fadeIn()
    }, 500)
});
$('body').on('click', '.comparison_box > .close', function () {
    $(this).parents('.comparison_box').next().remove();
    $(this).parents('.comparison_box').remove();
});
*/
var index = 0;

$('body').on('click','.edit_modal .add_argument_btn', function () {
    index = 0;
    $('.add_argument .head').append(`<div class="theargument">
        <button class="close">
        <svg viewBox="0 0 329.26933 329" xmlns="http://www.w3.org/2000/svg"><path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg>
        </button>
        <div class="name">Аргумент <span class="index"></span></div>
    <textarea name="arguments[]"></textarea>
    </div>`);

    $('.theargument').each(function(){
       index++;
       $(this).find('.index').text(index);

    });

});

$('body').on('click', '.theargument .close', function () {
    index = 0;
    $(this).parent('.theargument').remove();
    $('.theargument').each(function(){
        index++;
        $(this).find('.index').text(index);

    });
});

// handle links with @href started with '#' only
$(document).on('click', 'a[href^="#"]', function(e) {
    // target element id
    var id = $(this).attr('href');

    // target element
    var $id = $(id);
    if ($id.length === 0) {
        return;
    }

    // prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault();

    // top position relative to the document
    var pos = $id.offset().top;

    // animated top scrolling
    $('body, html').animate({scrollTop: pos});
});

if($(window).width() > 575){
    $('body').on('click', '.arrow.next', function(){
        $('.dragscroll').animate({scrollLeft: $('.dragscroll').scrollLeft() + 350}, 300);
    });
    $('body').on('click', '.arrow.prev', function(){
        $('.dragscroll').animate({scrollLeft: $('.dragscroll').scrollLeft() - 350}, 300);
    });
}

if($(window).width() < 575){
    $('body').on('click', '.arrow.next', function(){
        $('.dragscroll').animate({scrollLeft: $('.dragscroll').scrollLeft() + 220}, 300);
    });
    $('body').on('click', '.arrow.prev', function(){
        $('.dragscroll').animate({scrollLeft: $('.dragscroll').scrollLeft() - 220}, 300);
    });
}
$.fn.isHScrollable = function () {
    return this[0].scrollWidth > this[0].clientWidth;
};
$('.dragscroll a').click(function(event) {
    if ($(this).closest('.dragscroll').hasClass('dragging')) {
        event.preventDefault();
        return false;
    }
});
if($('.topButtons_slider').isHScrollable()){
    /* EMPTY SPACES, SO WHEN SLIDER AT THE END, THERE WOULD BE SPACE FROM RIGHT THAT WILL TAKE COVER OF GRADIENT */
    $('.top_buttons').addClass('slider');
    $('.topButtons_slider').prepend(`
        <div class="empty_block"></div>
    `);
    $('.topButtons_slider').parents('.container').css({
        'padding-left': '0',
        'padding-right': '0',
    });
    $('.topButtons_slider').css({
        'justify-content': 'left',

    });
    $('.topButtons_slider').append(`
        <div class="empty_block"></div>
        <div class="left fade-effect"></div>
        <div class="right fade-effect"></div>

    `);

    $('.topButtons_slider').parents('.dragscroll').addClass('activated').append(`

        <div class="arrow prev">
            <img src="/front/assets/images/arrow.svg" style="transform: scale(-1)">
        </div>
        <div class="arrow next">
            <img src="/front/assets/images/arrow.svg">

        </div>
    `);
}
