/**
 * htmldiff.js a diff algorithm that understands HTML, and produces HTML in the browser.
 * 
 * @author https://github.com/tnwinc
 * @see https://github.com/tnwinc/htmldiff.js
 */
//!function(){var e,n,t,r,i,f,_,a,o,s,u,h,l,c,d,b,p;o=function(e){return">"===e},s=function(e){return"<"===e},h=function(e){return/^\s+$/.test(e)},u=function(e){return/^\s*<[^>]+>\s*$/.test(e)},l=function(e){return!u(e)},e=function(){return function(e,n,t){this.start_in_before=e,this.start_in_after=n,this.length=t,this.end_in_before=this.start_in_before+this.length-1,this.end_in_after=this.start_in_after+this.length-1}}(),a=function(e){var n,t,r,i,f,_;for(f="char",t="",_=[],r=0,i=e.length;r<i;r++)switch(n=e[r],f){case"tag":o(n)?(t+=">",_.push(t),t="",f=h(n)?"whitespace":"char"):t+=n;break;case"char":s(n)?(t&&_.push(t),t="<",f="tag"):/\s/.test(n)?(t&&_.push(t),t=n,f="whitespace"):/[\w\#@]+/i.test(n)?t+=n:(t&&_.push(t),t=n);break;case"whitespace":s(n)?(t&&_.push(t),t="<",f="tag"):h(n)?t+=n:(t&&_.push(t),t=n,f="char");break;default:throw new Error("Unknown mode "+f)}return t&&_.push(t),_},f=function(n,t,r,i,f,_,a){var o,s,u,h,l,c,d,b,p,g,w,v,k,m,y;for(s=i,o=_,u=0,w={},c=h=m=i,y=f;m<=y?h<y:h>y;c=m<=y?++h:--h){for(k={},d=0,b=(p=r[n[c]]).length;d<b;d++)if(!((l=p[d])<_)){if(l>=a)break;null==w[l-1]&&(w[l-1]=0),v=w[l-1]+1,k[l]=v,v>u&&(s=c-v+1,o=l-v+1,u=v)}w=k}return 0!==u&&(g=new e(s,o,u)),g},d=function(e,n,t,r,i,_,a,o){var s;return null!=(s=f(e,0,t,r,i,_,a))&&(r<s.start_in_before&&_<s.start_in_after&&d(e,n,t,r,s.start_in_before,_,s.start_in_after,o),o.push(s),s.end_in_before<=i&&s.end_in_after<=a&&d(e,n,t,s.end_in_before+1,i,s.end_in_after+1,a,o)),o},r=function(e){var n,t,r,i,f,_;if(null==e.find_these)throw new Error("params must have find_these key");if(null==e.in_these)throw new Error("params must have in_these key");for(r={},n=0,i=(f=e.find_these).length;n<i;n++)for(r[_=f[n]]=[],t=e.in_these.indexOf(_);-1!==t;)r[_].push(t),t=e.in_these.indexOf(_,t+1);return r},_=function(e,n){var t,i;return i=[],t=r({find_these:e,in_these:n}),d(e,n,t,0,e.length,0,n.length,i)},n=function(n,t){var r,i,f,a,o,s,u,h,l,c,d,b,p,g,w,v;if(null==n)throw new Error("before_tokens?");if(null==t)throw new Error("after_tokens?");for(w=g=0,p=[],r={"false,false":"replace","true,false":"insert","false,true":"delete","true,true":"none"},(d=_(n,t)).push(new e(n.length,t.length,0)),a=f=0,h=d.length;f<h;a=++f)"none"!==(i=r[[w===(c=d[a]).start_in_before,g===c.start_in_after].toString()])&&p.push({action:i,start_in_before:w,end_in_before:"insert"!==i?c.start_in_before-1:void 0,start_in_after:g,end_in_after:"delete"!==i?c.start_in_after-1:void 0}),0!==c.length&&p.push({action:"equal",start_in_before:c.start_in_before,end_in_before:c.end_in_before,start_in_after:c.start_in_after,end_in_after:c.end_in_after}),w=c.end_in_before+1,g=c.end_in_after+1;for(v=[],u={action:"none"},o=function(e){return"equal"===e.action&&(e.end_in_before-e.start_in_before==0&&/^\s$/.test(n.slice(e.start_in_before,+e.end_in_before+1||9e9)))},s=0,l=p.length;s<l;s++)o(b=p[s])&&"replace"===u.action||"replace"===b.action&&"replace"===u.action?(u.end_in_before=b.end_in_before,u.end_in_after=b.end_in_after):(v.push(b),u=b);return v},t=function(e,n,t){var r,i,f,_,a,o;for(_=void 0,f=i=0,a=(n=n.slice(e,+n.length+1||9e9)).length;i<a&&(o=n[f],!0===(r=t(o))&&(_=f),!1!==r);f=++i);return null!=_?n.slice(0,+_+1||9e9):[]},p=function(e,n){var r,i,f,_,a;for(_="",f=0,r=n.length;;){if(f>=r)break;if(i=t(f,n,l),f+=i.length,0!==i.length&&(_+="<"+e+">"+i.join("")+"</"+e+">"),f>=r)break;f+=(a=t(f,n,u)).length,_+=a.join("")}return _},(c={equal:function(e,n,t){return n.slice(e.start_in_before,+e.end_in_before+1||9e9).join("")},insert:function(e,n,t){var r;return r=t.slice(e.start_in_after,+e.end_in_after+1||9e9),p("ins",r)},delete:function(e,n,t){var r;return r=n.slice(e.start_in_before,+e.end_in_before+1||9e9),p("del",r)}}).replace=function(e,n,t){return c.delete(e,n,t)+c.insert(e,n,t)},b=function(e,n,t){var r,i,f,_;for(_="",r=0,i=t.length;r<i;r++)f=t[r],_+=c[f.action](f,e,n);return _},(i=function(e,t){var r;return e===t?e:(e=a(e),t=a(t),r=n(e,t),b(e,t,r))}).html_to_tokens=a,i.find_matching_blocks=_,_.find_match=f,_.create_index=r,i.calculate_operations=n,i.render_operations=b,"function"==typeof define?define([],function(){return i}):"undefined"!=typeof module&&null!==module?module.exports=i:"undefined"!=typeof window&&(window.htmldiff=i)}();
/* 
Simple Diff for version 1.0 (ported to JavaScript)
Annotate two versions of a list with the values that have been
changed between the versions, similar to unix's `diff` but with
a dead-simple JavaScript interface.
JavaScript port by DJ Mountney (twk3) based on code by Paul Butler.
(C) 2008-2012 <http://www.paulbutler.org/>
May be used and distributed under the zlib/libpng license
<http://www.opensource.org/licenses/zlib-license.php>
*/

var diff = function(before, after) {
    /*
        Find the differences between two lists. Returns a list of pairs, where the
        first value is in ['+','-','='] and represents an insertion, deletion, or
        no change for that list. The second value of the pair is the list
        of elements.
        Params:
            before  the old list of immutable, comparable values (ie. a list
                    of strings)
            after   the new list of immutable, comparable values
        Returns:
            A list of pairs, with the first part of the pair being one of three
            strings ('-', '+', '=') and the second part being a list of values from
            the original before and/or after lists. The first part of the pair
            corresponds to whether the list of values is a deletion, insertion, or
            unchanged, respectively.
        Examples:
            diff([1,2,3,4],[1,3,4])
            [["=",[1]],["-",[2]],["=",[3,4]]]
            diff([1,2,3,4],[2,3,4,1])
            [["-",[1]],["=",[2,3,4]],["+",[1]]]
            diff('The quick brown fox jumps over the lazy dog'.split(/[ ]+/),
                'The slow blue cheese drips over the lazy carrot'.split(/[ ]+/))
            [["=",["The"]],
             ["-",["quick","brown","fox","jumps"]],
             ["+",["slow","blue","cheese","drips"]],
             ["=",["over","the","lazy"]],
             ["-",["dog"]],
             ["+",["carrot"]]]
    */

    // Create a map from before values to their indices
    var oldIndexMap = {}, i;
    for (i = 0; i < before.length; i ++) {
        oldIndexMap[before[i]] = oldIndexMap[before[i]] || [];
        oldIndexMap[before[i]].push(i);
    }

    // Find the largest substring common to before and after.
    // We use a dynamic programming approach here.
    // We iterate over each value in the `after` list.
    // At each iteration, `overlap[inew]` is the
    // length of the largest substring of `before.slice(0, iold)` equal
    // to a substring of `after.splice(0, iold)` (or unset when
    // `before[iold]` != `after[inew]`).
    // At each stage of iteration, the new `overlap` (called
    // `_overlap` until the original `overlap` is no longer needed)
    // is built from the old one.
    // If the length of overlap exceeds the largest substring
    // seen so far (`subLength`), we update the largest substring
    // to the overlapping strings.

    var overlap = [], startOld, startNew, subLength, inew;

    // `startOld` is the index of the beginning of the largest overlapping
    // substring in the before list. `startNew` is the index of the beginning
    // of the same substring in the after list. `subLength` is the length that
    // overlaps in both.
    // These track the largest overlapping substring seen so far, so naturally
    // we start with a 0-length substring.
    startOld = startNew = subLength = 0;

    for (inew = 0; inew < after.length; inew++) {
        var _overlap                = [];
        oldIndexMap[after[inew]]    = oldIndexMap[after[inew]] || [];
        for (i = 0; i < oldIndexMap[after[inew]].length; i++) {
            var iold        = oldIndexMap[after[inew]][i];
            // now we are considering all values of val such that
            // `before[iold] == after[inew]`
            _overlap[iold]  = ((iold && overlap[iold-1]) || 0) + 1;
            if (_overlap[iold] > subLength) {
                // this is the largest substring seen so far, so store its
                // indices
                subLength   = _overlap[iold];
                startOld    = iold - subLength + 1;
                startNew    = inew - subLength + 1;
            }
        }
        overlap = _overlap;
    }

    if (subLength === 0) {
        // If no common substring is found, we return an insert and delete...
        var result = [];
        before.length && result.push(['-', before]);
        after.length  && result.push(['+', after]);
        return result;
    }

    // ...otherwise, the common substring is unchanged and we recursively
    // diff the text before and after that substring
    return [].concat(
        diff(before.slice(0, startOld), after.slice(0, startNew)),
        [['=', after.slice(startNew, startNew + subLength)]],
        diff(before.slice(startOld + subLength), after.slice(startNew + subLength))
    );
};

var stringDiff = function(before, after) {
    /*
        Returns the difference between the old and new strings when split on
        whitespace. Considers punctuation a part of the word
        This function is intended as an example; you'll probably want
        a more sophisticated wrapper in practice.
        Params:
            before  the old string
            after   the new string
        Returns:
            the output of `diff` on the two strings after splitting them
            on whitespace (a list of change instructions; see the comment
            of `diff`)
        Examples:
            stringDiff('The quick brown fox', 'The fast blue fox')
            [["=",["The"]],
             ["-",["quick","brown"]],
             ["+",["fast","blue"]],
             ["=",["fox"]]]
    */
    return diff(before.split(/[ ]+/), after.split(/[ ]+/));
};

var htmlDiff = function(before, after) {
    /*
        Returns the difference between two strings (as in stringDiff) in
        HTML format. HTML code in the strings is NOT escaped, so you
        will get weird results if the strings contain HTML.
        This function is intended as an example; you'll probably want
        a more sophisticated wrapper in practice.
        Params:
            before  the old string
            after   the new string
        Returns:
            the output of the diff expressed with HTML <ins> and <del>
            tags.
        Examples:
            htmlDiff('The quick brown fox', 'The fast blue fox')
            'The <del>quick brown</del> <ins>fast blue</ins> fox'
    */
    var a, b, con, diff, i, results = [];
    con = {
        '=': function(x) {
            return x;
        },
        '+': function(x) {
            return '<ins>' + x + '</ins>';
        },
        '-': function(x) {
            return '<del>' + x + '</del>';
        }
    };

    diff = stringDiff(before, after);
    for (i=0; i < diff.length; i++) {
        var chunk = diff[i];
        results.push(con[chunk[0]](chunk[1].join(' ')));
    }

    return results.join(' ');
};

var checkDiff = function(before, after) {
    /*
        This tests that diffs returned by `diff` are valid. You probably won't
        want to use this function, but it's provided for documentation and
        testing.
        A diff should satisfy the property that the before input is equal to the
        elements of the result annotated with '-' or '=' concatenated together.
        Likewise, the after input is equal to the elements of the result annotated
        with '+' or '=' concatenated together. This function compares `before`,
        `after`, and the results of `diff(before, after)` to ensure this is true.
        Tests:
            checkDiff('ABCBA', 'CBABA')
            checkDiff('Foobarbaz', 'Foobarbaz')
            checkDiff('Foobarbaz', 'Boobazbam')
            checkDiff('The quick brown fox', 'Some quick brown car')
            checkDiff('A thick red book', 'A quick blue book')
            checkDiff('dafhjkdashfkhasfjsdafdasfsda', 'asdfaskjfhksahkfjsdha')
            checkDiff('88288822828828288282828', '88288882882828282882828')
            checkDiff('1234567890', '24689')
    */
    before  = [before];
    after   = [after];

    var result  = diff(before, after),
        _before = [],
        _after  = [], i;

    for (i=0; i < result.length; i++) {
        switch (result[i][0]) {
            case '-':
                _before = _before.concat(result[i][1]);
                break;
            case '+':
                _after  = _after.concat(result[i][1]);
                break;
            default:
                _before = _before.concat(result[i][1]);
                _after  = _after.concat( result[i][1]);
        }
    }

    console.assert(JSON.stringify(before) === JSON.stringify(_before), 'Expected', before, 'got', _before);
    console.assert(JSON.stringify(after)  === JSON.stringify(_after),  'Expected', after,  'got', _after);
};

if (typeof(module) === 'object') {
  // Export functionality if used as a node.js or requirejs module
  module.exports = {
    diff: diff,
    htmlDiff: htmlDiff,
    stringDiff: stringDiff,
    checkDiff: checkDiff
  };
}