<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToArgumentsTable extends Migration
{
    public function up()
    {
        Schema::table('arguments', function (Blueprint $table) {
            $table->unsignedInteger('position_id');
            $table->foreign('position_id', 'position_fk_1991669')->references('id')->on('positions');
        });
    }
}
