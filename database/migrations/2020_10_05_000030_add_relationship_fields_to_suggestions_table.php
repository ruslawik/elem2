<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToSuggestionsTable extends Migration
{
    public function up()
    {
        Schema::table('suggestions', function (Blueprint $table) {
            $table->unsignedInteger('rule_id');
            $table->foreign('rule_id', 'rule_fk_2329179')->references('id')->on('rules');
            $table->unsignedInteger('npa_id');
            $table->foreign('npa_id', 'npa_fk_2329956')->references('id')->on('npas');
        });
    }
}
