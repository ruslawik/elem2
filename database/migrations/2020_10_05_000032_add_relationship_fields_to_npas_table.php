<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToNpasTable extends Migration
{
    public function up()
    {
        Schema::table('npas', function (Blueprint $table) {
            $table->unsignedInteger('organization_id');
            $table->foreign('organization_id', 'organization_fk_1991328')->references('id')->on('organizations');
            $table->unsignedInteger('category_id');
            $table->foreign('category_id', 'category_fk_2321884')->references('id')->on('categories');
        });
    }
}
