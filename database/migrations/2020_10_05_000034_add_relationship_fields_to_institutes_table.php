<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToInstitutesTable extends Migration
{
    public function up()
    {
        Schema::table('institutes', function (Blueprint $table) {
            $table->unsignedInteger('npa_id');
            $table->foreign('npa_id', 'npa_fk_1991366')->references('id')->on('npas');
            $table->unsignedInteger('parent_institute_id')->nullable();
            $table->foreign('parent_institute_id', 'parent_institute_fk_2322011')->references('id')->on('institutes');
        });
    }
}
