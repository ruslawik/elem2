<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctrinesTable extends Migration
{
    public function up()
    {
        Schema::create('doctrines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('alter_name')->nullable();
            $table->longText('goal');
            $table->longText('base');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
