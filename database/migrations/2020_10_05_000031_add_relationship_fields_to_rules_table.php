<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToRulesTable extends Migration
{
    public function up()
    {
        Schema::table('rules', function (Blueprint $table) {
            $table->unsignedInteger('npa_id');
            $table->foreign('npa_id', 'npa_fk_2329175')->references('id')->on('npas');
        });
    }
}
