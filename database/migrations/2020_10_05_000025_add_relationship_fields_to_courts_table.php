<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToCourtsTable extends Migration
{
    public function up()
    {
        Schema::table('courts', function (Blueprint $table) {
            $table->unsignedInteger('doctrine_id');
            $table->foreign('doctrine_id', 'doctrine_fk_2329724')->references('id')->on('doctrines');
            $table->unsignedInteger('law_system_id');
            $table->foreign('law_system_id', 'law_system_fk_2329725')->references('id')->on('law_systems');
        });
    }
}
