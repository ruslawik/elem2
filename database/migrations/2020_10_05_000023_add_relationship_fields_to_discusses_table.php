<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToDiscussesTable extends Migration
{
    public function up()
    {
        Schema::table('discusses', function (Blueprint $table) {
            $table->unsignedInteger('institute_id');
            $table->foreign('institute_id', 'institute_fk_1991548')->references('id')->on('institutes');
            $table->unsignedInteger('jurisdiction_id');
            $table->foreign('jurisdiction_id', 'jurisdiction_fk_2329189')->references('id')->on('law_systems');
            $table->unsignedInteger('npa_id');
            $table->foreign('npa_id', 'npa_fk_2329957')->references('id')->on('npas');
        });
    }
}
