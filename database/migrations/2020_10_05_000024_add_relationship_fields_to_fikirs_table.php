<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToFikirsTable extends Migration
{
    public function up()
    {
        Schema::table('fikirs', function (Blueprint $table) {
            $table->unsignedInteger('doctrine_id');
            $table->foreign('doctrine_id', 'doctrine_fk_2329734')->references('id')->on('doctrines');
        });
    }
}
