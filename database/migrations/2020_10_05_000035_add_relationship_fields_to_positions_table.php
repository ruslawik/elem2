<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToPositionsTable extends Migration
{
    public function up()
    {
        Schema::table('positions', function (Blueprint $table) {
            $table->unsignedInteger('author_id');
            $table->foreign('author_id', 'author_fk_1992844')->references('id')->on('users');
            $table->unsignedInteger('doctrine_id')->nullable();
            $table->foreign('doctrine_id', 'doctrine_fk_2329749')->references('id')->on('doctrines');
        });
    }
}
