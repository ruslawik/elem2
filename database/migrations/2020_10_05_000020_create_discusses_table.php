<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscussesTable extends Migration
{
    public function up()
    {
        Schema::create('discusses', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('rule');
            $table->string('post_type');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
