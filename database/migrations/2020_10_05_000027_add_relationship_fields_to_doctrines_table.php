<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToDoctrinesTable extends Migration
{
    public function up()
    {
        Schema::table('doctrines', function (Blueprint $table) {
            $table->unsignedInteger('razdel_id');
            $table->foreign('razdel_id', 'razdel_fk_2329740')->references('id')->on('razdels');
        });
    }
}
