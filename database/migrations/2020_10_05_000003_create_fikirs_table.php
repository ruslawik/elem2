<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFikirsTable extends Migration
{
    public function up()
    {
        Schema::create('fikirs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fio');
            $table->longText('text');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
