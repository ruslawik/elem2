<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNormatsTable extends Migration
{
    public function up()
    {
        Schema::create('normats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('text');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
