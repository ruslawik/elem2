<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArgumentsTable extends Migration
{
    public function up()
    {
        Schema::create('arguments', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('argument');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
