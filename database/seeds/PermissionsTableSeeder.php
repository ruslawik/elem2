<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'organization_create',
            ],
            [
                'id'    => 18,
                'title' => 'organization_edit',
            ],
            [
                'id'    => 19,
                'title' => 'organization_show',
            ],
            [
                'id'    => 20,
                'title' => 'organization_delete',
            ],
            [
                'id'    => 21,
                'title' => 'organization_access',
            ],
            [
                'id'    => 22,
                'title' => 'npa_create',
            ],
            [
                'id'    => 23,
                'title' => 'npa_edit',
            ],
            [
                'id'    => 24,
                'title' => 'npa_show',
            ],
            [
                'id'    => 25,
                'title' => 'npa_delete',
            ],
            [
                'id'    => 26,
                'title' => 'npa_access',
            ],
            [
                'id'    => 27,
                'title' => 'institute_create',
            ],
            [
                'id'    => 28,
                'title' => 'institute_edit',
            ],
            [
                'id'    => 29,
                'title' => 'institute_show',
            ],
            [
                'id'    => 30,
                'title' => 'institute_delete',
            ],
            [
                'id'    => 31,
                'title' => 'institute_access',
            ],
            [
                'id'    => 32,
                'title' => 'discuss_create',
            ],
            [
                'id'    => 33,
                'title' => 'discuss_edit',
            ],
            [
                'id'    => 34,
                'title' => 'discuss_show',
            ],
            [
                'id'    => 35,
                'title' => 'discuss_delete',
            ],
            [
                'id'    => 36,
                'title' => 'discuss_access',
            ],
            [
                'id'    => 37,
                'title' => 'law_system_create',
            ],
            [
                'id'    => 38,
                'title' => 'law_system_edit',
            ],
            [
                'id'    => 39,
                'title' => 'law_system_show',
            ],
            [
                'id'    => 40,
                'title' => 'law_system_delete',
            ],
            [
                'id'    => 41,
                'title' => 'law_system_access',
            ],
            [
                'id'    => 42,
                'title' => 'position_create',
            ],
            [
                'id'    => 43,
                'title' => 'position_edit',
            ],
            [
                'id'    => 44,
                'title' => 'position_show',
            ],
            [
                'id'    => 45,
                'title' => 'position_delete',
            ],
            [
                'id'    => 46,
                'title' => 'position_access',
            ],
            [
                'id'    => 47,
                'title' => 'argument_create',
            ],
            [
                'id'    => 48,
                'title' => 'argument_edit',
            ],
            [
                'id'    => 49,
                'title' => 'argument_show',
            ],
            [
                'id'    => 50,
                'title' => 'argument_delete',
            ],
            [
                'id'    => 51,
                'title' => 'argument_access',
            ],
            [
                'id'    => 52,
                'title' => 'npa_manage_access',
            ],
            [
                'id'    => 53,
                'title' => 'category_create',
            ],
            [
                'id'    => 54,
                'title' => 'category_edit',
            ],
            [
                'id'    => 55,
                'title' => 'category_show',
            ],
            [
                'id'    => 56,
                'title' => 'category_delete',
            ],
            [
                'id'    => 57,
                'title' => 'category_access',
            ],
            [
                'id'    => 58,
                'title' => 'institutional_npa_access',
            ],
            [
                'id'    => 59,
                'title' => 'postateika_access',
            ],
            [
                'id'    => 60,
                'title' => 'article_create',
            ],
            [
                'id'    => 61,
                'title' => 'article_edit',
            ],
            [
                'id'    => 62,
                'title' => 'article_show',
            ],
            [
                'id'    => 63,
                'title' => 'article_delete',
            ],
            [
                'id'    => 64,
                'title' => 'article_access',
            ],
            [
                'id'    => 65,
                'title' => 'rule_create',
            ],
            [
                'id'    => 66,
                'title' => 'rule_edit',
            ],
            [
                'id'    => 67,
                'title' => 'rule_show',
            ],
            [
                'id'    => 68,
                'title' => 'rule_delete',
            ],
            [
                'id'    => 69,
                'title' => 'rule_access',
            ],
            [
                'id'    => 70,
                'title' => 'suggestion_create',
            ],
            [
                'id'    => 71,
                'title' => 'suggestion_edit',
            ],
            [
                'id'    => 72,
                'title' => 'suggestion_show',
            ],
            [
                'id'    => 73,
                'title' => 'suggestion_delete',
            ],
            [
                'id'    => 74,
                'title' => 'suggestion_access',
            ],
            [
                'id'    => 75,
                'title' => 'library_access',
            ],
            [
                'id'    => 76,
                'title' => 'razdel_create',
            ],
            [
                'id'    => 77,
                'title' => 'razdel_edit',
            ],
            [
                'id'    => 78,
                'title' => 'razdel_show',
            ],
            [
                'id'    => 79,
                'title' => 'razdel_delete',
            ],
            [
                'id'    => 80,
                'title' => 'razdel_access',
            ],
            [
                'id'    => 81,
                'title' => 'doctrine_create',
            ],
            [
                'id'    => 82,
                'title' => 'doctrine_edit',
            ],
            [
                'id'    => 83,
                'title' => 'doctrine_show',
            ],
            [
                'id'    => 84,
                'title' => 'doctrine_delete',
            ],
            [
                'id'    => 85,
                'title' => 'doctrine_access',
            ],
            [
                'id'    => 86,
                'title' => 'normat_create',
            ],
            [
                'id'    => 87,
                'title' => 'normat_edit',
            ],
            [
                'id'    => 88,
                'title' => 'normat_show',
            ],
            [
                'id'    => 89,
                'title' => 'normat_delete',
            ],
            [
                'id'    => 90,
                'title' => 'normat_access',
            ],
            [
                'id'    => 91,
                'title' => 'court_create',
            ],
            [
                'id'    => 92,
                'title' => 'court_edit',
            ],
            [
                'id'    => 93,
                'title' => 'court_show',
            ],
            [
                'id'    => 94,
                'title' => 'court_delete',
            ],
            [
                'id'    => 95,
                'title' => 'court_access',
            ],
            [
                'id'    => 96,
                'title' => 'fikir_create',
            ],
            [
                'id'    => 97,
                'title' => 'fikir_edit',
            ],
            [
                'id'    => 98,
                'title' => 'fikir_show',
            ],
            [
                'id'    => 99,
                'title' => 'fikir_delete',
            ],
            [
                'id'    => 100,
                'title' => 'fikir_access',
            ],
            [
                'id'    => 101,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
